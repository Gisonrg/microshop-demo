(function(){//[javascript/closure/base.js]
  var $jscomp = $jscomp || {};
  $jscomp.scope = {};
  $jscomp.findInternal = function($array$$, $callback$$, $thisArg$$) {
    $array$$ instanceof String && ($array$$ = String($array$$));
    for (var $len$$ = $array$$.length, $i$$ = 0; $i$$ < $len$$; $i$$++) {
      var $value$$ = $array$$[$i$$];
      if ($callback$$.call($thisArg$$, $value$$, $i$$, $array$$)) {
        return {i:$i$$, v:$value$$};
      }
    }
    return {i:-1, v:void 0};
  };
  $jscomp.ASSUME_ES5 = !1;
  $jscomp.ASSUME_NO_NATIVE_MAP = !1;
  $jscomp.ASSUME_NO_NATIVE_SET = !1;
  $jscomp.SIMPLE_FROUND_POLYFILL = !1;
  $jscomp.defineProperty = $jscomp.ASSUME_ES5 || "function" == typeof Object.defineProperties ? Object.defineProperty : function($target$$, $property$$, $descriptor$$) {
    $target$$ != Array.prototype && $target$$ != Object.prototype && ($target$$[$property$$] = $descriptor$$.value);
  };
  $jscomp.getGlobal = function($maybeGlobal$$) {
    return "undefined" != typeof window && window === $maybeGlobal$$ ? $maybeGlobal$$ : "undefined" != typeof global && null != global ? global : $maybeGlobal$$;
  };
  $jscomp.global = $jscomp.getGlobal(this);
  $jscomp.polyfill = function($property$jscomp$5_split_target$$, $impl_polyfill$$) {
    if ($impl_polyfill$$) {
      var $obj$$ = $jscomp.global;
      $property$jscomp$5_split_target$$ = $property$jscomp$5_split_target$$.split(".");
      for (var $i$$ = 0; $i$$ < $property$jscomp$5_split_target$$.length - 1; $i$$++) {
        var $key$$ = $property$jscomp$5_split_target$$[$i$$];
        $key$$ in $obj$$ || ($obj$$[$key$$] = {});
        $obj$$ = $obj$$[$key$$];
      }
      $property$jscomp$5_split_target$$ = $property$jscomp$5_split_target$$[$property$jscomp$5_split_target$$.length - 1];
      $i$$ = $obj$$[$property$jscomp$5_split_target$$];
      $impl_polyfill$$ = $impl_polyfill$$($i$$);
      $impl_polyfill$$ != $i$$ && null != $impl_polyfill$$ && $jscomp.defineProperty($obj$$, $property$jscomp$5_split_target$$, {configurable:!0, writable:!0, value:$impl_polyfill$$});
    }
  };
  $jscomp.checkStringArgs = function($thisArg$$, $arg$$, $func$$) {
    if (null == $thisArg$$) {
      throw new TypeError("The 'this' value for String.prototype." + $func$$ + " must not be null or undefined");
    }
    if ($arg$$ instanceof RegExp) {
      throw new TypeError("First argument to String.prototype." + $func$$ + " must not be a regular expression");
    }
    return $thisArg$$ + "";
  };
  $jscomp.polyfill("String.prototype.repeat", function($orig$jscomp$5_polyfill$$) {
    return $orig$jscomp$5_polyfill$$ ? $orig$jscomp$5_polyfill$$ : $orig$jscomp$5_polyfill$$ = function($copies$$) {
      var $string$$ = $jscomp.checkStringArgs(this, null, "repeat");
      if (0 > $copies$$ || 1342177279 < $copies$$) {
        throw new RangeError("Invalid count value");
      }
      $copies$$ |= 0;
      for (var $result$$ = ""; $copies$$;) {
        if ($copies$$ & 1 && ($result$$ += $string$$), $copies$$ >>>= 1) {
          $string$$ += $string$$;
        }
      }
      return $result$$;
    };
  }, "es6", "es3");
  $jscomp.checkEs6ConformanceViaProxy = function() {
    try {
      var $proxied$$ = {}, $proxy$$ = Object.create(new $jscomp.global.Proxy($proxied$$, {get:function($target$$, $key$$, $receiver$$) {
        return $target$$ == $proxied$$ && "q" == $key$$ && $receiver$$ == $proxy$$;
      }}));
      return !0 === $proxy$$.q;
    } catch ($err$$) {
      return !1;
    }
  };
  $jscomp.USE_PROXY_FOR_ES6_CONFORMANCE_CHECKS = !1;
  $jscomp.ES6_CONFORMANCE = $jscomp.USE_PROXY_FOR_ES6_CONFORMANCE_CHECKS && $jscomp.checkEs6ConformanceViaProxy();
  $jscomp.arrayIteratorImpl = function($array$$) {
    var $index$$ = 0;
    return function() {
      return $index$$ < $array$$.length ? {done:!1, value:$array$$[$index$$++]} : {done:!0};
    };
  };
  $jscomp.arrayIterator = function($array$$) {
    return {next:$jscomp.arrayIteratorImpl($array$$)};
  };
  $jscomp.SYMBOL_PREFIX = "jscomp_symbol_";
  $jscomp.initSymbol = function() {
    $jscomp.initSymbol = function() {
    };
    $jscomp.global.Symbol || ($jscomp.global.Symbol = $jscomp.Symbol);
  };
  $jscomp.SymbolClass = function($id$$, $opt_description$$) {
    this.$jscomp$symbol$id_ = $id$$;
    $jscomp.defineProperty(this, "description", {configurable:!0, writable:!0, value:$opt_description$$});
  };
  $jscomp.SymbolClass.prototype.toString = function() {
    return this.$jscomp$symbol$id_;
  };
  $jscomp.Symbol = function() {
    function $Symbol$$($opt_description$$) {
      if (this instanceof $Symbol$$) {
        throw new TypeError("Symbol is not a constructor");
      }
      return new $jscomp.SymbolClass($jscomp.SYMBOL_PREFIX + ($opt_description$$ || "") + "_" + $counter$$++, $opt_description$$);
    }
    var $counter$$ = 0;
    return $Symbol$$;
  }();
  $jscomp.initSymbolIterator = function() {
    $jscomp.initSymbol();
    var $symbolIterator$$ = $jscomp.global.Symbol.iterator;
    $symbolIterator$$ || ($symbolIterator$$ = $jscomp.global.Symbol.iterator = $jscomp.global.Symbol("Symbol.iterator"));
    "function" != typeof Array.prototype[$symbolIterator$$] && $jscomp.defineProperty(Array.prototype, $symbolIterator$$, {configurable:!0, writable:!0, value:function() {
      return $jscomp.iteratorPrototype($jscomp.arrayIteratorImpl(this));
    }});
    $jscomp.initSymbolIterator = function() {
    };
  };
  $jscomp.initSymbolAsyncIterator = function() {
    $jscomp.initSymbol();
    var $symbolAsyncIterator$$ = $jscomp.global.Symbol.asyncIterator;
    $symbolAsyncIterator$$ || ($symbolAsyncIterator$$ = $jscomp.global.Symbol.asyncIterator = $jscomp.global.Symbol("Symbol.asyncIterator"));
    $jscomp.initSymbolAsyncIterator = function() {
    };
  };
  $jscomp.iteratorPrototype = function($iterator$$) {
    $jscomp.initSymbolIterator();
    $iterator$$ = {next:$iterator$$};
    $iterator$$[$jscomp.global.Symbol.iterator] = function() {
      return this;
    };
    return $iterator$$;
  };
  $jscomp.makeIterator = function($iterable$$) {
    var $iteratorFunction$$ = "undefined" != typeof Symbol && Symbol.iterator && $iterable$$[Symbol.iterator];
    return $iteratorFunction$$ ? $iteratorFunction$$.call($iterable$$) : $jscomp.arrayIterator($iterable$$);
  };
  $jscomp.owns = function($obj$$, $prop$$) {
    return Object.prototype.hasOwnProperty.call($obj$$, $prop$$);
  };
  $jscomp.polyfill("WeakMap", function($NativeWeakMap$$) {
    function $isConformant$$() {
      if (!$NativeWeakMap$$ || !Object.seal) {
        return !1;
      }
      try {
        var $x$$ = Object.seal({}), $y$$ = Object.seal({}), $map$$ = new $NativeWeakMap$$([[$x$$, 2], [$y$$, 3]]);
        if (2 != $map$$.get($x$$) || 3 != $map$$.get($y$$)) {
          return !1;
        }
        $map$$["delete"]($x$$);
        $map$$.set($y$$, 4);
        return !$map$$.has($x$$) && 4 == $map$$.get($y$$);
      } catch ($err$$) {
        return !1;
      }
    }
    function $WeakMapMembership$$() {
    }
    function $insert$$($target$$) {
      if (!$jscomp.owns($target$$, $prop$$)) {
        var $obj$$ = new $WeakMapMembership$$;
        $jscomp.defineProperty($target$$, $prop$$, {value:$obj$$});
      }
    }
    function $patch$$($name$$) {
      var $prev$$ = Object[$name$$];
      $prev$$ && (Object[$name$$] = function($target$$) {
        if ($target$$ instanceof $WeakMapMembership$$) {
          return $target$$;
        }
        $insert$$($target$$);
        return $prev$$($target$$);
      });
    }
    if ($jscomp.USE_PROXY_FOR_ES6_CONFORMANCE_CHECKS) {
      if ($NativeWeakMap$$ && $jscomp.ES6_CONFORMANCE) {
        return $NativeWeakMap$$;
      }
    } else {
      if ($isConformant$$()) {
        return $NativeWeakMap$$;
      }
    }
    var $prop$$ = "$jscomp_hidden_" + Math.random();
    $patch$$("freeze");
    $patch$$("preventExtensions");
    $patch$$("seal");
    var $index$$ = 0, $PolyfillWeakMap$$ = function($iter$jscomp$1_opt_iterable$$) {
      this.id_ = ($index$$ += Math.random() + 1).toString();
      if ($iter$jscomp$1_opt_iterable$$) {
        $iter$jscomp$1_opt_iterable$$ = $jscomp.makeIterator($iter$jscomp$1_opt_iterable$$);
        for (var $entry_item$$; !($entry_item$$ = $iter$jscomp$1_opt_iterable$$.next()).done;) {
          $entry_item$$ = $entry_item$$.value, this.set($entry_item$$[0], $entry_item$$[1]);
        }
      }
    };
    $PolyfillWeakMap$$.prototype.set = function($key$$, $value$$) {
      $insert$$($key$$);
      if (!$jscomp.owns($key$$, $prop$$)) {
        throw Error("WeakMap key fail: " + $key$$);
      }
      $key$$[$prop$$][this.id_] = $value$$;
      return this;
    };
    $PolyfillWeakMap$$.prototype.get = function($key$$) {
      return $jscomp.owns($key$$, $prop$$) ? $key$$[$prop$$][this.id_] : void 0;
    };
    $PolyfillWeakMap$$.prototype.has = function($key$$) {
      return $jscomp.owns($key$$, $prop$$) && $jscomp.owns($key$$[$prop$$], this.id_);
    };
    $PolyfillWeakMap$$.prototype["delete"] = function($key$$) {
      return $jscomp.owns($key$$, $prop$$) && $jscomp.owns($key$$[$prop$$], this.id_) ? delete $key$$[$prop$$][this.id_] : !1;
    };
    return $PolyfillWeakMap$$;
  }, "es6", "es3");
  $jscomp.MapEntry = function() {
  };
  $jscomp.polyfill("Map", function($NativeMap$$) {
    function $isConformant$$() {
      if ($jscomp.ASSUME_NO_NATIVE_MAP || !$NativeMap$$ || "function" != typeof $NativeMap$$ || !$NativeMap$$.prototype.entries || "function" != typeof Object.seal) {
        return !1;
      }
      try {
        var $key$$ = Object.seal({x:4}), $map$$ = new $NativeMap$$($jscomp.makeIterator([[$key$$, "s"]]));
        if ("s" != $map$$.get($key$$) || 1 != $map$$.size || $map$$.get({x:4}) || $map$$.set({x:4}, "t") != $map$$ || 2 != $map$$.size) {
          return !1;
        }
        var $iter$$ = $map$$.entries(), $item$$ = $iter$$.next();
        if ($item$$.done || $item$$.value[0] != $key$$ || "s" != $item$$.value[1]) {
          return !1;
        }
        $item$$ = $iter$$.next();
        return $item$$.done || 4 != $item$$.value[0].x || "t" != $item$$.value[1] || !$iter$$.next().done ? !1 : !0;
      } catch ($err$$) {
        return !1;
      }
    }
    if ($jscomp.USE_PROXY_FOR_ES6_CONFORMANCE_CHECKS) {
      if ($NativeMap$$ && $jscomp.ES6_CONFORMANCE) {
        return $NativeMap$$;
      }
    } else {
      if ($isConformant$$()) {
        return $NativeMap$$;
      }
    }
    $jscomp.initSymbolIterator();
    var $idMap$$ = new WeakMap, $PolyfillMap$$ = function($iter$jscomp$3_opt_iterable$$) {
      this.data_ = {};
      this.head_ = $createHead$$();
      this.size = 0;
      if ($iter$jscomp$3_opt_iterable$$) {
        $iter$jscomp$3_opt_iterable$$ = $jscomp.makeIterator($iter$jscomp$3_opt_iterable$$);
        for (var $entry$jscomp$1_item$$; !($entry$jscomp$1_item$$ = $iter$jscomp$3_opt_iterable$$.next()).done;) {
          $entry$jscomp$1_item$$ = $entry$jscomp$1_item$$.value, this.set($entry$jscomp$1_item$$[0], $entry$jscomp$1_item$$[1]);
        }
      }
    };
    $PolyfillMap$$.prototype.set = function($key$$, $value$$) {
      $key$$ = 0 === $key$$ ? 0 : $key$$;
      var $r$$ = $maybeGetEntry$$(this, $key$$);
      $r$$.list || ($r$$.list = this.data_[$r$$.id] = []);
      $r$$.entry ? $r$$.entry.value = $value$$ : ($r$$.entry = {next:this.head_, previous:this.head_.previous, head:this.head_, key:$key$$, value:$value$$}, $r$$.list.push($r$$.entry), this.head_.previous.next = $r$$.entry, this.head_.previous = $r$$.entry, this.size++);
      return this;
    };
    $PolyfillMap$$.prototype["delete"] = function($key$jscomp$47_r$$) {
      $key$jscomp$47_r$$ = $maybeGetEntry$$(this, $key$jscomp$47_r$$);
      return $key$jscomp$47_r$$.entry && $key$jscomp$47_r$$.list ? ($key$jscomp$47_r$$.list.splice($key$jscomp$47_r$$.index, 1), $key$jscomp$47_r$$.list.length || delete this.data_[$key$jscomp$47_r$$.id], $key$jscomp$47_r$$.entry.previous.next = $key$jscomp$47_r$$.entry.next, $key$jscomp$47_r$$.entry.next.previous = $key$jscomp$47_r$$.entry.previous, $key$jscomp$47_r$$.entry.head = null, this.size--, !0) : !1;
    };
    $PolyfillMap$$.prototype.clear = function() {
      this.data_ = {};
      this.head_ = this.head_.previous = $createHead$$();
      this.size = 0;
    };
    $PolyfillMap$$.prototype.has = function($key$$) {
      return !!$maybeGetEntry$$(this, $key$$).entry;
    };
    $PolyfillMap$$.prototype.get = function($entry$jscomp$2_key$$) {
      return ($entry$jscomp$2_key$$ = $maybeGetEntry$$(this, $entry$jscomp$2_key$$).entry) && $entry$jscomp$2_key$$.value;
    };
    $PolyfillMap$$.prototype.entries = function() {
      return $makeIterator$$(this, function($entry$$) {
        return [$entry$$.key, $entry$$.value];
      });
    };
    $PolyfillMap$$.prototype.keys = function() {
      return $makeIterator$$(this, function($entry$$) {
        return $entry$$.key;
      });
    };
    $PolyfillMap$$.prototype.values = function() {
      return $makeIterator$$(this, function($entry$$) {
        return $entry$$.value;
      });
    };
    $PolyfillMap$$.prototype.forEach = function($callback$$, $opt_thisArg$$) {
      for (var $iter$$ = this.entries(), $entry$jscomp$6_item$$; !($entry$jscomp$6_item$$ = $iter$$.next()).done;) {
        $entry$jscomp$6_item$$ = $entry$jscomp$6_item$$.value, $callback$$.call($opt_thisArg$$, $entry$jscomp$6_item$$[1], $entry$jscomp$6_item$$[0], this);
      }
    };
    $PolyfillMap$$.prototype[Symbol.iterator] = $PolyfillMap$$.prototype.entries;
    var $maybeGetEntry$$ = function($index$jscomp$70_map$$, $key$$) {
      var $id$jscomp$6_obj$$;
      var $id$jscomp$inline_20_list_type$$ = ($id$jscomp$6_obj$$ = $key$$) && typeof $id$jscomp$6_obj$$;
      "object" == $id$jscomp$inline_20_list_type$$ || "function" == $id$jscomp$inline_20_list_type$$ ? $idMap$$.has($id$jscomp$6_obj$$) ? $id$jscomp$6_obj$$ = $idMap$$.get($id$jscomp$6_obj$$) : ($id$jscomp$inline_20_list_type$$ = "" + ++$mapIndex$$, $idMap$$.set($id$jscomp$6_obj$$, $id$jscomp$inline_20_list_type$$), $id$jscomp$6_obj$$ = $id$jscomp$inline_20_list_type$$) : $id$jscomp$6_obj$$ = "p_" + $id$jscomp$6_obj$$;
      if (($id$jscomp$inline_20_list_type$$ = $index$jscomp$70_map$$.data_[$id$jscomp$6_obj$$]) && $jscomp.owns($index$jscomp$70_map$$.data_, $id$jscomp$6_obj$$)) {
        for ($index$jscomp$70_map$$ = 0; $index$jscomp$70_map$$ < $id$jscomp$inline_20_list_type$$.length; $index$jscomp$70_map$$++) {
          var $entry$$ = $id$jscomp$inline_20_list_type$$[$index$jscomp$70_map$$];
          if ($key$$ !== $key$$ && $entry$$.key !== $entry$$.key || $key$$ === $entry$$.key) {
            return {id:$id$jscomp$6_obj$$, list:$id$jscomp$inline_20_list_type$$, index:$index$jscomp$70_map$$, entry:$entry$$};
          }
        }
      }
      return {id:$id$jscomp$6_obj$$, list:$id$jscomp$inline_20_list_type$$, index:-1, entry:void 0};
    }, $makeIterator$$ = function($map$$, $func$$) {
      var $entry$$ = $map$$.head_;
      return $jscomp.iteratorPrototype(function() {
        if ($entry$$) {
          for (; $entry$$.head != $map$$.head_;) {
            $entry$$ = $entry$$.previous;
          }
          for (; $entry$$.next != $entry$$.head;) {
            return $entry$$ = $entry$$.next, {done:!1, value:$func$$($entry$$)};
          }
          $entry$$ = null;
        }
        return {done:!0, value:void 0};
      });
    }, $createHead$$ = function() {
      var $head$$ = {};
      return $head$$.previous = $head$$.next = $head$$.head = $head$$;
    }, $mapIndex$$ = 0;
    return $PolyfillMap$$;
  }, "es6", "es3");
  $jscomp.FORCE_POLYFILL_PROMISE = !1;
  $jscomp.polyfill("Promise", function($NativePromise$$) {
    function $AsyncExecutor$$() {
      this.batch_ = null;
    }
    function $resolvingPromise$$($opt_value$$) {
      return $opt_value$$ instanceof $PolyfillPromise$$ ? $opt_value$$ : new $PolyfillPromise$$(function($resolve$$) {
        $resolve$$($opt_value$$);
      });
    }
    if ($NativePromise$$ && !$jscomp.FORCE_POLYFILL_PROMISE) {
      return $NativePromise$$;
    }
    $AsyncExecutor$$.prototype.asyncExecute = function($f$$) {
      if (null == this.batch_) {
        this.batch_ = [];
        var $self$$ = this;
        this.asyncExecuteFunction(function() {
          $self$$.executeBatch_();
        });
      }
      this.batch_.push($f$$);
    };
    var $nativeSetTimeout$$ = $jscomp.global.setTimeout;
    $AsyncExecutor$$.prototype.asyncExecuteFunction = function($f$$) {
      $nativeSetTimeout$$($f$$, 0);
    };
    $AsyncExecutor$$.prototype.executeBatch_ = function() {
      for (; this.batch_ && this.batch_.length;) {
        var $executingBatch$$ = this.batch_;
        this.batch_ = [];
        for (var $i$$ = 0; $i$$ < $executingBatch$$.length; ++$i$$) {
          var $f$$ = $executingBatch$$[$i$$];
          $executingBatch$$[$i$$] = null;
          try {
            $f$$();
          } catch ($error$$) {
            this.asyncThrow_($error$$);
          }
        }
      }
      this.batch_ = null;
    };
    $AsyncExecutor$$.prototype.asyncThrow_ = function($exception$$) {
      this.asyncExecuteFunction(function() {
        throw $exception$$;
      });
    };
    var $PromiseState$$ = {PENDING:0, FULFILLED:1, REJECTED:2}, $PolyfillPromise$$ = function($executor$$) {
      this.state_ = $PromiseState$$.PENDING;
      this.result_ = void 0;
      this.onSettledCallbacks_ = [];
      var $resolveAndReject$$ = this.createResolveAndReject_();
      try {
        $executor$$($resolveAndReject$$.resolve, $resolveAndReject$$.reject);
      } catch ($e$$) {
        $resolveAndReject$$.reject($e$$);
      }
    };
    $PolyfillPromise$$.prototype.createResolveAndReject_ = function() {
      function $firstCallWins$$($method$$) {
        return function($x$$) {
          $alreadyCalled$$ || ($alreadyCalled$$ = !0, $method$$.call($thisPromise$$, $x$$));
        };
      }
      var $thisPromise$$ = this, $alreadyCalled$$ = !1;
      return {resolve:$firstCallWins$$(this.resolveTo_), reject:$firstCallWins$$(this.reject_)};
    };
    $PolyfillPromise$$.prototype.resolveTo_ = function($value$$) {
      if ($value$$ === this) {
        this.reject_(new TypeError("A Promise cannot resolve to itself"));
      } else {
        if ($value$$ instanceof $PolyfillPromise$$) {
          this.settleSameAsPromise_($value$$);
        } else {
          a: {
            switch(typeof $value$$) {
              case "object":
                var $JSCompiler_inline_result$$ = null != $value$$;
                break a;
              case "function":
                $JSCompiler_inline_result$$ = !0;
                break a;
              default:
                $JSCompiler_inline_result$$ = !1;
            }
          }
          $JSCompiler_inline_result$$ ? this.resolveToNonPromiseObj_($value$$) : this.fulfill_($value$$);
        }
      }
    };
    $PolyfillPromise$$.prototype.resolveToNonPromiseObj_ = function($obj$$) {
      var $thenMethod$$ = void 0;
      try {
        $thenMethod$$ = $obj$$.then;
      } catch ($error$$) {
        this.reject_($error$$);
        return;
      }
      "function" == typeof $thenMethod$$ ? this.settleSameAsThenable_($thenMethod$$, $obj$$) : this.fulfill_($obj$$);
    };
    $PolyfillPromise$$.prototype.reject_ = function($reason$$) {
      this.settle_($PromiseState$$.REJECTED, $reason$$);
    };
    $PolyfillPromise$$.prototype.fulfill_ = function($value$$) {
      this.settle_($PromiseState$$.FULFILLED, $value$$);
    };
    $PolyfillPromise$$.prototype.settle_ = function($settledState$$, $valueOrReason$$) {
      if (this.state_ != $PromiseState$$.PENDING) {
        throw Error("Cannot settle(" + $settledState$$ + ", " + $valueOrReason$$ + "): Promise already settled in state" + this.state_);
      }
      this.state_ = $settledState$$;
      this.result_ = $valueOrReason$$;
      this.executeOnSettledCallbacks_();
    };
    $PolyfillPromise$$.prototype.executeOnSettledCallbacks_ = function() {
      if (null != this.onSettledCallbacks_) {
        for (var $i$$ = 0; $i$$ < this.onSettledCallbacks_.length; ++$i$$) {
          $asyncExecutor$$.asyncExecute(this.onSettledCallbacks_[$i$$]);
        }
        this.onSettledCallbacks_ = null;
      }
    };
    var $asyncExecutor$$ = new $AsyncExecutor$$;
    $PolyfillPromise$$.prototype.settleSameAsPromise_ = function($promise$$) {
      var $methods$$ = this.createResolveAndReject_();
      $promise$$.callWhenSettled_($methods$$.resolve, $methods$$.reject);
    };
    $PolyfillPromise$$.prototype.settleSameAsThenable_ = function($thenMethod$$, $thenable$$) {
      var $methods$$ = this.createResolveAndReject_();
      try {
        $thenMethod$$.call($thenable$$, $methods$$.resolve, $methods$$.reject);
      } catch ($error$$) {
        $methods$$.reject($error$$);
      }
    };
    $PolyfillPromise$$.prototype.then = function($onFulfilled$$, $onRejected$$) {
      function $createCallback$$($paramF$$, $defaultF$$) {
        return "function" == typeof $paramF$$ ? function($x$$) {
          try {
            $resolveChild$$($paramF$$($x$$));
          } catch ($error$$) {
            $rejectChild$$($error$$);
          }
        } : $defaultF$$;
      }
      var $resolveChild$$, $rejectChild$$, $childPromise$$ = new $PolyfillPromise$$(function($resolve$$, $reject$$) {
        $resolveChild$$ = $resolve$$;
        $rejectChild$$ = $reject$$;
      });
      this.callWhenSettled_($createCallback$$($onFulfilled$$, $resolveChild$$), $createCallback$$($onRejected$$, $rejectChild$$));
      return $childPromise$$;
    };
    $PolyfillPromise$$.prototype["catch"] = function($onRejected$$) {
      return this.then(void 0, $onRejected$$);
    };
    $PolyfillPromise$$.prototype.callWhenSettled_ = function($onFulfilled$$, $onRejected$$) {
      function $callback$$() {
        switch($thisPromise$$.state_) {
          case $PromiseState$$.FULFILLED:
            $onFulfilled$$($thisPromise$$.result_);
            break;
          case $PromiseState$$.REJECTED:
            $onRejected$$($thisPromise$$.result_);
            break;
          default:
            throw Error("Unexpected state: " + $thisPromise$$.state_);
        }
      }
      var $thisPromise$$ = this;
      null == this.onSettledCallbacks_ ? $asyncExecutor$$.asyncExecute($callback$$) : this.onSettledCallbacks_.push($callback$$);
    };
    $PolyfillPromise$$.resolve = $resolvingPromise$$;
    $PolyfillPromise$$.reject = function($opt_reason$$) {
      return new $PolyfillPromise$$(function($resolve$$, $reject$$) {
        $reject$$($opt_reason$$);
      });
    };
    $PolyfillPromise$$.race = function($thenablesOrValues$$) {
      return new $PolyfillPromise$$(function($resolve$$, $reject$$) {
        for (var $iterator$$ = $jscomp.makeIterator($thenablesOrValues$$), $iterRec$$ = $iterator$$.next(); !$iterRec$$.done; $iterRec$$ = $iterator$$.next()) {
          $resolvingPromise$$($iterRec$$.value).callWhenSettled_($resolve$$, $reject$$);
        }
      });
    };
    $PolyfillPromise$$.all = function($thenablesOrValues$$) {
      var $iterator$$ = $jscomp.makeIterator($thenablesOrValues$$), $iterRec$$ = $iterator$$.next();
      return $iterRec$$.done ? $resolvingPromise$$([]) : new $PolyfillPromise$$(function($resolveAll$$, $rejectAll$$) {
        function $onFulfilled$$($i$$) {
          return function($ithResult$$) {
            $resultsArray$$[$i$$] = $ithResult$$;
            $unresolvedCount$$--;
            0 == $unresolvedCount$$ && $resolveAll$$($resultsArray$$);
          };
        }
        var $resultsArray$$ = [], $unresolvedCount$$ = 0;
        do {
          $resultsArray$$.push(void 0), $unresolvedCount$$++, $resolvingPromise$$($iterRec$$.value).callWhenSettled_($onFulfilled$$($resultsArray$$.length - 1), $rejectAll$$), $iterRec$$ = $iterator$$.next();
        } while (!$iterRec$$.done);
      });
    };
    return $PolyfillPromise$$;
  }, "es6", "es3");
  var goog = goog || {};
  goog.global = this || self;
  goog.isDef = function($val$$) {
    return void 0 !== $val$$;
  };
  goog.isString = function($val$$) {
    return "string" == typeof $val$$;
  };
  goog.isBoolean = function($val$$) {
    return "boolean" == typeof $val$$;
  };
  goog.isNumber = function($val$$) {
    return "number" == typeof $val$$;
  };
  goog.exportPath_ = function($name$$, $opt_object$$, $cur_opt_objectToExportTo$$) {
    $name$$ = $name$$.split(".");
    $cur_opt_objectToExportTo$$ = $cur_opt_objectToExportTo$$ || goog.global;
    $name$$[0] in $cur_opt_objectToExportTo$$ || "undefined" == typeof $cur_opt_objectToExportTo$$.execScript || $cur_opt_objectToExportTo$$.execScript("var " + $name$$[0]);
    for (var $part$$; $name$$.length && ($part$$ = $name$$.shift());) {
      !$name$$.length && goog.isDef($opt_object$$) ? $cur_opt_objectToExportTo$$[$part$$] = $opt_object$$ : $cur_opt_objectToExportTo$$ = $cur_opt_objectToExportTo$$[$part$$] && $cur_opt_objectToExportTo$$[$part$$] !== Object.prototype[$part$$] ? $cur_opt_objectToExportTo$$[$part$$] : $cur_opt_objectToExportTo$$[$part$$] = {};
    }
  };
  goog.define = function($name$jscomp$75_value$$, $defaultValue$$) {
    return $name$jscomp$75_value$$ = $defaultValue$$;
  };
  goog.FEATURESET_YEAR = 2012;
  goog.DEBUG = !0;
  goog.LOCALE = "en";
  goog.TRUSTED_SITE = !0;
  goog.STRICT_MODE_COMPATIBLE = !1;
  goog.DISALLOW_TEST_ONLY_CODE = !goog.DEBUG;
  goog.ENABLE_CHROME_APP_SAFE_SCRIPT_LOADING = !1;
  goog.provide = function($name$$) {
    if (goog.isInModuleLoader_()) {
      throw Error("goog.provide cannot be used within a module.");
    }
    goog.constructNamespace_($name$$);
  };
  goog.constructNamespace_ = function($name$$, $opt_obj$$) {
    goog.exportPath_($name$$, $opt_obj$$);
  };
  goog.getScriptNonce = function($opt_window$$) {
    if ($opt_window$$ && $opt_window$$ != goog.global) {
      return goog.getScriptNonce_($opt_window$$.document);
    }
    null === goog.cspNonce_ && (goog.cspNonce_ = goog.getScriptNonce_(goog.global.document));
    return goog.cspNonce_;
  };
  goog.NONCE_PATTERN_ = /^[\w+/_-]+[=]{0,2}$/;
  goog.cspNonce_ = null;
  goog.getScriptNonce_ = function($doc_nonce_script$$) {
    return ($doc_nonce_script$$ = $doc_nonce_script$$.querySelector && $doc_nonce_script$$.querySelector("script[nonce]")) && ($doc_nonce_script$$ = $doc_nonce_script$$.nonce || $doc_nonce_script$$.getAttribute("nonce")) && goog.NONCE_PATTERN_.test($doc_nonce_script$$) ? $doc_nonce_script$$ : "";
  };
  goog.VALID_MODULE_RE_ = /^[a-zA-Z_$][a-zA-Z0-9._$]*$/;
  goog.module = function($name$$) {
    if (!goog.isString($name$$) || !$name$$ || -1 == $name$$.search(goog.VALID_MODULE_RE_)) {
      throw Error("Invalid module identifier");
    }
    if (!goog.isInGoogModuleLoader_()) {
      throw Error("Module " + $name$$ + " has been loaded incorrectly. Note, modules cannot be loaded as normal scripts. They require some kind of pre-processing step. You're likely trying to load a module via a script tag or as a part of a concatenated bundle without rewriting the module. For more info see: https://github.com/google/closure-library/wiki/goog.module:-an-ES6-module-like-alternative-to-goog.provide.");
    }
    if (goog.moduleLoaderState_.moduleName) {
      throw Error("goog.module may only be called once per module.");
    }
    goog.moduleLoaderState_.moduleName = $name$$;
  };
  goog.module.get = function($name$$) {
    return null;
  };
  goog.module.getInternal_ = function() {
    return null;
  };
  goog.ModuleType = {ES6:"es6", GOOG:"goog"};
  goog.moduleLoaderState_ = null;
  goog.isInModuleLoader_ = function() {
    return goog.isInGoogModuleLoader_() || goog.isInEs6ModuleLoader_();
  };
  goog.isInGoogModuleLoader_ = function() {
    return !!goog.moduleLoaderState_ && goog.moduleLoaderState_.type == goog.ModuleType.GOOG;
  };
  goog.isInEs6ModuleLoader_ = function() {
    var $inLoader_jscomp$$ = !!goog.moduleLoaderState_ && goog.moduleLoaderState_.type == goog.ModuleType.ES6;
    return $inLoader_jscomp$$ ? !0 : ($inLoader_jscomp$$ = goog.global.$jscomp) ? "function" != typeof $inLoader_jscomp$$.getCurrentModulePath ? !1 : !!$inLoader_jscomp$$.getCurrentModulePath() : !1;
  };
  goog.module.declareLegacyNamespace = function() {
    goog.moduleLoaderState_.declareLegacyNamespace = !0;
  };
  goog.declareModuleId = function($namespace$$) {
    if (goog.moduleLoaderState_) {
      goog.moduleLoaderState_.moduleName = $namespace$$;
    } else {
      var $exports_jscomp$$ = goog.global.$jscomp;
      if (!$exports_jscomp$$ || "function" != typeof $exports_jscomp$$.getCurrentModulePath) {
        throw Error('Module with namespace "' + $namespace$$ + '" has been loaded incorrectly.');
      }
      $exports_jscomp$$ = $exports_jscomp$$.require($exports_jscomp$$.getCurrentModulePath());
      goog.loadedModules_[$namespace$$] = {exports:$exports_jscomp$$, type:goog.ModuleType.ES6, moduleId:$namespace$$};
    }
  };
  goog.setTestOnly = function($opt_message$$) {
    if (goog.DISALLOW_TEST_ONLY_CODE) {
      throw $opt_message$$ = $opt_message$$ || "", Error("Importing test-only code into non-debug environment" + ($opt_message$$ ? ": " + $opt_message$$ : "."));
    }
  };
  goog.forwardDeclare = function() {
  };
  goog.getObjectByName = function($name$jscomp$83_parts$$, $cur$jscomp$1_opt_obj$$) {
    $name$jscomp$83_parts$$ = $name$jscomp$83_parts$$.split(".");
    $cur$jscomp$1_opt_obj$$ = $cur$jscomp$1_opt_obj$$ || goog.global;
    for (var $i$$ = 0; $i$$ < $name$jscomp$83_parts$$.length; $i$$++) {
      if ($cur$jscomp$1_opt_obj$$ = $cur$jscomp$1_opt_obj$$[$name$jscomp$83_parts$$[$i$$]], !goog.isDefAndNotNull($cur$jscomp$1_opt_obj$$)) {
        return null;
      }
    }
    return $cur$jscomp$1_opt_obj$$;
  };
  goog.globalize = function($obj$$, $global$$) {
    $global$$ = $global$$ || goog.global;
    for (var $x$$ in $obj$$) {
      $global$$[$x$$] = $obj$$[$x$$];
    }
  };
  goog.addDependency = function() {
  };
  goog.useStrictRequires = !1;
  goog.ENABLE_DEBUG_LOADER = !0;
  goog.logToConsole_ = function($msg$$) {
    goog.global.console && goog.global.console.error($msg$$);
  };
  goog.require = function() {
  };
  goog.requireType = function() {
    return {};
  };
  goog.basePath = "";
  goog.nullFunction = function() {
  };
  goog.abstractMethod = function() {
    throw Error("unimplemented abstract method");
  };
  goog.addSingletonGetter = function($ctor$$) {
    $ctor$$.instance_ = void 0;
    $ctor$$.getInstance = function() {
      if ($ctor$$.instance_) {
        return $ctor$$.instance_;
      }
      goog.DEBUG && (goog.instantiatedSingletons_[goog.instantiatedSingletons_.length] = $ctor$$);
      return $ctor$$.instance_ = new $ctor$$;
    };
  };
  goog.instantiatedSingletons_ = [];
  goog.LOAD_MODULE_USING_EVAL = !0;
  goog.SEAL_MODULE_EXPORTS = goog.DEBUG;
  goog.loadedModules_ = {};
  goog.DEPENDENCIES_ENABLED = !1;
  goog.TRANSPILE = "detect";
  goog.ASSUME_ES_MODULES_TRANSPILED = !1;
  goog.TRANSPILE_TO_LANGUAGE = "";
  goog.TRANSPILER = "transpile.js";
  goog.hasBadLetScoping = null;
  goog.useSafari10Workaround = function() {
    if (null == goog.hasBadLetScoping) {
      try {
        var $hasBadLetScoping$$ = !eval('"use strict";let x = 1; function f() { return typeof x; };f() == "number";');
      } catch ($e$$) {
        $hasBadLetScoping$$ = !1;
      }
      goog.hasBadLetScoping = $hasBadLetScoping$$;
    }
    return goog.hasBadLetScoping;
  };
  goog.workaroundSafari10EvalBug = function($moduleDef$$) {
    return "(function(){" + $moduleDef$$ + "\n;})();\n";
  };
  goog.loadModule = function($moduleDef$$) {
    var $previousState$$ = goog.moduleLoaderState_;
    try {
      goog.moduleLoaderState_ = {moduleName:"", declareLegacyNamespace:!1, type:goog.ModuleType.GOOG};
      if (goog.isFunction($moduleDef$$)) {
        var $exports$$ = $moduleDef$$.call(void 0, {});
      } else {
        if (goog.isString($moduleDef$$)) {
          goog.useSafari10Workaround() && ($moduleDef$$ = goog.workaroundSafari10EvalBug($moduleDef$$)), $exports$$ = goog.loadModuleFromSource_.call(void 0, $moduleDef$$);
        } else {
          throw Error("Invalid module definition");
        }
      }
      var $moduleName$$ = goog.moduleLoaderState_.moduleName;
      if (goog.isString($moduleName$$) && $moduleName$$) {
        goog.moduleLoaderState_.declareLegacyNamespace ? goog.constructNamespace_($moduleName$$, $exports$$) : goog.SEAL_MODULE_EXPORTS && Object.seal && "object" == typeof $exports$$ && null != $exports$$ && Object.seal($exports$$);
        var $data$$ = {exports:$exports$$, type:goog.ModuleType.GOOG, moduleId:goog.moduleLoaderState_.moduleName};
        goog.loadedModules_[$moduleName$$] = $data$$;
      } else {
        throw Error('Invalid module name "' + $moduleName$$ + '"');
      }
    } finally {
      goog.moduleLoaderState_ = $previousState$$;
    }
  };
  goog.loadModuleFromSource_ = function($JSCompiler_OptimizeArgumentsArray_p0$$) {
    var $exports$$ = {};
    eval($JSCompiler_OptimizeArgumentsArray_p0$$);
    return $exports$$;
  };
  goog.normalizePath_ = function($components_path$$) {
    $components_path$$ = $components_path$$.split("/");
    for (var $i$$ = 0; $i$$ < $components_path$$.length;) {
      "." == $components_path$$[$i$$] ? $components_path$$.splice($i$$, 1) : $i$$ && ".." == $components_path$$[$i$$] && $components_path$$[$i$$ - 1] && ".." != $components_path$$[$i$$ - 1] ? $components_path$$.splice(--$i$$, 2) : $i$$++;
    }
    return $components_path$$.join("/");
  };
  goog.loadFileSync_ = function($src$$) {
    if (goog.global.CLOSURE_LOAD_FILE_SYNC) {
      return goog.global.CLOSURE_LOAD_FILE_SYNC($src$$);
    }
    try {
      var $xhr$$ = new goog.global.XMLHttpRequest;
      $xhr$$.open("get", $src$$, !1);
      $xhr$$.send();
      return 0 == $xhr$$.status || 200 == $xhr$$.status ? $xhr$$.responseText : null;
    } catch ($err$$) {
      return null;
    }
  };
  goog.transpile_ = function($code$jscomp$0$$, $path$jscomp$0$$, $target$$) {
    var $jscomp$$ = goog.global.$jscomp;
    $jscomp$$ || (goog.global.$jscomp = $jscomp$$ = {});
    var $transpile$$ = $jscomp$$.transpile;
    if (!$transpile$$) {
      var $transpilerPath$$ = goog.basePath + goog.TRANSPILER, $transpilerCode$$ = goog.loadFileSync_($transpilerPath$$);
      if ($transpilerCode$$) {
        (function() {
          (0,eval)($transpilerCode$$ + "\n//# sourceURL=" + $transpilerPath$$);
        }).call(goog.global);
        if (goog.global.$gwtExport && goog.global.$gwtExport.$jscomp && !goog.global.$gwtExport.$jscomp.transpile) {
          throw Error('The transpiler did not properly export the "transpile" method. $gwtExport: ' + JSON.stringify(goog.global.$gwtExport));
        }
        goog.global.$jscomp.transpile = goog.global.$gwtExport.$jscomp.transpile;
        $jscomp$$ = goog.global.$jscomp;
        $transpile$$ = $jscomp$$.transpile;
      }
    }
    if (!$transpile$$) {
      var $suffix$$ = " requires transpilation but no transpiler was found.";
      $suffix$$ += ' Please add "//javascript/closure:transpiler" as a data dependency to ensure it is included.';
      $transpile$$ = $jscomp$$.transpile = function($code$$, $path$$) {
        goog.logToConsole_($path$$ + $suffix$$);
        return $code$$;
      };
    }
    return $transpile$$($code$jscomp$0$$, $path$jscomp$0$$, $target$$);
  };
  goog.typeOf = function($value$$) {
    var $s$$ = typeof $value$$;
    if ("object" == $s$$) {
      if ($value$$) {
        if ($value$$ instanceof Array) {
          return "array";
        }
        if ($value$$ instanceof Object) {
          return $s$$;
        }
        var $className$$ = Object.prototype.toString.call($value$$);
        if ("[object Window]" == $className$$) {
          return "object";
        }
        if ("[object Array]" == $className$$ || "number" == typeof $value$$.length && "undefined" != typeof $value$$.splice && "undefined" != typeof $value$$.propertyIsEnumerable && !$value$$.propertyIsEnumerable("splice")) {
          return "array";
        }
        if ("[object Function]" == $className$$ || "undefined" != typeof $value$$.call && "undefined" != typeof $value$$.propertyIsEnumerable && !$value$$.propertyIsEnumerable("call")) {
          return "function";
        }
      } else {
        return "null";
      }
    } else {
      if ("function" == $s$$ && "undefined" == typeof $value$$.call) {
        return "object";
      }
    }
    return $s$$;
  };
  goog.isNull = function($val$$) {
    return null === $val$$;
  };
  goog.isDefAndNotNull = function($val$$) {
    return null != $val$$;
  };
  goog.isArray = function($val$$) {
    return "array" == goog.typeOf($val$$);
  };
  goog.isArrayLike = function($val$$) {
    var $type$$ = goog.typeOf($val$$);
    return "array" == $type$$ || "object" == $type$$ && "number" == typeof $val$$.length;
  };
  goog.isDateLike = function($val$$) {
    return goog.isObject($val$$) && "function" == typeof $val$$.getFullYear;
  };
  goog.isFunction = function($val$$) {
    return "function" == goog.typeOf($val$$);
  };
  goog.isObject = function($val$$) {
    var $type$$ = typeof $val$$;
    return "object" == $type$$ && null != $val$$ || "function" == $type$$;
  };
  goog.getUid = function($obj$$) {
    return $obj$$[goog.UID_PROPERTY_] || ($obj$$[goog.UID_PROPERTY_] = ++goog.uidCounter_);
  };
  goog.hasUid = function($obj$$) {
    return !!$obj$$[goog.UID_PROPERTY_];
  };
  goog.removeUid = function($obj$$) {
    null !== $obj$$ && "removeAttribute" in $obj$$ && $obj$$.removeAttribute(goog.UID_PROPERTY_);
    try {
      delete $obj$$[goog.UID_PROPERTY_];
    } catch ($ex$$) {
    }
  };
  goog.UID_PROPERTY_ = "closure_uid_" + (1e9 * Math.random() >>> 0);
  goog.uidCounter_ = 0;
  goog.getHashCode = goog.getUid;
  goog.removeHashCode = goog.removeUid;
  goog.cloneObject = function($obj$$) {
    var $clone_type$$ = goog.typeOf($obj$$);
    if ("object" == $clone_type$$ || "array" == $clone_type$$) {
      if ("function" === typeof $obj$$.clone) {
        return $obj$$.clone();
      }
      $clone_type$$ = "array" == $clone_type$$ ? [] : {};
      for (var $key$$ in $obj$$) {
        $clone_type$$[$key$$] = goog.cloneObject($obj$$[$key$$]);
      }
      return $clone_type$$;
    }
    return $obj$$;
  };
  goog.bindNative_ = function($fn$$, $selfObj$$, $var_args$$) {
    return $fn$$.call.apply($fn$$.bind, arguments);
  };
  goog.bindJs_ = function($fn$$, $selfObj$$, $var_args$$) {
    if (!$fn$$) {
      throw Error();
    }
    if (2 < arguments.length) {
      var $boundArgs$$ = Array.prototype.slice.call(arguments, 2);
      return function() {
        var $newArgs$$ = Array.prototype.slice.call(arguments);
        Array.prototype.unshift.apply($newArgs$$, $boundArgs$$);
        return $fn$$.apply($selfObj$$, $newArgs$$);
      };
    }
    return function() {
      return $fn$$.apply($selfObj$$, arguments);
    };
  };
  goog.bind = function($fn$$, $selfObj$$, $var_args$$) {
    Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? goog.bind = goog.bindNative_ : goog.bind = goog.bindJs_;
    return goog.bind.apply(null, arguments);
  };
  goog.partial = function($fn$$, $var_args$$) {
    var $args$$ = Array.prototype.slice.call(arguments, 1);
    return function() {
      var $newArgs$$ = $args$$.slice();
      $newArgs$$.push.apply($newArgs$$, arguments);
      return $fn$$.apply(this, $newArgs$$);
    };
  };
  goog.mixin = function($target$$, $source$$) {
    for (var $x$$ in $source$$) {
      $target$$[$x$$] = $source$$[$x$$];
    }
  };
  goog.now = goog.TRUSTED_SITE && Date.now || function() {
    return +new Date;
  };
  goog.globalEval = function($script$$) {
    if (goog.global.execScript) {
      goog.global.execScript($script$$, "JavaScript");
    } else {
      if (goog.global.eval) {
        if (null == goog.evalWorksForGlobals_) {
          try {
            goog.global.eval("var _evalTest_ = 1;");
          } catch ($ignore$$) {
          }
          if ("undefined" != typeof goog.global._evalTest_) {
            try {
              delete goog.global._evalTest_;
            } catch ($ignore$0$$) {
            }
            goog.evalWorksForGlobals_ = !0;
          } else {
            goog.evalWorksForGlobals_ = !1;
          }
        }
        if (goog.evalWorksForGlobals_) {
          goog.global.eval($script$$);
        } else {
          var $doc$$ = goog.global.document, $scriptElt$$ = $doc$$.createElement("SCRIPT");
          $scriptElt$$.type = "text/javascript";
          $scriptElt$$.defer = !1;
          $scriptElt$$.appendChild($doc$$.createTextNode($script$$));
          $doc$$.head.appendChild($scriptElt$$);
          $doc$$.head.removeChild($scriptElt$$);
        }
      } else {
        throw Error("goog.globalEval not available");
      }
    }
  };
  goog.evalWorksForGlobals_ = null;
  goog.getCssName = function($className$jscomp$1_result$$, $opt_modifier$$) {
    if ("." == String($className$jscomp$1_result$$).charAt(0)) {
      throw Error('className passed in goog.getCssName must not start with ".". You passed: ' + $className$jscomp$1_result$$);
    }
    var $getMapping$$ = function($cssName$$) {
      return goog.cssNameMapping_[$cssName$$] || $cssName$$;
    }, $rename_renameByParts$$ = function($cssName$jscomp$1_parts$$) {
      $cssName$jscomp$1_parts$$ = $cssName$jscomp$1_parts$$.split("-");
      for (var $mapped$$ = [], $i$$ = 0; $i$$ < $cssName$jscomp$1_parts$$.length; $i$$++) {
        $mapped$$.push($getMapping$$($cssName$jscomp$1_parts$$[$i$$]));
      }
      return $mapped$$.join("-");
    };
    $rename_renameByParts$$ = goog.cssNameMapping_ ? "BY_WHOLE" == goog.cssNameMappingStyle_ ? $getMapping$$ : $rename_renameByParts$$ : function($a$$) {
      return $a$$;
    };
    $className$jscomp$1_result$$ = $opt_modifier$$ ? $className$jscomp$1_result$$ + "-" + $rename_renameByParts$$($opt_modifier$$) : $rename_renameByParts$$($className$jscomp$1_result$$);
    return goog.global.CLOSURE_CSS_NAME_MAP_FN ? goog.global.CLOSURE_CSS_NAME_MAP_FN($className$jscomp$1_result$$) : $className$jscomp$1_result$$;
  };
  goog.setCssNameMapping = function($mapping$$, $opt_style$$) {
    goog.cssNameMapping_ = $mapping$$;
    goog.cssNameMappingStyle_ = $opt_style$$;
  };
  goog.getMsg = function($str$$, $opt_values$$) {
    $opt_values$$ && ($str$$ = $str$$.replace(/\{\$([^}]+)}/g, function($match$$, $key$$) {
      return null != $opt_values$$ && $key$$ in $opt_values$$ ? $opt_values$$[$key$$] : $match$$;
    }));
    return $str$$;
  };
  goog.getMsgWithFallback = function($a$$) {
    return $a$$;
  };
  goog.exportSymbol = function($publicPath$$, $object$$, $opt_objectToExportTo$$) {
    goog.exportPath_($publicPath$$, $object$$, $opt_objectToExportTo$$);
  };
  goog.exportProperty = function($object$$, $publicName$$, $symbol$$) {
    $object$$[$publicName$$] = $symbol$$;
  };
  goog.inherits = function($childCtor$$, $parentCtor$$) {
    function $tempCtor$$() {
    }
    $tempCtor$$.prototype = $parentCtor$$.prototype;
    $childCtor$$.superClass_ = $parentCtor$$.prototype;
    $childCtor$$.prototype = new $tempCtor$$;
    $childCtor$$.prototype.constructor = $childCtor$$;
    $childCtor$$.base = function($me$$, $methodName$$, $var_args$$) {
      for (var $args$$ = Array(arguments.length - 2), $i$$ = 2; $i$$ < arguments.length; $i$$++) {
        $args$$[$i$$ - 2] = arguments[$i$$];
      }
      return $parentCtor$$.prototype[$methodName$$].apply($me$$, $args$$);
    };
  };
  goog.base = function($me$$, $opt_methodName$$, $var_args$$) {
    var $caller$$ = arguments.callee.caller;
    if (goog.STRICT_MODE_COMPATIBLE || goog.DEBUG && !$caller$$) {
      throw Error("arguments.caller not defined.  goog.base() cannot be used with strict mode code. See http://www.ecma-international.org/ecma-262/5.1/#sec-C");
    }
    if ("undefined" !== typeof $caller$$.superClass_) {
      for (var $args$$ = Array(arguments.length - 1), $foundCaller_i$$ = 1; $foundCaller_i$$ < arguments.length; $foundCaller_i$$++) {
        $args$$[$foundCaller_i$$ - 1] = arguments[$foundCaller_i$$];
      }
      return $caller$$.superClass_.constructor.apply($me$$, $args$$);
    }
    if ("string" != typeof $opt_methodName$$ && "symbol" != typeof $opt_methodName$$) {
      throw Error("method names provided to goog.base must be a string or a symbol");
    }
    $args$$ = Array(arguments.length - 2);
    for ($foundCaller_i$$ = 2; $foundCaller_i$$ < arguments.length; $foundCaller_i$$++) {
      $args$$[$foundCaller_i$$ - 2] = arguments[$foundCaller_i$$];
    }
    $foundCaller_i$$ = !1;
    for (var $proto$$ = $me$$.constructor.prototype; $proto$$; $proto$$ = Object.getPrototypeOf($proto$$)) {
      if ($proto$$[$opt_methodName$$] === $caller$$) {
        $foundCaller_i$$ = !0;
      } else {
        if ($foundCaller_i$$) {
          return $proto$$[$opt_methodName$$].apply($me$$, $args$$);
        }
      }
    }
    if ($me$$[$opt_methodName$$] === $caller$$) {
      return $me$$.constructor.prototype[$opt_methodName$$].apply($me$$, $args$$);
    }
    throw Error("goog.base called from a method of one name to a method of a different name");
  };
  goog.scope = function($fn$$) {
    if (goog.isInModuleLoader_()) {
      throw Error("goog.scope is not supported within a module.");
    }
    $fn$$.call(goog.global);
  };
  goog.defineClass = function($superClass$$, $def$$) {
    var $cls_constructor$$ = $def$$.constructor, $statics$$ = $def$$.statics;
    $cls_constructor$$ && $cls_constructor$$ != Object.prototype.constructor || ($cls_constructor$$ = function() {
      throw Error("cannot instantiate an interface (no constructor defined).");
    });
    $cls_constructor$$ = goog.defineClass.createSealingConstructor_($cls_constructor$$, $superClass$$);
    $superClass$$ && goog.inherits($cls_constructor$$, $superClass$$);
    delete $def$$.constructor;
    delete $def$$.statics;
    goog.defineClass.applyProperties_($cls_constructor$$.prototype, $def$$);
    null != $statics$$ && ($statics$$ instanceof Function ? $statics$$($cls_constructor$$) : goog.defineClass.applyProperties_($cls_constructor$$, $statics$$));
    return $cls_constructor$$;
  };
  goog.defineClass.SEAL_CLASS_INSTANCES = goog.DEBUG;
  goog.defineClass.createSealingConstructor_ = function($ctr$$, $superClass$$) {
    if (!goog.defineClass.SEAL_CLASS_INSTANCES) {
      return $ctr$$;
    }
    var $superclassSealable$$ = !goog.defineClass.isUnsealable_($superClass$$), $wrappedCtr$$ = function() {
      var $instance$$ = $ctr$$.apply(this, arguments) || this;
      $instance$$[goog.UID_PROPERTY_] = $instance$$[goog.UID_PROPERTY_];
      this.constructor === $wrappedCtr$$ && $superclassSealable$$ && Object.seal instanceof Function && Object.seal($instance$$);
      return $instance$$;
    };
    return $wrappedCtr$$;
  };
  goog.defineClass.isUnsealable_ = function($ctr$$) {
    return $ctr$$ && $ctr$$.prototype && $ctr$$.prototype[goog.UNSEALABLE_CONSTRUCTOR_PROPERTY_];
  };
  goog.defineClass.OBJECT_PROTOTYPE_FIELDS_ = "constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf".split(" ");
  goog.defineClass.applyProperties_ = function($target$$, $source$$) {
    for (var $key$$ in $source$$) {
      Object.prototype.hasOwnProperty.call($source$$, $key$$) && ($target$$[$key$$] = $source$$[$key$$]);
    }
    for (var $i$$ = 0; $i$$ < goog.defineClass.OBJECT_PROTOTYPE_FIELDS_.length; $i$$++) {
      $key$$ = goog.defineClass.OBJECT_PROTOTYPE_FIELDS_[$i$$], Object.prototype.hasOwnProperty.call($source$$, $key$$) && ($target$$[$key$$] = $source$$[$key$$]);
    }
  };
  goog.tagUnsealableClass = function() {
  };
  goog.UNSEALABLE_CONSTRUCTOR_PROPERTY_ = "goog_defineClass_legacy_unsealable";
  goog.TRUSTED_TYPES_POLICY_NAME = "";
  goog.identity_ = function($s$$) {
    return $s$$;
  };
  goog.createTrustedTypesPolicy = function($name$$) {
    var $policy$$ = null;
    if ("undefined" === typeof TrustedTypes || !TrustedTypes.createPolicy) {
      return $policy$$;
    }
    try {
      $policy$$ = TrustedTypes.createPolicy($name$$, {createHTML:goog.identity_, createScript:goog.identity_, createScriptURL:goog.identity_, createURL:goog.identity_});
    } catch ($e$$) {
      goog.logToConsole_($e$$.message);
    }
    return $policy$$;
  };
  goog.TRUSTED_TYPES_POLICY_ = goog.TRUSTED_TYPES_POLICY_NAME ? goog.createTrustedTypesPolicy(goog.TRUSTED_TYPES_POLICY_NAME + "#base") : null;
  //[java/com/google/nbu/paisa/microapps/js/api/microapps_api.js]
  var module$exports$boq$paisamicroappsjs$api$MicroappsApi = function() {
  };
  module$exports$boq$paisamicroappsjs$api$MicroappsApi.prototype.initialize = function() {
  };
  module$exports$boq$paisamicroappsjs$api$MicroappsApi.prototype.requestPayment = function() {
  };
  module$exports$boq$paisamicroappsjs$api$MicroappsApi.prototype.requestScan = function() {
  };
  module$exports$boq$paisamicroappsjs$api$MicroappsApi.prototype.getParameter = function() {
  };
  module$exports$boq$paisamicroappsjs$api$MicroappsApi.prototype.getCurrentLocation = function() {
  };
  module$exports$boq$paisamicroappsjs$api$MicroappsApi.create = function($provider$$) {
    module$contents$boq$paisamicroappsjs$api$MicroappsApi_apiProvider = $provider$$;
    return new module$contents$boq$paisamicroappsjs$api$MicroappsApi_MicroappsApiDelegate;
  };
  var module$contents$boq$paisamicroappsjs$api$MicroappsApi_apiProvider = null, module$contents$boq$paisamicroappsjs$api$MicroappsApi_MicroappsApiDelegate = function() {
  };
  module$contents$boq$paisamicroappsjs$api$MicroappsApi_MicroappsApiDelegate.prototype.initialize = function() {
    module$contents$boq$paisamicroappsjs$api$MicroappsApi_apiProvider().initialize();
  };
  module$contents$boq$paisamicroappsjs$api$MicroappsApi_MicroappsApiDelegate.prototype.requestPayment = function($paymentRequest$$) {
    return module$contents$boq$paisamicroappsjs$api$MicroappsApi_apiProvider().requestPayment($paymentRequest$$);
  };
  module$contents$boq$paisamicroappsjs$api$MicroappsApi_MicroappsApiDelegate.prototype.requestScan = function($scanRequest$$) {
    return module$contents$boq$paisamicroappsjs$api$MicroappsApi_apiProvider().requestScan($scanRequest$$);
  };
  module$contents$boq$paisamicroappsjs$api$MicroappsApi_MicroappsApiDelegate.prototype.getParameter = function($param$$, $defaultValue$$) {
    return module$contents$boq$paisamicroappsjs$api$MicroappsApi_apiProvider().getParameter($param$$, $defaultValue$$);
  };
  module$contents$boq$paisamicroappsjs$api$MicroappsApi_MicroappsApiDelegate.prototype.getCurrentLocation = function() {
    return module$contents$boq$paisamicroappsjs$api$MicroappsApi_apiProvider().getCurrentLocation();
  };
  //[java/com/google/nbu/paisa/microapps/js/api/config.js]
  //[java/com/google/nbu/paisa/microapps/js/messaging/constants.js]
  var module$exports$boq$paisamicroappsjs$messaging$constants = {MessageName:{REQUEST_CONNECTION:"requestConnection", ACCEPT_CONNECTION:"acceptConnection"}, Environment:{AUTOPUSH:"autopush", PROD:"prod", PREPROD:"preprod", PROD_TT:"prod-tt", DAILY_1:"daily-1", DAILY_2:"daily-2", DAILY_3:"daily-3", DAILY_4:"daily-4", DAILY_5:"daily-5"}};
  //[javascript/closure/debug/error.js]
  goog.debug = {};
  goog.debug.Error = function($opt_msg$$) {
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, goog.debug.Error);
    } else {
      var $stack$$ = Error().stack;
      $stack$$ && (this.stack = $stack$$);
    }
    $opt_msg$$ && (this.message = String($opt_msg$$));
  };
  goog.inherits(goog.debug.Error, Error);
  goog.debug.Error.prototype.name = "CustomError";
  //[javascript/closure/dom/nodetype.js]
  goog.dom = {};
  goog.dom.NodeType = {ELEMENT:1, ATTRIBUTE:2, TEXT:3, CDATA_SECTION:4, ENTITY_REFERENCE:5, ENTITY:6, PROCESSING_INSTRUCTION:7, COMMENT:8, DOCUMENT:9, DOCUMENT_TYPE:10, DOCUMENT_FRAGMENT:11, NOTATION:12};
  //[javascript/closure/asserts/asserts.js]
  goog.asserts = {};
  goog.asserts.ENABLE_ASSERTS = goog.DEBUG;
  goog.asserts.AssertionError = function($messagePattern$$, $messageArgs$$) {
    goog.debug.Error.call(this, goog.asserts.subs_($messagePattern$$, $messageArgs$$));
  };
  goog.inherits(goog.asserts.AssertionError, goog.debug.Error);
  goog.asserts.AssertionError.prototype.name = "AssertionError";
  goog.asserts.DEFAULT_ERROR_HANDLER = function($e$$) {
    throw $e$$;
  };
  goog.asserts.errorHandler_ = goog.asserts.DEFAULT_ERROR_HANDLER;
  goog.asserts.subs_ = function($pattern$$, $subs$$) {
    $pattern$$ = $pattern$$.split("%s");
    for (var $returnString$$ = "", $subLast$$ = $pattern$$.length - 1, $i$$ = 0; $i$$ < $subLast$$; $i$$++) {
      var $sub$$ = $i$$ < $subs$$.length ? $subs$$[$i$$] : "%s";
      $returnString$$ += $pattern$$[$i$$] + $sub$$;
    }
    return $returnString$$ + $pattern$$[$subLast$$];
  };
  goog.asserts.doAssertFailure_ = function($defaultMessage_e$$, $defaultArgs$$, $givenMessage$$, $givenArgs$$) {
    var $message$$ = "Assertion failed";
    if ($givenMessage$$) {
      $message$$ += ": " + $givenMessage$$;
      var $args$$ = $givenArgs$$;
    } else {
      $defaultMessage_e$$ && ($message$$ += ": " + $defaultMessage_e$$, $args$$ = $defaultArgs$$);
    }
    $defaultMessage_e$$ = new goog.asserts.AssertionError("" + $message$$, $args$$ || []);
    goog.asserts.errorHandler_($defaultMessage_e$$);
  };
  goog.asserts.setErrorHandler = function($errorHandler$$) {
    goog.asserts.ENABLE_ASSERTS && (goog.asserts.errorHandler_ = $errorHandler$$);
  };
  goog.asserts.assert = function($condition$$, $opt_message$$, $var_args$$) {
    goog.asserts.ENABLE_ASSERTS && !$condition$$ && goog.asserts.doAssertFailure_("", null, $opt_message$$, Array.prototype.slice.call(arguments, 2));
    return $condition$$;
  };
  goog.asserts.assertExists = function($value$$, $opt_message$$, $var_args$$) {
    goog.asserts.ENABLE_ASSERTS && null == $value$$ && goog.asserts.doAssertFailure_("Expected to exist: %s.", [$value$$], $opt_message$$, Array.prototype.slice.call(arguments, 2));
    return $value$$;
  };
  goog.asserts.fail = function($opt_message$$, $var_args$$) {
    goog.asserts.ENABLE_ASSERTS && goog.asserts.errorHandler_(new goog.asserts.AssertionError("Failure" + ($opt_message$$ ? ": " + $opt_message$$ : ""), Array.prototype.slice.call(arguments, 1)));
  };
  goog.asserts.assertNumber = function($value$$, $opt_message$$, $var_args$$) {
    goog.asserts.ENABLE_ASSERTS && !goog.isNumber($value$$) && goog.asserts.doAssertFailure_("Expected number but got %s: %s.", [goog.typeOf($value$$), $value$$], $opt_message$$, Array.prototype.slice.call(arguments, 2));
    return $value$$;
  };
  goog.asserts.assertString = function($value$$, $opt_message$$, $var_args$$) {
    goog.asserts.ENABLE_ASSERTS && !goog.isString($value$$) && goog.asserts.doAssertFailure_("Expected string but got %s: %s.", [goog.typeOf($value$$), $value$$], $opt_message$$, Array.prototype.slice.call(arguments, 2));
    return $value$$;
  };
  goog.asserts.assertFunction = function($value$$, $opt_message$$, $var_args$$) {
    goog.asserts.ENABLE_ASSERTS && !goog.isFunction($value$$) && goog.asserts.doAssertFailure_("Expected function but got %s: %s.", [goog.typeOf($value$$), $value$$], $opt_message$$, Array.prototype.slice.call(arguments, 2));
    return $value$$;
  };
  goog.asserts.assertObject = function($value$$, $opt_message$$, $var_args$$) {
    goog.asserts.ENABLE_ASSERTS && !goog.isObject($value$$) && goog.asserts.doAssertFailure_("Expected object but got %s: %s.", [goog.typeOf($value$$), $value$$], $opt_message$$, Array.prototype.slice.call(arguments, 2));
    return $value$$;
  };
  goog.asserts.assertArray = function($value$$, $opt_message$$, $var_args$$) {
    goog.asserts.ENABLE_ASSERTS && !goog.isArray($value$$) && goog.asserts.doAssertFailure_("Expected array but got %s: %s.", [goog.typeOf($value$$), $value$$], $opt_message$$, Array.prototype.slice.call(arguments, 2));
    return $value$$;
  };
  goog.asserts.assertBoolean = function($value$$, $opt_message$$, $var_args$$) {
    goog.asserts.ENABLE_ASSERTS && !goog.isBoolean($value$$) && goog.asserts.doAssertFailure_("Expected boolean but got %s: %s.", [goog.typeOf($value$$), $value$$], $opt_message$$, Array.prototype.slice.call(arguments, 2));
    return $value$$;
  };
  goog.asserts.assertElement = function($value$$, $opt_message$$, $var_args$$) {
    !goog.asserts.ENABLE_ASSERTS || goog.isObject($value$$) && $value$$.nodeType == goog.dom.NodeType.ELEMENT || goog.asserts.doAssertFailure_("Expected Element but got %s: %s.", [goog.typeOf($value$$), $value$$], $opt_message$$, Array.prototype.slice.call(arguments, 2));
    return $value$$;
  };
  goog.asserts.assertInstanceof = function($value$$, $type$$, $opt_message$$, $var_args$$) {
    !goog.asserts.ENABLE_ASSERTS || $value$$ instanceof $type$$ || goog.asserts.doAssertFailure_("Expected instanceof %s but got %s.", [goog.asserts.getType_($type$$), goog.asserts.getType_($value$$)], $opt_message$$, Array.prototype.slice.call(arguments, 3));
    return $value$$;
  };
  goog.asserts.assertFinite = function($value$$, $opt_message$$, $var_args$$) {
    !goog.asserts.ENABLE_ASSERTS || "number" == typeof $value$$ && isFinite($value$$) || goog.asserts.doAssertFailure_("Expected %s to be a finite number but it is not.", [$value$$], $opt_message$$, Array.prototype.slice.call(arguments, 2));
    return $value$$;
  };
  goog.asserts.assertObjectPrototypeIsIntact = function() {
    for (var $key$$ in Object.prototype) {
      goog.asserts.fail($key$$ + " should not be enumerable in Object.prototype.");
    }
  };
  goog.asserts.getType_ = function($value$$) {
    return $value$$ instanceof Function ? $value$$.displayName || $value$$.name || "unknown type name" : $value$$ instanceof Object ? $value$$.constructor.displayName || $value$$.constructor.name || Object.prototype.toString.call($value$$) : null === $value$$ ? "null" : typeof $value$$;
  };
  //[javascript/closure/array/array.js]
  goog.array = {};
  goog.NATIVE_ARRAY_PROTOTYPES = goog.TRUSTED_SITE;
  goog.array.ASSUME_NATIVE_FUNCTIONS = !1;
  goog.array.peek = function($array$$) {
    return $array$$[$array$$.length - 1];
  };
  goog.array.last = goog.array.peek;
  goog.array.indexOf = goog.NATIVE_ARRAY_PROTOTYPES && (goog.array.ASSUME_NATIVE_FUNCTIONS || Array.prototype.indexOf) ? function($arr$$, $obj$$, $opt_fromIndex$$) {
    goog.asserts.assert(null != $arr$$.length);
    return Array.prototype.indexOf.call($arr$$, $obj$$, $opt_fromIndex$$);
  } : function($arr$$, $obj$$, $fromIndex_i$jscomp$27_opt_fromIndex$$) {
    $fromIndex_i$jscomp$27_opt_fromIndex$$ = null == $fromIndex_i$jscomp$27_opt_fromIndex$$ ? 0 : 0 > $fromIndex_i$jscomp$27_opt_fromIndex$$ ? Math.max(0, $arr$$.length + $fromIndex_i$jscomp$27_opt_fromIndex$$) : $fromIndex_i$jscomp$27_opt_fromIndex$$;
    if (goog.isString($arr$$)) {
      return goog.isString($obj$$) && 1 == $obj$$.length ? $arr$$.indexOf($obj$$, $fromIndex_i$jscomp$27_opt_fromIndex$$) : -1;
    }
    for (; $fromIndex_i$jscomp$27_opt_fromIndex$$ < $arr$$.length; $fromIndex_i$jscomp$27_opt_fromIndex$$++) {
      if ($fromIndex_i$jscomp$27_opt_fromIndex$$ in $arr$$ && $arr$$[$fromIndex_i$jscomp$27_opt_fromIndex$$] === $obj$$) {
        return $fromIndex_i$jscomp$27_opt_fromIndex$$;
      }
    }
    return -1;
  };
  goog.array.lastIndexOf = goog.NATIVE_ARRAY_PROTOTYPES && (goog.array.ASSUME_NATIVE_FUNCTIONS || Array.prototype.lastIndexOf) ? function($arr$$, $obj$$, $fromIndex$jscomp$1_opt_fromIndex$$) {
    goog.asserts.assert(null != $arr$$.length);
    $fromIndex$jscomp$1_opt_fromIndex$$ = null == $fromIndex$jscomp$1_opt_fromIndex$$ ? $arr$$.length - 1 : $fromIndex$jscomp$1_opt_fromIndex$$;
    return Array.prototype.lastIndexOf.call($arr$$, $obj$$, $fromIndex$jscomp$1_opt_fromIndex$$);
  } : function($arr$$, $obj$$, $fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$) {
    $fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$ = null == $fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$ ? $arr$$.length - 1 : $fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$;
    0 > $fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$ && ($fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$ = Math.max(0, $arr$$.length + $fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$));
    if (goog.isString($arr$$)) {
      return goog.isString($obj$$) && 1 == $obj$$.length ? $arr$$.lastIndexOf($obj$$, $fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$) : -1;
    }
    for (; 0 <= $fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$; $fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$--) {
      if ($fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$ in $arr$$ && $arr$$[$fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$] === $obj$$) {
        return $fromIndex$jscomp$2_i$jscomp$28_opt_fromIndex$$;
      }
    }
    return -1;
  };
  goog.array.forEach = goog.NATIVE_ARRAY_PROTOTYPES && (goog.array.ASSUME_NATIVE_FUNCTIONS || Array.prototype.forEach) ? function($arr$$, $f$$, $opt_obj$$) {
    goog.asserts.assert(null != $arr$$.length);
    Array.prototype.forEach.call($arr$$, $f$$, $opt_obj$$);
  } : function($arr$$, $f$$, $opt_obj$$) {
    for (var $l$$ = $arr$$.length, $arr2$$ = goog.isString($arr$$) ? $arr$$.split("") : $arr$$, $i$$ = 0; $i$$ < $l$$; $i$$++) {
      $i$$ in $arr2$$ && $f$$.call($opt_obj$$, $arr2$$[$i$$], $i$$, $arr$$);
    }
  };
  goog.array.forEachRight = function($arr$$, $f$$, $opt_obj$$) {
    var $i$jscomp$30_l$$ = $arr$$.length, $arr2$$ = goog.isString($arr$$) ? $arr$$.split("") : $arr$$;
    for (--$i$jscomp$30_l$$; 0 <= $i$jscomp$30_l$$; --$i$jscomp$30_l$$) {
      $i$jscomp$30_l$$ in $arr2$$ && $f$$.call($opt_obj$$, $arr2$$[$i$jscomp$30_l$$], $i$jscomp$30_l$$, $arr$$);
    }
  };
  goog.array.filter = goog.NATIVE_ARRAY_PROTOTYPES && (goog.array.ASSUME_NATIVE_FUNCTIONS || Array.prototype.filter) ? function($arr$$, $f$$, $opt_obj$$) {
    goog.asserts.assert(null != $arr$$.length);
    return Array.prototype.filter.call($arr$$, $f$$, $opt_obj$$);
  } : function($arr$$, $f$$, $opt_obj$$) {
    for (var $l$$ = $arr$$.length, $res$$ = [], $resLength$$ = 0, $arr2$$ = goog.isString($arr$$) ? $arr$$.split("") : $arr$$, $i$$ = 0; $i$$ < $l$$; $i$$++) {
      if ($i$$ in $arr2$$) {
        var $val$$ = $arr2$$[$i$$];
        $f$$.call($opt_obj$$, $val$$, $i$$, $arr$$) && ($res$$[$resLength$$++] = $val$$);
      }
    }
    return $res$$;
  };
  goog.array.map = goog.NATIVE_ARRAY_PROTOTYPES && (goog.array.ASSUME_NATIVE_FUNCTIONS || Array.prototype.map) ? function($arr$$, $f$$, $opt_obj$$) {
    goog.asserts.assert(null != $arr$$.length);
    return Array.prototype.map.call($arr$$, $f$$, $opt_obj$$);
  } : function($arr$$, $f$$, $opt_obj$$) {
    for (var $l$$ = $arr$$.length, $res$$ = Array($l$$), $arr2$$ = goog.isString($arr$$) ? $arr$$.split("") : $arr$$, $i$$ = 0; $i$$ < $l$$; $i$$++) {
      $i$$ in $arr2$$ && ($res$$[$i$$] = $f$$.call($opt_obj$$, $arr2$$[$i$$], $i$$, $arr$$));
    }
    return $res$$;
  };
  goog.array.reduce = goog.NATIVE_ARRAY_PROTOTYPES && (goog.array.ASSUME_NATIVE_FUNCTIONS || Array.prototype.reduce) ? function($arr$$, $f$$, $val$$, $opt_obj$$) {
    goog.asserts.assert(null != $arr$$.length);
    $opt_obj$$ && ($f$$ = goog.bind($f$$, $opt_obj$$));
    return Array.prototype.reduce.call($arr$$, $f$$, $val$$);
  } : function($arr$$, $f$$, $val$jscomp$0$$, $opt_obj$$) {
    var $rval$$ = $val$jscomp$0$$;
    goog.array.forEach($arr$$, function($val$$, $index$$) {
      $rval$$ = $f$$.call($opt_obj$$, $rval$$, $val$$, $index$$, $arr$$);
    });
    return $rval$$;
  };
  goog.array.reduceRight = goog.NATIVE_ARRAY_PROTOTYPES && (goog.array.ASSUME_NATIVE_FUNCTIONS || Array.prototype.reduceRight) ? function($arr$$, $f$$, $val$$, $opt_obj$$) {
    goog.asserts.assert(null != $arr$$.length);
    goog.asserts.assert(null != $f$$);
    $opt_obj$$ && ($f$$ = goog.bind($f$$, $opt_obj$$));
    return Array.prototype.reduceRight.call($arr$$, $f$$, $val$$);
  } : function($arr$$, $f$$, $val$jscomp$0$$, $opt_obj$$) {
    var $rval$$ = $val$jscomp$0$$;
    goog.array.forEachRight($arr$$, function($val$$, $index$$) {
      $rval$$ = $f$$.call($opt_obj$$, $rval$$, $val$$, $index$$, $arr$$);
    });
    return $rval$$;
  };
  goog.array.some = goog.NATIVE_ARRAY_PROTOTYPES && (goog.array.ASSUME_NATIVE_FUNCTIONS || Array.prototype.some) ? function($arr$$, $f$$, $opt_obj$$) {
    goog.asserts.assert(null != $arr$$.length);
    return Array.prototype.some.call($arr$$, $f$$, $opt_obj$$);
  } : function($arr$$, $f$$, $opt_obj$$) {
    for (var $l$$ = $arr$$.length, $arr2$$ = goog.isString($arr$$) ? $arr$$.split("") : $arr$$, $i$$ = 0; $i$$ < $l$$; $i$$++) {
      if ($i$$ in $arr2$$ && $f$$.call($opt_obj$$, $arr2$$[$i$$], $i$$, $arr$$)) {
        return !0;
      }
    }
    return !1;
  };
  goog.array.every = goog.NATIVE_ARRAY_PROTOTYPES && (goog.array.ASSUME_NATIVE_FUNCTIONS || Array.prototype.every) ? function($arr$$, $f$$, $opt_obj$$) {
    goog.asserts.assert(null != $arr$$.length);
    return Array.prototype.every.call($arr$$, $f$$, $opt_obj$$);
  } : function($arr$$, $f$$, $opt_obj$$) {
    for (var $l$$ = $arr$$.length, $arr2$$ = goog.isString($arr$$) ? $arr$$.split("") : $arr$$, $i$$ = 0; $i$$ < $l$$; $i$$++) {
      if ($i$$ in $arr2$$ && !$f$$.call($opt_obj$$, $arr2$$[$i$$], $i$$, $arr$$)) {
        return !1;
      }
    }
    return !0;
  };
  goog.array.count = function($arr$jscomp$0$$, $f$$, $opt_obj$$) {
    var $count$$ = 0;
    goog.array.forEach($arr$jscomp$0$$, function($element$$, $index$$, $arr$$) {
      $f$$.call($opt_obj$$, $element$$, $index$$, $arr$$) && ++$count$$;
    }, $opt_obj$$);
    return $count$$;
  };
  goog.array.find = function($arr$$, $f$jscomp$20_i$$, $opt_obj$$) {
    $f$jscomp$20_i$$ = goog.array.findIndex($arr$$, $f$jscomp$20_i$$, $opt_obj$$);
    return 0 > $f$jscomp$20_i$$ ? null : goog.isString($arr$$) ? $arr$$.charAt($f$jscomp$20_i$$) : $arr$$[$f$jscomp$20_i$$];
  };
  goog.array.findIndex = function($arr$$, $f$$, $opt_obj$$) {
    for (var $l$$ = $arr$$.length, $arr2$$ = goog.isString($arr$$) ? $arr$$.split("") : $arr$$, $i$$ = 0; $i$$ < $l$$; $i$$++) {
      if ($i$$ in $arr2$$ && $f$$.call($opt_obj$$, $arr2$$[$i$$], $i$$, $arr$$)) {
        return $i$$;
      }
    }
    return -1;
  };
  goog.array.findRight = function($arr$$, $f$jscomp$22_i$$, $opt_obj$$) {
    $f$jscomp$22_i$$ = goog.array.findIndexRight($arr$$, $f$jscomp$22_i$$, $opt_obj$$);
    return 0 > $f$jscomp$22_i$$ ? null : goog.isString($arr$$) ? $arr$$.charAt($f$jscomp$22_i$$) : $arr$$[$f$jscomp$22_i$$];
  };
  goog.array.findIndexRight = function($arr$$, $f$$, $opt_obj$$) {
    var $i$jscomp$38_l$$ = $arr$$.length, $arr2$$ = goog.isString($arr$$) ? $arr$$.split("") : $arr$$;
    for (--$i$jscomp$38_l$$; 0 <= $i$jscomp$38_l$$; $i$jscomp$38_l$$--) {
      if ($i$jscomp$38_l$$ in $arr2$$ && $f$$.call($opt_obj$$, $arr2$$[$i$jscomp$38_l$$], $i$jscomp$38_l$$, $arr$$)) {
        return $i$jscomp$38_l$$;
      }
    }
    return -1;
  };
  goog.array.contains = function($arr$$, $obj$$) {
    return 0 <= goog.array.indexOf($arr$$, $obj$$);
  };
  goog.array.isEmpty = function($arr$$) {
    return 0 == $arr$$.length;
  };
  goog.array.clear = function($arr$$) {
    if (!goog.isArray($arr$$)) {
      for (var $i$$ = $arr$$.length - 1; 0 <= $i$$; $i$$--) {
        delete $arr$$[$i$$];
      }
    }
    $arr$$.length = 0;
  };
  goog.array.insert = function($arr$$, $obj$$) {
    goog.array.contains($arr$$, $obj$$) || $arr$$.push($obj$$);
  };
  goog.array.insertAt = function($arr$$, $obj$$, $opt_i$$) {
    goog.array.splice($arr$$, $opt_i$$, 0, $obj$$);
  };
  goog.array.insertArrayAt = function($arr$$, $elementsToAdd$$, $opt_i$$) {
    goog.partial(goog.array.splice, $arr$$, $opt_i$$, 0).apply(null, $elementsToAdd$$);
  };
  goog.array.insertBefore = function($arr$$, $obj$$, $opt_obj2$$) {
    var $i$$;
    2 == arguments.length || 0 > ($i$$ = goog.array.indexOf($arr$$, $opt_obj2$$)) ? $arr$$.push($obj$$) : goog.array.insertAt($arr$$, $obj$$, $i$$);
  };
  goog.array.remove = function($arr$$, $i$jscomp$41_obj$$) {
    $i$jscomp$41_obj$$ = goog.array.indexOf($arr$$, $i$jscomp$41_obj$$);
    var $rv$$;
    ($rv$$ = 0 <= $i$jscomp$41_obj$$) && goog.array.removeAt($arr$$, $i$jscomp$41_obj$$);
    return $rv$$;
  };
  goog.array.removeLast = function($arr$$, $i$jscomp$42_obj$$) {
    $i$jscomp$42_obj$$ = goog.array.lastIndexOf($arr$$, $i$jscomp$42_obj$$);
    return 0 <= $i$jscomp$42_obj$$ ? (goog.array.removeAt($arr$$, $i$jscomp$42_obj$$), !0) : !1;
  };
  goog.array.removeAt = function($arr$$, $i$$) {
    goog.asserts.assert(null != $arr$$.length);
    return 1 == Array.prototype.splice.call($arr$$, $i$$, 1).length;
  };
  goog.array.removeIf = function($arr$$, $f$jscomp$24_i$$, $opt_obj$$) {
    $f$jscomp$24_i$$ = goog.array.findIndex($arr$$, $f$jscomp$24_i$$, $opt_obj$$);
    return 0 <= $f$jscomp$24_i$$ ? (goog.array.removeAt($arr$$, $f$jscomp$24_i$$), !0) : !1;
  };
  goog.array.removeAllIf = function($arr$$, $f$$, $opt_obj$$) {
    var $removedCount$$ = 0;
    goog.array.forEachRight($arr$$, function($val$$, $index$$) {
      $f$$.call($opt_obj$$, $val$$, $index$$, $arr$$) && goog.array.removeAt($arr$$, $index$$) && $removedCount$$++;
    });
    return $removedCount$$;
  };
  goog.array.concat = function($var_args$$) {
    return Array.prototype.concat.apply([], arguments);
  };
  goog.array.join = function($var_args$$) {
    return Array.prototype.concat.apply([], arguments);
  };
  goog.array.toArray = function($object$$) {
    var $length$$ = $object$$.length;
    if (0 < $length$$) {
      for (var $rv$$ = Array($length$$), $i$$ = 0; $i$$ < $length$$; $i$$++) {
        $rv$$[$i$$] = $object$$[$i$$];
      }
      return $rv$$;
    }
    return [];
  };
  goog.array.clone = goog.array.toArray;
  goog.array.extend = function($arr1$$, $var_args$$) {
    for (var $i$$ = 1; $i$$ < arguments.length; $i$$++) {
      var $arr2$$ = arguments[$i$$];
      if (goog.isArrayLike($arr2$$)) {
        var $len1$$ = $arr1$$.length || 0, $len2$$ = $arr2$$.length || 0;
        $arr1$$.length = $len1$$ + $len2$$;
        for (var $j$$ = 0; $j$$ < $len2$$; $j$$++) {
          $arr1$$[$len1$$ + $j$$] = $arr2$$[$j$$];
        }
      } else {
        $arr1$$.push($arr2$$);
      }
    }
  };
  goog.array.splice = function($arr$$, $index$$, $howMany$$, $var_args$$) {
    goog.asserts.assert(null != $arr$$.length);
    return Array.prototype.splice.apply($arr$$, goog.array.slice(arguments, 1));
  };
  goog.array.slice = function($arr$$, $start$$, $opt_end$$) {
    goog.asserts.assert(null != $arr$$.length);
    return 2 >= arguments.length ? Array.prototype.slice.call($arr$$, $start$$) : Array.prototype.slice.call($arr$$, $start$$, $opt_end$$);
  };
  goog.array.removeDuplicates = function($arr$$, $opt_rv_returnArray$$, $hashFn_opt_hashFn$$) {
    $opt_rv_returnArray$$ = $opt_rv_returnArray$$ || $arr$$;
    var $defaultHashFn_seen$$ = function($item$$) {
      return goog.isObject($item$$) ? "o" + goog.getUid($item$$) : (typeof $item$$).charAt(0) + $item$$;
    };
    $hashFn_opt_hashFn$$ = $hashFn_opt_hashFn$$ || $defaultHashFn_seen$$;
    $defaultHashFn_seen$$ = {};
    for (var $cursorInsert$$ = 0, $cursorRead$$ = 0; $cursorRead$$ < $arr$$.length;) {
      var $current$$ = $arr$$[$cursorRead$$++], $key$$ = $hashFn_opt_hashFn$$($current$$);
      Object.prototype.hasOwnProperty.call($defaultHashFn_seen$$, $key$$) || ($defaultHashFn_seen$$[$key$$] = !0, $opt_rv_returnArray$$[$cursorInsert$$++] = $current$$);
    }
    $opt_rv_returnArray$$.length = $cursorInsert$$;
  };
  goog.array.binarySearch = function($arr$$, $target$$, $opt_compareFn$$) {
    return goog.array.binarySearch_($arr$$, $opt_compareFn$$ || goog.array.defaultCompare, !1, $target$$);
  };
  goog.array.binarySelect = function($arr$$, $evaluator$$, $opt_obj$$) {
    return goog.array.binarySearch_($arr$$, $evaluator$$, !0, void 0, $opt_obj$$);
  };
  goog.array.binarySearch_ = function($arr$$, $compareFn$$, $isEvaluator$$, $opt_target$$, $opt_selfObj$$) {
    for (var $left$$ = 0, $right$$ = $arr$$.length, $found$$; $left$$ < $right$$;) {
      var $middle$$ = $left$$ + $right$$ >> 1;
      var $compareResult$$ = $isEvaluator$$ ? $compareFn$$.call($opt_selfObj$$, $arr$$[$middle$$], $middle$$, $arr$$) : $compareFn$$($opt_target$$, $arr$$[$middle$$]);
      0 < $compareResult$$ ? $left$$ = $middle$$ + 1 : ($right$$ = $middle$$, $found$$ = !$compareResult$$);
    }
    return $found$$ ? $left$$ : ~$left$$;
  };
  goog.array.sort = function($arr$$, $opt_compareFn$$) {
    $arr$$.sort($opt_compareFn$$ || goog.array.defaultCompare);
  };
  goog.array.stableSort = function($arr$$, $opt_compareFn$$) {
    function $stableCompareFn$$($obj1$$, $obj2$$) {
      return $valueCompareFn$$($obj1$$.value, $obj2$$.value) || $obj1$$.index - $obj2$$.index;
    }
    for (var $compArr$$ = Array($arr$$.length), $i$$ = 0; $i$$ < $arr$$.length; $i$$++) {
      $compArr$$[$i$$] = {index:$i$$, value:$arr$$[$i$$]};
    }
    var $valueCompareFn$$ = $opt_compareFn$$ || goog.array.defaultCompare;
    goog.array.sort($compArr$$, $stableCompareFn$$);
    for ($i$$ = 0; $i$$ < $arr$$.length; $i$$++) {
      $arr$$[$i$$] = $compArr$$[$i$$].value;
    }
  };
  goog.array.sortByKey = function($arr$$, $keyFn$$, $opt_compareFn$$) {
    var $keyCompareFn$$ = $opt_compareFn$$ || goog.array.defaultCompare;
    goog.array.sort($arr$$, function($a$$, $b$$) {
      return $keyCompareFn$$($keyFn$$($a$$), $keyFn$$($b$$));
    });
  };
  goog.array.sortObjectsByKey = function($arr$$, $key$$, $opt_compareFn$$) {
    goog.array.sortByKey($arr$$, function($obj$$) {
      return $obj$$[$key$$];
    }, $opt_compareFn$$);
  };
  goog.array.isSorted = function($arr$$, $compare_opt_compareFn$$, $opt_strict$$) {
    $compare_opt_compareFn$$ = $compare_opt_compareFn$$ || goog.array.defaultCompare;
    for (var $i$$ = 1; $i$$ < $arr$$.length; $i$$++) {
      var $compareResult$$ = $compare_opt_compareFn$$($arr$$[$i$$ - 1], $arr$$[$i$$]);
      if (0 < $compareResult$$ || 0 == $compareResult$$ && $opt_strict$$) {
        return !1;
      }
    }
    return !0;
  };
  goog.array.equals = function($arr1$$, $arr2$$, $equalsFn_opt_equalsFn$$) {
    if (!goog.isArrayLike($arr1$$) || !goog.isArrayLike($arr2$$) || $arr1$$.length != $arr2$$.length) {
      return !1;
    }
    var $l$$ = $arr1$$.length;
    $equalsFn_opt_equalsFn$$ = $equalsFn_opt_equalsFn$$ || goog.array.defaultCompareEquality;
    for (var $i$$ = 0; $i$$ < $l$$; $i$$++) {
      if (!$equalsFn_opt_equalsFn$$($arr1$$[$i$$], $arr2$$[$i$$])) {
        return !1;
      }
    }
    return !0;
  };
  goog.array.compare3 = function($arr1$$, $arr2$$, $compare$jscomp$1_opt_compareFn$$) {
    $compare$jscomp$1_opt_compareFn$$ = $compare$jscomp$1_opt_compareFn$$ || goog.array.defaultCompare;
    for (var $l$$ = Math.min($arr1$$.length, $arr2$$.length), $i$$ = 0; $i$$ < $l$$; $i$$++) {
      var $result$$ = $compare$jscomp$1_opt_compareFn$$($arr1$$[$i$$], $arr2$$[$i$$]);
      if (0 != $result$$) {
        return $result$$;
      }
    }
    return goog.array.defaultCompare($arr1$$.length, $arr2$$.length);
  };
  goog.array.defaultCompare = function($a$$, $b$$) {
    return $a$$ > $b$$ ? 1 : $a$$ < $b$$ ? -1 : 0;
  };
  goog.array.inverseDefaultCompare = function($a$$, $b$$) {
    return -goog.array.defaultCompare($a$$, $b$$);
  };
  goog.array.defaultCompareEquality = function($a$$, $b$$) {
    return $a$$ === $b$$;
  };
  goog.array.binaryInsert = function($array$$, $value$$, $index$jscomp$76_opt_compareFn$$) {
    $index$jscomp$76_opt_compareFn$$ = goog.array.binarySearch($array$$, $value$$, $index$jscomp$76_opt_compareFn$$);
    return 0 > $index$jscomp$76_opt_compareFn$$ ? (goog.array.insertAt($array$$, $value$$, -($index$jscomp$76_opt_compareFn$$ + 1)), !0) : !1;
  };
  goog.array.binaryRemove = function($array$$, $index$jscomp$77_value$$, $opt_compareFn$$) {
    $index$jscomp$77_value$$ = goog.array.binarySearch($array$$, $index$jscomp$77_value$$, $opt_compareFn$$);
    return 0 <= $index$jscomp$77_value$$ ? goog.array.removeAt($array$$, $index$jscomp$77_value$$) : !1;
  };
  goog.array.bucket = function($array$$, $sorter$$, $opt_obj$$) {
    for (var $buckets$$ = {}, $i$$ = 0; $i$$ < $array$$.length; $i$$++) {
      var $value$$ = $array$$[$i$$], $bucket_key$$ = $sorter$$.call($opt_obj$$, $value$$, $i$$, $array$$);
      goog.isDef($bucket_key$$) && ($bucket_key$$ = $buckets$$[$bucket_key$$] || ($buckets$$[$bucket_key$$] = []), $bucket_key$$.push($value$$));
    }
    return $buckets$$;
  };
  goog.array.toObject = function($arr$$, $keyFunc$$, $opt_obj$$) {
    var $ret$$ = {};
    goog.array.forEach($arr$$, function($element$$, $index$$) {
      $ret$$[$keyFunc$$.call($opt_obj$$, $element$$, $index$$, $arr$$)] = $element$$;
    });
    return $ret$$;
  };
  goog.array.range = function($i$$, $opt_end$$, $opt_step_step$$) {
    var $array$$ = [], $start$$ = 0, $end$$ = $i$$;
    $opt_step_step$$ = $opt_step_step$$ || 1;
    void 0 !== $opt_end$$ && ($start$$ = $i$$, $end$$ = $opt_end$$);
    if (0 > $opt_step_step$$ * ($end$$ - $start$$)) {
      return [];
    }
    if (0 < $opt_step_step$$) {
      for ($i$$ = $start$$; $i$$ < $end$$; $i$$ += $opt_step_step$$) {
        $array$$.push($i$$);
      }
    } else {
      for ($i$$ = $start$$; $i$$ > $end$$; $i$$ += $opt_step_step$$) {
        $array$$.push($i$$);
      }
    }
    return $array$$;
  };
  goog.array.repeat = function($value$$, $n$$) {
    for (var $array$$ = [], $i$$ = 0; $i$$ < $n$$; $i$$++) {
      $array$$[$i$$] = $value$$;
    }
    return $array$$;
  };
  goog.array.flatten = function($var_args$$) {
    for (var $result$$ = [], $i$$ = 0; $i$$ < arguments.length; $i$$++) {
      var $element$$ = arguments[$i$$];
      if (goog.isArray($element$$)) {
        for (var $c$$ = 0; $c$$ < $element$$.length; $c$$ += 8192) {
          var $chunk$$ = goog.array.slice($element$$, $c$$, $c$$ + 8192);
          $chunk$$ = goog.array.flatten.apply(null, $chunk$$);
          for (var $r$$ = 0; $r$$ < $chunk$$.length; $r$$++) {
            $result$$.push($chunk$$[$r$$]);
          }
        }
      } else {
        $result$$.push($element$$);
      }
    }
    return $result$$;
  };
  goog.array.rotate = function($array$$, $n$$) {
    goog.asserts.assert(null != $array$$.length);
    $array$$.length && ($n$$ %= $array$$.length, 0 < $n$$ ? Array.prototype.unshift.apply($array$$, $array$$.splice(-$n$$, $n$$)) : 0 > $n$$ && Array.prototype.push.apply($array$$, $array$$.splice(0, -$n$$)));
    return $array$$;
  };
  goog.array.moveItem = function($arr$$, $fromIndex$$, $toIndex$$) {
    goog.asserts.assert(0 <= $fromIndex$$ && $fromIndex$$ < $arr$$.length);
    goog.asserts.assert(0 <= $toIndex$$ && $toIndex$$ < $arr$$.length);
    $fromIndex$$ = Array.prototype.splice.call($arr$$, $fromIndex$$, 1);
    Array.prototype.splice.call($arr$$, $toIndex$$, 0, $fromIndex$$[0]);
  };
  goog.array.zip = function($var_args$$) {
    if (!arguments.length) {
      return [];
    }
    for (var $result$$ = [], $minLen$$ = arguments[0].length, $i$$ = 1; $i$$ < arguments.length; $i$$++) {
      arguments[$i$$].length < $minLen$$ && ($minLen$$ = arguments[$i$$].length);
    }
    for ($i$$ = 0; $i$$ < $minLen$$; $i$$++) {
      for (var $value$$ = [], $j$$ = 0; $j$$ < arguments.length; $j$$++) {
        $value$$.push(arguments[$j$$][$i$$]);
      }
      $result$$.push($value$$);
    }
    return $result$$;
  };
  goog.array.shuffle = function($arr$$, $opt_randFn_randFn$$) {
    $opt_randFn_randFn$$ = $opt_randFn_randFn$$ || Math.random;
    for (var $i$$ = $arr$$.length - 1; 0 < $i$$; $i$$--) {
      var $j$$ = Math.floor($opt_randFn_randFn$$() * ($i$$ + 1)), $tmp$$ = $arr$$[$i$$];
      $arr$$[$i$$] = $arr$$[$j$$];
      $arr$$[$j$$] = $tmp$$;
    }
  };
  goog.array.copyByIndex = function($arr$$, $index_arr$$) {
    var $result$$ = [];
    goog.array.forEach($index_arr$$, function($index$$) {
      $result$$.push($arr$$[$index$$]);
    });
    return $result$$;
  };
  goog.array.concatMap = function($arr$$, $f$$, $opt_obj$$) {
    return goog.array.concat.apply([], goog.array.map($arr$$, $f$$, $opt_obj$$));
  };
  //[javascript/closure/debug/errorcontext.js]
  goog.debug.errorcontext = {};
  goog.debug.errorcontext.addErrorContext = function($err$$, $contextKey$$, $contextValue$$) {
    $err$$[goog.debug.errorcontext.CONTEXT_KEY_] || ($err$$[goog.debug.errorcontext.CONTEXT_KEY_] = {});
    $err$$[goog.debug.errorcontext.CONTEXT_KEY_][$contextKey$$] = $contextValue$$;
  };
  goog.debug.errorcontext.getErrorContext = function($err$$) {
    return $err$$[goog.debug.errorcontext.CONTEXT_KEY_] || {};
  };
  goog.debug.errorcontext.CONTEXT_KEY_ = "__closure__error__context__984382";
  //[javascript/closure/string/internal.js]
  goog.string = {};
  goog.string.internal = {};
  goog.string.internal.startsWith = function($str$$, $prefix$$) {
    return 0 == $str$$.lastIndexOf($prefix$$, 0);
  };
  goog.string.internal.endsWith = function($str$$, $suffix$$) {
    var $l$$ = $str$$.length - $suffix$$.length;
    return 0 <= $l$$ && $str$$.indexOf($suffix$$, $l$$) == $l$$;
  };
  goog.string.internal.caseInsensitiveStartsWith = function($str$$, $prefix$$) {
    return 0 == goog.string.internal.caseInsensitiveCompare($prefix$$, $str$$.substr(0, $prefix$$.length));
  };
  goog.string.internal.caseInsensitiveEndsWith = function($str$$, $suffix$$) {
    return 0 == goog.string.internal.caseInsensitiveCompare($suffix$$, $str$$.substr($str$$.length - $suffix$$.length, $suffix$$.length));
  };
  goog.string.internal.caseInsensitiveEquals = function($str1$$, $str2$$) {
    return $str1$$.toLowerCase() == $str2$$.toLowerCase();
  };
  goog.string.internal.isEmptyOrWhitespace = function($str$$) {
    return /^[\s\xa0]*$/.test($str$$);
  };
  goog.string.internal.trim = goog.TRUSTED_SITE && String.prototype.trim ? function($str$$) {
    return $str$$.trim();
  } : function($str$$) {
    return /^[\s\xa0]*([\s\S]*?)[\s\xa0]*$/.exec($str$$)[1];
  };
  goog.string.internal.caseInsensitiveCompare = function($str1$$, $str2$$) {
    $str1$$ = String($str1$$).toLowerCase();
    $str2$$ = String($str2$$).toLowerCase();
    return $str1$$ < $str2$$ ? -1 : $str1$$ == $str2$$ ? 0 : 1;
  };
  goog.string.internal.newLineToBr = function($str$$, $opt_xml$$) {
    return $str$$.replace(/(\r\n|\r|\n)/g, $opt_xml$$ ? "<br />" : "<br>");
  };
  goog.string.internal.htmlEscape = function($str$$, $opt_isLikelyToContainHtmlChars$$) {
    if ($opt_isLikelyToContainHtmlChars$$) {
      $str$$ = $str$$.replace(goog.string.internal.AMP_RE_, "&amp;").replace(goog.string.internal.LT_RE_, "&lt;").replace(goog.string.internal.GT_RE_, "&gt;").replace(goog.string.internal.QUOT_RE_, "&quot;").replace(goog.string.internal.SINGLE_QUOTE_RE_, "&#39;").replace(goog.string.internal.NULL_RE_, "&#0;");
    } else {
      if (!goog.string.internal.ALL_RE_.test($str$$)) {
        return $str$$;
      }
      -1 != $str$$.indexOf("&") && ($str$$ = $str$$.replace(goog.string.internal.AMP_RE_, "&amp;"));
      -1 != $str$$.indexOf("<") && ($str$$ = $str$$.replace(goog.string.internal.LT_RE_, "&lt;"));
      -1 != $str$$.indexOf(">") && ($str$$ = $str$$.replace(goog.string.internal.GT_RE_, "&gt;"));
      -1 != $str$$.indexOf('"') && ($str$$ = $str$$.replace(goog.string.internal.QUOT_RE_, "&quot;"));
      -1 != $str$$.indexOf("'") && ($str$$ = $str$$.replace(goog.string.internal.SINGLE_QUOTE_RE_, "&#39;"));
      -1 != $str$$.indexOf("\x00") && ($str$$ = $str$$.replace(goog.string.internal.NULL_RE_, "&#0;"));
    }
    return $str$$;
  };
  goog.string.internal.AMP_RE_ = /&/g;
  goog.string.internal.LT_RE_ = /</g;
  goog.string.internal.GT_RE_ = />/g;
  goog.string.internal.QUOT_RE_ = /"/g;
  goog.string.internal.SINGLE_QUOTE_RE_ = /'/g;
  goog.string.internal.NULL_RE_ = /\x00/g;
  goog.string.internal.ALL_RE_ = /[\x00&<>"']/;
  goog.string.internal.whitespaceEscape = function($str$$, $opt_xml$$) {
    return goog.string.internal.newLineToBr($str$$.replace(/  /g, " &#160;"), $opt_xml$$);
  };
  goog.string.internal.contains = function($str$$, $subString$$) {
    return -1 != $str$$.indexOf($subString$$);
  };
  goog.string.internal.caseInsensitiveContains = function($str$$, $subString$$) {
    return goog.string.internal.contains($str$$.toLowerCase(), $subString$$.toLowerCase());
  };
  goog.string.internal.compareVersions = function($v1Subs_version1$$, $v2Subs_version2$$) {
    var $order_v1CompNum$$ = 0;
    $v1Subs_version1$$ = goog.string.internal.trim(String($v1Subs_version1$$)).split(".");
    $v2Subs_version2$$ = goog.string.internal.trim(String($v2Subs_version2$$)).split(".");
    for (var $subCount$$ = Math.max($v1Subs_version1$$.length, $v2Subs_version2$$.length), $subIdx$$ = 0; 0 == $order_v1CompNum$$ && $subIdx$$ < $subCount$$; $subIdx$$++) {
      var $v1Comp_v1Sub$$ = $v1Subs_version1$$[$subIdx$$] || "", $v2Comp_v2Sub$$ = $v2Subs_version2$$[$subIdx$$] || "";
      do {
        $v1Comp_v1Sub$$ = /(\d*)(\D*)(.*)/.exec($v1Comp_v1Sub$$) || ["", "", "", ""];
        $v2Comp_v2Sub$$ = /(\d*)(\D*)(.*)/.exec($v2Comp_v2Sub$$) || ["", "", "", ""];
        if (0 == $v1Comp_v1Sub$$[0].length && 0 == $v2Comp_v2Sub$$[0].length) {
          break;
        }
        $order_v1CompNum$$ = 0 == $v1Comp_v1Sub$$[1].length ? 0 : parseInt($v1Comp_v1Sub$$[1], 10);
        var $v2CompNum$$ = 0 == $v2Comp_v2Sub$$[1].length ? 0 : parseInt($v2Comp_v2Sub$$[1], 10);
        $order_v1CompNum$$ = goog.string.internal.compareElements_($order_v1CompNum$$, $v2CompNum$$) || goog.string.internal.compareElements_(0 == $v1Comp_v1Sub$$[2].length, 0 == $v2Comp_v2Sub$$[2].length) || goog.string.internal.compareElements_($v1Comp_v1Sub$$[2], $v2Comp_v2Sub$$[2]);
        $v1Comp_v1Sub$$ = $v1Comp_v1Sub$$[3];
        $v2Comp_v2Sub$$ = $v2Comp_v2Sub$$[3];
      } while (0 == $order_v1CompNum$$);
    }
    return $order_v1CompNum$$;
  };
  goog.string.internal.compareElements_ = function($left$$, $right$$) {
    return $left$$ < $right$$ ? -1 : $left$$ > $right$$ ? 1 : 0;
  };
  //[javascript/closure/labs/useragent/util.js]
  goog.labs = {};
  goog.labs.userAgent = {};
  goog.labs.userAgent.util = {};
  goog.labs.userAgent.util.getNativeUserAgentString_ = function() {
    var $navigator$jscomp$1_userAgent$$ = goog.labs.userAgent.util.getNavigator_();
    return $navigator$jscomp$1_userAgent$$ && ($navigator$jscomp$1_userAgent$$ = $navigator$jscomp$1_userAgent$$.userAgent) ? $navigator$jscomp$1_userAgent$$ : "";
  };
  goog.labs.userAgent.util.getNavigator_ = function() {
    return goog.global.navigator;
  };
  goog.labs.userAgent.util.userAgent_ = goog.labs.userAgent.util.getNativeUserAgentString_();
  goog.labs.userAgent.util.setUserAgent = function($opt_userAgent$$) {
    goog.labs.userAgent.util.userAgent_ = $opt_userAgent$$ || goog.labs.userAgent.util.getNativeUserAgentString_();
  };
  goog.labs.userAgent.util.getUserAgent = function() {
    return goog.labs.userAgent.util.userAgent_;
  };
  goog.labs.userAgent.util.matchUserAgent = function($str$$) {
    var $userAgent$$ = goog.labs.userAgent.util.getUserAgent();
    return goog.string.internal.contains($userAgent$$, $str$$);
  };
  goog.labs.userAgent.util.matchUserAgentIgnoreCase = function($str$$) {
    var $userAgent$$ = goog.labs.userAgent.util.getUserAgent();
    return goog.string.internal.caseInsensitiveContains($userAgent$$, $str$$);
  };
  goog.labs.userAgent.util.extractVersionTuples = function($userAgent$$) {
    for (var $versionRegExp$$ = /(\w[\w ]+)\/([^\s]+)\s*(?:\((.*?)\))?/g, $data$$ = [], $match$$; $match$$ = $versionRegExp$$.exec($userAgent$$);) {
      $data$$.push([$match$$[1], $match$$[2], $match$$[3] || void 0]);
    }
    return $data$$;
  };
  //[javascript/closure/object/object.js]
  goog.object = {};
  goog.object.is = function($v$$, $v2$$) {
    return $v$$ === $v2$$ ? 0 !== $v$$ || 1 / $v$$ === 1 / $v2$$ : $v$$ !== $v$$ && $v2$$ !== $v2$$;
  };
  goog.object.forEach = function($obj$$, $f$$, $opt_obj$$) {
    for (var $key$$ in $obj$$) {
      $f$$.call($opt_obj$$, $obj$$[$key$$], $key$$, $obj$$);
    }
  };
  goog.object.filter = function($obj$$, $f$$, $opt_obj$$) {
    var $res$$ = {}, $key$$;
    for ($key$$ in $obj$$) {
      $f$$.call($opt_obj$$, $obj$$[$key$$], $key$$, $obj$$) && ($res$$[$key$$] = $obj$$[$key$$]);
    }
    return $res$$;
  };
  goog.object.map = function($obj$$, $f$$, $opt_obj$$) {
    var $res$$ = {}, $key$$;
    for ($key$$ in $obj$$) {
      $res$$[$key$$] = $f$$.call($opt_obj$$, $obj$$[$key$$], $key$$, $obj$$);
    }
    return $res$$;
  };
  goog.object.some = function($obj$$, $f$$, $opt_obj$$) {
    for (var $key$$ in $obj$$) {
      if ($f$$.call($opt_obj$$, $obj$$[$key$$], $key$$, $obj$$)) {
        return !0;
      }
    }
    return !1;
  };
  goog.object.every = function($obj$$, $f$$, $opt_obj$$) {
    for (var $key$$ in $obj$$) {
      if (!$f$$.call($opt_obj$$, $obj$$[$key$$], $key$$, $obj$$)) {
        return !1;
      }
    }
    return !0;
  };
  goog.object.getCount = function($obj$$) {
    var $rv$$ = 0, $key$$;
    for ($key$$ in $obj$$) {
      $rv$$++;
    }
    return $rv$$;
  };
  goog.object.getAnyKey = function($obj$$) {
    for (var $key$$ in $obj$$) {
      return $key$$;
    }
  };
  goog.object.getAnyValue = function($obj$$) {
    for (var $key$$ in $obj$$) {
      return $obj$$[$key$$];
    }
  };
  goog.object.contains = function($obj$$, $val$$) {
    return goog.object.containsValue($obj$$, $val$$);
  };
  goog.object.getValues = function($obj$$) {
    var $res$$ = [], $i$$ = 0, $key$$;
    for ($key$$ in $obj$$) {
      $res$$[$i$$++] = $obj$$[$key$$];
    }
    return $res$$;
  };
  goog.object.getKeys = function($obj$$) {
    var $res$$ = [], $i$$ = 0, $key$$;
    for ($key$$ in $obj$$) {
      $res$$[$i$$++] = $key$$;
    }
    return $res$$;
  };
  goog.object.getValueByKeys = function($obj$$, $var_args$$) {
    var $i$$ = goog.isArrayLike($var_args$$), $keys$$ = $i$$ ? $var_args$$ : arguments;
    for ($i$$ = $i$$ ? 0 : 1; $i$$ < $keys$$.length; $i$$++) {
      if (null == $obj$$) {
        return;
      }
      $obj$$ = $obj$$[$keys$$[$i$$]];
    }
    return $obj$$;
  };
  goog.object.containsKey = function($obj$$, $key$$) {
    return null !== $obj$$ && $key$$ in $obj$$;
  };
  goog.object.containsValue = function($obj$$, $val$$) {
    for (var $key$$ in $obj$$) {
      if ($obj$$[$key$$] == $val$$) {
        return !0;
      }
    }
    return !1;
  };
  goog.object.findKey = function($obj$$, $f$$, $opt_this$$) {
    for (var $key$$ in $obj$$) {
      if ($f$$.call($opt_this$$, $obj$$[$key$$], $key$$, $obj$$)) {
        return $key$$;
      }
    }
  };
  goog.object.findValue = function($obj$$, $f$jscomp$33_key$$, $opt_this$$) {
    return ($f$jscomp$33_key$$ = goog.object.findKey($obj$$, $f$jscomp$33_key$$, $opt_this$$)) && $obj$$[$f$jscomp$33_key$$];
  };
  goog.object.isEmpty = function($obj$$) {
    for (var $key$$ in $obj$$) {
      return !1;
    }
    return !0;
  };
  goog.object.clear = function($obj$$) {
    for (var $i$$ in $obj$$) {
      delete $obj$$[$i$$];
    }
  };
  goog.object.remove = function($obj$$, $key$$) {
    var $rv$$;
    ($rv$$ = $key$$ in $obj$$) && delete $obj$$[$key$$];
    return $rv$$;
  };
  goog.object.add = function($obj$$, $key$$, $val$$) {
    if (null !== $obj$$ && $key$$ in $obj$$) {
      throw Error('The object already contains the key "' + $key$$ + '"');
    }
    goog.object.set($obj$$, $key$$, $val$$);
  };
  goog.object.get = function($obj$$, $key$$, $opt_val$$) {
    return null !== $obj$$ && $key$$ in $obj$$ ? $obj$$[$key$$] : $opt_val$$;
  };
  goog.object.set = function($obj$$, $key$$, $value$$) {
    $obj$$[$key$$] = $value$$;
  };
  goog.object.setIfUndefined = function($obj$$, $key$$, $value$$) {
    return $key$$ in $obj$$ ? $obj$$[$key$$] : $obj$$[$key$$] = $value$$;
  };
  goog.object.setWithReturnValueIfNotSet = function($obj$$, $key$$, $f$jscomp$34_val$$) {
    if ($key$$ in $obj$$) {
      return $obj$$[$key$$];
    }
    $f$jscomp$34_val$$ = $f$jscomp$34_val$$();
    return $obj$$[$key$$] = $f$jscomp$34_val$$;
  };
  goog.object.equals = function($a$$, $b$$) {
    for (var $k$$ in $a$$) {
      if (!($k$$ in $b$$) || $a$$[$k$$] !== $b$$[$k$$]) {
        return !1;
      }
    }
    for ($k$$ in $b$$) {
      if (!($k$$ in $a$$)) {
        return !1;
      }
    }
    return !0;
  };
  goog.object.clone = function($obj$$) {
    var $res$$ = {}, $key$$;
    for ($key$$ in $obj$$) {
      $res$$[$key$$] = $obj$$[$key$$];
    }
    return $res$$;
  };
  goog.object.unsafeClone = function($obj$$) {
    var $clone$jscomp$1_type$$ = goog.typeOf($obj$$);
    if ("object" == $clone$jscomp$1_type$$ || "array" == $clone$jscomp$1_type$$) {
      if (goog.isFunction($obj$$.clone)) {
        return $obj$$.clone();
      }
      $clone$jscomp$1_type$$ = "array" == $clone$jscomp$1_type$$ ? [] : {};
      for (var $key$$ in $obj$$) {
        $clone$jscomp$1_type$$[$key$$] = goog.object.unsafeClone($obj$$[$key$$]);
      }
      return $clone$jscomp$1_type$$;
    }
    return $obj$$;
  };
  goog.object.transpose = function($obj$$) {
    var $transposed$$ = {}, $key$$;
    for ($key$$ in $obj$$) {
      $transposed$$[$obj$$[$key$$]] = $key$$;
    }
    return $transposed$$;
  };
  goog.object.PROTOTYPE_FIELDS_ = "constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf".split(" ");
  goog.object.extend = function($target$$, $var_args$$) {
    for (var $key$$, $source$$, $i$$ = 1; $i$$ < arguments.length; $i$$++) {
      $source$$ = arguments[$i$$];
      for ($key$$ in $source$$) {
        $target$$[$key$$] = $source$$[$key$$];
      }
      for (var $j$$ = 0; $j$$ < goog.object.PROTOTYPE_FIELDS_.length; $j$$++) {
        $key$$ = goog.object.PROTOTYPE_FIELDS_[$j$$], Object.prototype.hasOwnProperty.call($source$$, $key$$) && ($target$$[$key$$] = $source$$[$key$$]);
      }
    }
  };
  goog.object.create = function($var_args$$) {
    var $argLength$$ = arguments.length;
    if (1 == $argLength$$ && goog.isArray(arguments[0])) {
      return goog.object.create.apply(null, arguments[0]);
    }
    if ($argLength$$ % 2) {
      throw Error("Uneven number of arguments");
    }
    for (var $rv$$ = {}, $i$$ = 0; $i$$ < $argLength$$; $i$$ += 2) {
      $rv$$[arguments[$i$$]] = arguments[$i$$ + 1];
    }
    return $rv$$;
  };
  goog.object.createSet = function($var_args$$) {
    var $argLength$$ = arguments.length;
    if (1 == $argLength$$ && goog.isArray(arguments[0])) {
      return goog.object.createSet.apply(null, arguments[0]);
    }
    for (var $rv$$ = {}, $i$$ = 0; $i$$ < $argLength$$; $i$$++) {
      $rv$$[arguments[$i$$]] = !0;
    }
    return $rv$$;
  };
  goog.object.createImmutableView = function($obj$$) {
    var $result$$ = $obj$$;
    Object.isFrozen && !Object.isFrozen($obj$$) && ($result$$ = Object.create($obj$$), Object.freeze($result$$));
    return $result$$;
  };
  goog.object.isImmutableView = function($obj$$) {
    return !!Object.isFrozen && Object.isFrozen($obj$$);
  };
  goog.object.getAllPropertyNames = function($obj$jscomp$77_proto$$, $opt_includeObjectPrototype$$, $opt_includeFunctionPrototype$$) {
    if (!$obj$jscomp$77_proto$$) {
      return [];
    }
    if (!Object.getOwnPropertyNames || !Object.getPrototypeOf) {
      return goog.object.getKeys($obj$jscomp$77_proto$$);
    }
    for (var $visitedSet$$ = {}; $obj$jscomp$77_proto$$ && ($obj$jscomp$77_proto$$ !== Object.prototype || $opt_includeObjectPrototype$$) && ($obj$jscomp$77_proto$$ !== Function.prototype || $opt_includeFunctionPrototype$$);) {
      for (var $names$$ = Object.getOwnPropertyNames($obj$jscomp$77_proto$$), $i$$ = 0; $i$$ < $names$$.length; $i$$++) {
        $visitedSet$$[$names$$[$i$$]] = !0;
      }
      $obj$jscomp$77_proto$$ = Object.getPrototypeOf($obj$jscomp$77_proto$$);
    }
    return goog.object.getKeys($visitedSet$$);
  };
  //[javascript/closure/labs/useragent/browser.js]
  goog.labs.userAgent.browser = {};
  goog.labs.userAgent.browser.matchOpera_ = function() {
    return goog.labs.userAgent.util.matchUserAgent("Opera");
  };
  goog.labs.userAgent.browser.matchIE_ = function() {
    return goog.labs.userAgent.util.matchUserAgent("Trident") || goog.labs.userAgent.util.matchUserAgent("MSIE");
  };
  goog.labs.userAgent.browser.matchEdge_ = function() {
    return goog.labs.userAgent.util.matchUserAgent("Edge");
  };
  goog.labs.userAgent.browser.matchFirefox_ = function() {
    return goog.labs.userAgent.util.matchUserAgent("Firefox") || goog.labs.userAgent.util.matchUserAgent("FxiOS");
  };
  goog.labs.userAgent.browser.matchSafari_ = function() {
    return goog.labs.userAgent.util.matchUserAgent("Safari") && !(goog.labs.userAgent.browser.matchChrome_() || goog.labs.userAgent.browser.matchCoast_() || goog.labs.userAgent.browser.matchOpera_() || goog.labs.userAgent.browser.matchEdge_() || goog.labs.userAgent.browser.matchFirefox_() || goog.labs.userAgent.browser.isSilk() || goog.labs.userAgent.util.matchUserAgent("Android"));
  };
  goog.labs.userAgent.browser.matchCoast_ = function() {
    return goog.labs.userAgent.util.matchUserAgent("Coast");
  };
  goog.labs.userAgent.browser.matchIosWebview_ = function() {
    return (goog.labs.userAgent.util.matchUserAgent("iPad") || goog.labs.userAgent.util.matchUserAgent("iPhone")) && !goog.labs.userAgent.browser.matchSafari_() && !goog.labs.userAgent.browser.matchChrome_() && !goog.labs.userAgent.browser.matchCoast_() && !goog.labs.userAgent.browser.matchFirefox_() && goog.labs.userAgent.util.matchUserAgent("AppleWebKit");
  };
  goog.labs.userAgent.browser.matchChrome_ = function() {
    return (goog.labs.userAgent.util.matchUserAgent("Chrome") || goog.labs.userAgent.util.matchUserAgent("CriOS")) && !goog.labs.userAgent.browser.matchEdge_();
  };
  goog.labs.userAgent.browser.matchAndroidBrowser_ = function() {
    return goog.labs.userAgent.util.matchUserAgent("Android") && !(goog.labs.userAgent.browser.isChrome() || goog.labs.userAgent.browser.isFirefox() || goog.labs.userAgent.browser.isOpera() || goog.labs.userAgent.browser.isSilk());
  };
  goog.labs.userAgent.browser.isOpera = goog.labs.userAgent.browser.matchOpera_;
  goog.labs.userAgent.browser.isIE = goog.labs.userAgent.browser.matchIE_;
  goog.labs.userAgent.browser.isEdge = goog.labs.userAgent.browser.matchEdge_;
  goog.labs.userAgent.browser.isFirefox = goog.labs.userAgent.browser.matchFirefox_;
  goog.labs.userAgent.browser.isSafari = goog.labs.userAgent.browser.matchSafari_;
  goog.labs.userAgent.browser.isCoast = goog.labs.userAgent.browser.matchCoast_;
  goog.labs.userAgent.browser.isIosWebview = goog.labs.userAgent.browser.matchIosWebview_;
  goog.labs.userAgent.browser.isChrome = goog.labs.userAgent.browser.matchChrome_;
  goog.labs.userAgent.browser.isAndroidBrowser = goog.labs.userAgent.browser.matchAndroidBrowser_;
  goog.labs.userAgent.browser.isSilk = function() {
    return goog.labs.userAgent.util.matchUserAgent("Silk");
  };
  goog.labs.userAgent.browser.getVersion = function() {
    function $lookUpValueWithKeys$$($key$jscomp$88_keys$$) {
      $key$jscomp$88_keys$$ = goog.array.find($key$jscomp$88_keys$$, $versionMapHasKey$$);
      return $versionMap$$[$key$jscomp$88_keys$$] || "";
    }
    var $tuple_userAgentString_versionTuples$$ = goog.labs.userAgent.util.getUserAgent();
    if (goog.labs.userAgent.browser.isIE()) {
      return goog.labs.userAgent.browser.getIEVersion_($tuple_userAgentString_versionTuples$$);
    }
    $tuple_userAgentString_versionTuples$$ = goog.labs.userAgent.util.extractVersionTuples($tuple_userAgentString_versionTuples$$);
    var $versionMap$$ = {};
    goog.array.forEach($tuple_userAgentString_versionTuples$$, function($tuple$jscomp$1_value$$) {
      var $key$$ = $tuple$jscomp$1_value$$[0];
      $tuple$jscomp$1_value$$ = $tuple$jscomp$1_value$$[1];
      $versionMap$$[$key$$] = $tuple$jscomp$1_value$$;
    });
    var $versionMapHasKey$$ = goog.partial(goog.object.containsKey, $versionMap$$);
    return goog.labs.userAgent.browser.isOpera() ? $lookUpValueWithKeys$$(["Version", "Opera"]) : goog.labs.userAgent.browser.isEdge() ? $lookUpValueWithKeys$$(["Edge"]) : goog.labs.userAgent.browser.isChrome() ? $lookUpValueWithKeys$$(["Chrome", "CriOS"]) : ($tuple_userAgentString_versionTuples$$ = $tuple_userAgentString_versionTuples$$[2]) && $tuple_userAgentString_versionTuples$$[1] || "";
  };
  goog.labs.userAgent.browser.isVersionOrHigher = function($version$$) {
    return 0 <= goog.string.internal.compareVersions(goog.labs.userAgent.browser.getVersion(), $version$$);
  };
  goog.labs.userAgent.browser.getIEVersion_ = function($tridentVersion_userAgent$$) {
    var $rv$jscomp$6_version$$ = /rv: *([\d\.]*)/.exec($tridentVersion_userAgent$$);
    if ($rv$jscomp$6_version$$ && $rv$jscomp$6_version$$[1]) {
      return $rv$jscomp$6_version$$[1];
    }
    $rv$jscomp$6_version$$ = "";
    var $msie$$ = /MSIE +([\d\.]+)/.exec($tridentVersion_userAgent$$);
    if ($msie$$ && $msie$$[1]) {
      if ($tridentVersion_userAgent$$ = /Trident\/(\d.\d)/.exec($tridentVersion_userAgent$$), "7.0" == $msie$$[1]) {
        if ($tridentVersion_userAgent$$ && $tridentVersion_userAgent$$[1]) {
          switch($tridentVersion_userAgent$$[1]) {
            case "4.0":
              $rv$jscomp$6_version$$ = "8.0";
              break;
            case "5.0":
              $rv$jscomp$6_version$$ = "9.0";
              break;
            case "6.0":
              $rv$jscomp$6_version$$ = "10.0";
              break;
            case "7.0":
              $rv$jscomp$6_version$$ = "11.0";
          }
        } else {
          $rv$jscomp$6_version$$ = "7.0";
        }
      } else {
        $rv$jscomp$6_version$$ = $msie$$[1];
      }
    }
    return $rv$jscomp$6_version$$;
  };
  //[javascript/closure/dom/asserts.js]
  goog.dom.asserts = {};
  goog.dom.asserts.assertIsLocation = function($o$$) {
    if (goog.asserts.ENABLE_ASSERTS) {
      var $win$$ = goog.dom.asserts.getWindow_($o$$);
      $win$$ && (!$o$$ || !($o$$ instanceof $win$$.Location) && $o$$ instanceof $win$$.Element) && goog.asserts.fail("Argument is not a Location (or a non-Element mock); got: %s", goog.dom.asserts.debugStringForType_($o$$));
    }
    return $o$$;
  };
  goog.dom.asserts.assertIsElementType_ = function($o$$, $typename$$) {
    if (goog.asserts.ENABLE_ASSERTS) {
      var $win$$ = goog.dom.asserts.getWindow_($o$$);
      $win$$ && "undefined" != typeof $win$$[$typename$$] && ($o$$ && ($o$$ instanceof $win$$[$typename$$] || !($o$$ instanceof $win$$.Location || $o$$ instanceof $win$$.Element)) || goog.asserts.fail("Argument is not a %s (or a non-Element, non-Location mock); got: %s", $typename$$, goog.dom.asserts.debugStringForType_($o$$)));
    }
    return $o$$;
  };
  goog.dom.asserts.assertIsHTMLAnchorElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLAnchorElement");
  };
  goog.dom.asserts.assertIsHTMLButtonElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLButtonElement");
  };
  goog.dom.asserts.assertIsHTMLLinkElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLLinkElement");
  };
  goog.dom.asserts.assertIsHTMLImageElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLImageElement");
  };
  goog.dom.asserts.assertIsHTMLAudioElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLAudioElement");
  };
  goog.dom.asserts.assertIsHTMLVideoElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLVideoElement");
  };
  goog.dom.asserts.assertIsHTMLInputElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLInputElement");
  };
  goog.dom.asserts.assertIsHTMLTextAreaElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLTextAreaElement");
  };
  goog.dom.asserts.assertIsHTMLCanvasElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLCanvasElement");
  };
  goog.dom.asserts.assertIsHTMLEmbedElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLEmbedElement");
  };
  goog.dom.asserts.assertIsHTMLFormElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLFormElement");
  };
  goog.dom.asserts.assertIsHTMLFrameElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLFrameElement");
  };
  goog.dom.asserts.assertIsHTMLIFrameElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLIFrameElement");
  };
  goog.dom.asserts.assertIsHTMLObjectElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLObjectElement");
  };
  goog.dom.asserts.assertIsHTMLScriptElement = function($o$$) {
    return goog.dom.asserts.assertIsElementType_($o$$, "HTMLScriptElement");
  };
  goog.dom.asserts.debugStringForType_ = function($value$$) {
    if (goog.isObject($value$$)) {
      try {
        return $value$$.constructor.displayName || $value$$.constructor.name || Object.prototype.toString.call($value$$);
      } catch ($e$$) {
        return "<object could not be stringified>";
      }
    } else {
      return void 0 === $value$$ ? "undefined" : null === $value$$ ? "null" : typeof $value$$;
    }
  };
  goog.dom.asserts.getWindow_ = function($o$$) {
    try {
      var $doc$$ = $o$$ && $o$$.ownerDocument, $win$$ = $doc$$ && ($doc$$.defaultView || $doc$$.parentWindow);
      $win$$ = $win$$ || goog.global;
      if ($win$$.Element && $win$$.Location) {
        return $win$$;
      }
    } catch ($ex$$) {
    }
    return null;
  };
  //[javascript/closure/functions/functions.js]
  goog.functions = {};
  goog.functions.constant = function($retValue$$) {
    return function() {
      return $retValue$$;
    };
  };
  goog.functions.FALSE = function() {
    return !1;
  };
  goog.functions.TRUE = function() {
    return !0;
  };
  goog.functions.NULL = function() {
    return null;
  };
  goog.functions.identity = function($opt_returnValue$$) {
    return $opt_returnValue$$;
  };
  goog.functions.error = function($message$$) {
    return function() {
      throw Error($message$$);
    };
  };
  goog.functions.fail = function($err$$) {
    return function() {
      throw $err$$;
    };
  };
  goog.functions.lock = function($f$$, $opt_numArgs$$) {
    $opt_numArgs$$ = $opt_numArgs$$ || 0;
    return function() {
      var $self$$ = this;
      return $f$$.apply($self$$, Array.prototype.slice.call(arguments, 0, $opt_numArgs$$));
    };
  };
  goog.functions.nth = function($n$$) {
    return function() {
      return arguments[$n$$];
    };
  };
  goog.functions.partialRight = function($fn$$, $var_args$$) {
    var $rightArgs$$ = Array.prototype.slice.call(arguments, 1);
    return function() {
      var $self$$ = this, $newArgs$$ = Array.prototype.slice.call(arguments);
      $newArgs$$.push.apply($newArgs$$, $rightArgs$$);
      return $fn$$.apply($self$$, $newArgs$$);
    };
  };
  goog.functions.withReturnValue = function($f$$, $retValue$$) {
    return goog.functions.sequence($f$$, goog.functions.constant($retValue$$));
  };
  goog.functions.equalTo = function($value$$, $opt_useLooseComparison$$) {
    return function($other$$) {
      return $opt_useLooseComparison$$ ? $value$$ == $other$$ : $value$$ === $other$$;
    };
  };
  goog.functions.compose = function($fn$$, $var_args$$) {
    var $functions$$ = arguments, $length$$ = $functions$$.length;
    return function() {
      var $self$$ = this, $result$$;
      $length$$ && ($result$$ = $functions$$[$length$$ - 1].apply($self$$, arguments));
      for (var $i$$ = $length$$ - 2; 0 <= $i$$; $i$$--) {
        $result$$ = $functions$$[$i$$].call($self$$, $result$$);
      }
      return $result$$;
    };
  };
  goog.functions.sequence = function($var_args$$) {
    var $functions$$ = arguments, $length$$ = $functions$$.length;
    return function() {
      for (var $self$$ = this, $result$$, $i$$ = 0; $i$$ < $length$$; $i$$++) {
        $result$$ = $functions$$[$i$$].apply($self$$, arguments);
      }
      return $result$$;
    };
  };
  goog.functions.and = function($var_args$$) {
    var $functions$$ = arguments, $length$$ = $functions$$.length;
    return function() {
      for (var $self$$ = this, $i$$ = 0; $i$$ < $length$$; $i$$++) {
        if (!$functions$$[$i$$].apply($self$$, arguments)) {
          return !1;
        }
      }
      return !0;
    };
  };
  goog.functions.or = function($var_args$$) {
    var $functions$$ = arguments, $length$$ = $functions$$.length;
    return function() {
      for (var $self$$ = this, $i$$ = 0; $i$$ < $length$$; $i$$++) {
        if ($functions$$[$i$$].apply($self$$, arguments)) {
          return !0;
        }
      }
      return !1;
    };
  };
  goog.functions.not = function($f$$) {
    return function() {
      var $self$$ = this;
      return !$f$$.apply($self$$, arguments);
    };
  };
  goog.functions.create = function($constructor$$, $var_args$$) {
    var $obj$$ = function() {
    };
    $obj$$.prototype = $constructor$$.prototype;
    $obj$$ = new $obj$$;
    $constructor$$.apply($obj$$, Array.prototype.slice.call(arguments, 1));
    return $obj$$;
  };
  goog.functions.CACHE_RETURN_VALUE = !0;
  goog.functions.cacheReturnValue = function($fn$$) {
    var $called$$ = !1, $value$$;
    return function() {
      if (!goog.functions.CACHE_RETURN_VALUE) {
        return $fn$$();
      }
      $called$$ || ($value$$ = $fn$$(), $called$$ = !0);
      return $value$$;
    };
  };
  goog.functions.once = function($f$$) {
    var $inner$$ = $f$$;
    return function() {
      if ($inner$$) {
        var $tmp$$ = $inner$$;
        $inner$$ = null;
        $tmp$$();
      }
    };
  };
  goog.functions.debounce = function($f$$, $interval$$, $opt_scope$$) {
    var $timeout$$ = 0;
    return function($var_args$$) {
      goog.global.clearTimeout($timeout$$);
      var $args$$ = arguments;
      $timeout$$ = goog.global.setTimeout(function() {
        $f$$.apply($opt_scope$$, $args$$);
      }, $interval$$);
    };
  };
  goog.functions.throttle = function($f$$, $interval$$, $opt_scope$$) {
    var $timeout$$ = 0, $shouldFire$$ = !1, $args$$ = [], $handleTimeout$$ = function() {
      $timeout$$ = 0;
      $shouldFire$$ && ($shouldFire$$ = !1, $fire$$());
    }, $fire$$ = function() {
      $timeout$$ = goog.global.setTimeout($handleTimeout$$, $interval$$);
      $f$$.apply($opt_scope$$, $args$$);
    };
    return function($var_args$$) {
      $args$$ = arguments;
      $timeout$$ ? $shouldFire$$ = !0 : $fire$$();
    };
  };
  goog.functions.rateLimit = function($f$$, $interval$$, $opt_scope$$) {
    var $timeout$$ = 0, $handleTimeout$$ = function() {
      $timeout$$ = 0;
    };
    return function($var_args$$) {
      $timeout$$ || ($timeout$$ = goog.global.setTimeout($handleTimeout$$, $interval$$), $f$$.apply($opt_scope$$, arguments));
    };
  };
  //[javascript/closure/dom/htmlelement.js]
  goog.dom.HtmlElement = function() {
  };
  //[javascript/closure/dom/tagname.js]
  goog.dom.TagName = function($tagName$$) {
    this.tagName_ = $tagName$$;
  };
  goog.dom.TagName.prototype.toString = function() {
    return this.tagName_;
  };
  goog.dom.TagName.A = new goog.dom.TagName("A");
  goog.dom.TagName.ABBR = new goog.dom.TagName("ABBR");
  goog.dom.TagName.ACRONYM = new goog.dom.TagName("ACRONYM");
  goog.dom.TagName.ADDRESS = new goog.dom.TagName("ADDRESS");
  goog.dom.TagName.APPLET = new goog.dom.TagName("APPLET");
  goog.dom.TagName.AREA = new goog.dom.TagName("AREA");
  goog.dom.TagName.ARTICLE = new goog.dom.TagName("ARTICLE");
  goog.dom.TagName.ASIDE = new goog.dom.TagName("ASIDE");
  goog.dom.TagName.AUDIO = new goog.dom.TagName("AUDIO");
  goog.dom.TagName.B = new goog.dom.TagName("B");
  goog.dom.TagName.BASE = new goog.dom.TagName("BASE");
  goog.dom.TagName.BASEFONT = new goog.dom.TagName("BASEFONT");
  goog.dom.TagName.BDI = new goog.dom.TagName("BDI");
  goog.dom.TagName.BDO = new goog.dom.TagName("BDO");
  goog.dom.TagName.BIG = new goog.dom.TagName("BIG");
  goog.dom.TagName.BLOCKQUOTE = new goog.dom.TagName("BLOCKQUOTE");
  goog.dom.TagName.BODY = new goog.dom.TagName("BODY");
  goog.dom.TagName.BR = new goog.dom.TagName("BR");
  goog.dom.TagName.BUTTON = new goog.dom.TagName("BUTTON");
  goog.dom.TagName.CANVAS = new goog.dom.TagName("CANVAS");
  goog.dom.TagName.CAPTION = new goog.dom.TagName("CAPTION");
  goog.dom.TagName.CENTER = new goog.dom.TagName("CENTER");
  goog.dom.TagName.CITE = new goog.dom.TagName("CITE");
  goog.dom.TagName.CODE = new goog.dom.TagName("CODE");
  goog.dom.TagName.COL = new goog.dom.TagName("COL");
  goog.dom.TagName.COLGROUP = new goog.dom.TagName("COLGROUP");
  goog.dom.TagName.COMMAND = new goog.dom.TagName("COMMAND");
  goog.dom.TagName.DATA = new goog.dom.TagName("DATA");
  goog.dom.TagName.DATALIST = new goog.dom.TagName("DATALIST");
  goog.dom.TagName.DD = new goog.dom.TagName("DD");
  goog.dom.TagName.DEL = new goog.dom.TagName("DEL");
  goog.dom.TagName.DETAILS = new goog.dom.TagName("DETAILS");
  goog.dom.TagName.DFN = new goog.dom.TagName("DFN");
  goog.dom.TagName.DIALOG = new goog.dom.TagName("DIALOG");
  goog.dom.TagName.DIR = new goog.dom.TagName("DIR");
  goog.dom.TagName.DIV = new goog.dom.TagName("DIV");
  goog.dom.TagName.DL = new goog.dom.TagName("DL");
  goog.dom.TagName.DT = new goog.dom.TagName("DT");
  goog.dom.TagName.EM = new goog.dom.TagName("EM");
  goog.dom.TagName.EMBED = new goog.dom.TagName("EMBED");
  goog.dom.TagName.FIELDSET = new goog.dom.TagName("FIELDSET");
  goog.dom.TagName.FIGCAPTION = new goog.dom.TagName("FIGCAPTION");
  goog.dom.TagName.FIGURE = new goog.dom.TagName("FIGURE");
  goog.dom.TagName.FONT = new goog.dom.TagName("FONT");
  goog.dom.TagName.FOOTER = new goog.dom.TagName("FOOTER");
  goog.dom.TagName.FORM = new goog.dom.TagName("FORM");
  goog.dom.TagName.FRAME = new goog.dom.TagName("FRAME");
  goog.dom.TagName.FRAMESET = new goog.dom.TagName("FRAMESET");
  goog.dom.TagName.H1 = new goog.dom.TagName("H1");
  goog.dom.TagName.H2 = new goog.dom.TagName("H2");
  goog.dom.TagName.H3 = new goog.dom.TagName("H3");
  goog.dom.TagName.H4 = new goog.dom.TagName("H4");
  goog.dom.TagName.H5 = new goog.dom.TagName("H5");
  goog.dom.TagName.H6 = new goog.dom.TagName("H6");
  goog.dom.TagName.HEAD = new goog.dom.TagName("HEAD");
  goog.dom.TagName.HEADER = new goog.dom.TagName("HEADER");
  goog.dom.TagName.HGROUP = new goog.dom.TagName("HGROUP");
  goog.dom.TagName.HR = new goog.dom.TagName("HR");
  goog.dom.TagName.HTML = new goog.dom.TagName("HTML");
  goog.dom.TagName.I = new goog.dom.TagName("I");
  goog.dom.TagName.IFRAME = new goog.dom.TagName("IFRAME");
  goog.dom.TagName.IMG = new goog.dom.TagName("IMG");
  goog.dom.TagName.INPUT = new goog.dom.TagName("INPUT");
  goog.dom.TagName.INS = new goog.dom.TagName("INS");
  goog.dom.TagName.ISINDEX = new goog.dom.TagName("ISINDEX");
  goog.dom.TagName.KBD = new goog.dom.TagName("KBD");
  goog.dom.TagName.KEYGEN = new goog.dom.TagName("KEYGEN");
  goog.dom.TagName.LABEL = new goog.dom.TagName("LABEL");
  goog.dom.TagName.LEGEND = new goog.dom.TagName("LEGEND");
  goog.dom.TagName.LI = new goog.dom.TagName("LI");
  goog.dom.TagName.LINK = new goog.dom.TagName("LINK");
  goog.dom.TagName.MAIN = new goog.dom.TagName("MAIN");
  goog.dom.TagName.MAP = new goog.dom.TagName("MAP");
  goog.dom.TagName.MARK = new goog.dom.TagName("MARK");
  goog.dom.TagName.MATH = new goog.dom.TagName("MATH");
  goog.dom.TagName.MENU = new goog.dom.TagName("MENU");
  goog.dom.TagName.MENUITEM = new goog.dom.TagName("MENUITEM");
  goog.dom.TagName.META = new goog.dom.TagName("META");
  goog.dom.TagName.METER = new goog.dom.TagName("METER");
  goog.dom.TagName.NAV = new goog.dom.TagName("NAV");
  goog.dom.TagName.NOFRAMES = new goog.dom.TagName("NOFRAMES");
  goog.dom.TagName.NOSCRIPT = new goog.dom.TagName("NOSCRIPT");
  goog.dom.TagName.OBJECT = new goog.dom.TagName("OBJECT");
  goog.dom.TagName.OL = new goog.dom.TagName("OL");
  goog.dom.TagName.OPTGROUP = new goog.dom.TagName("OPTGROUP");
  goog.dom.TagName.OPTION = new goog.dom.TagName("OPTION");
  goog.dom.TagName.OUTPUT = new goog.dom.TagName("OUTPUT");
  goog.dom.TagName.P = new goog.dom.TagName("P");
  goog.dom.TagName.PARAM = new goog.dom.TagName("PARAM");
  goog.dom.TagName.PICTURE = new goog.dom.TagName("PICTURE");
  goog.dom.TagName.PRE = new goog.dom.TagName("PRE");
  goog.dom.TagName.PROGRESS = new goog.dom.TagName("PROGRESS");
  goog.dom.TagName.Q = new goog.dom.TagName("Q");
  goog.dom.TagName.RP = new goog.dom.TagName("RP");
  goog.dom.TagName.RT = new goog.dom.TagName("RT");
  goog.dom.TagName.RTC = new goog.dom.TagName("RTC");
  goog.dom.TagName.RUBY = new goog.dom.TagName("RUBY");
  goog.dom.TagName.S = new goog.dom.TagName("S");
  goog.dom.TagName.SAMP = new goog.dom.TagName("SAMP");
  goog.dom.TagName.SCRIPT = new goog.dom.TagName("SCRIPT");
  goog.dom.TagName.SECTION = new goog.dom.TagName("SECTION");
  goog.dom.TagName.SELECT = new goog.dom.TagName("SELECT");
  goog.dom.TagName.SMALL = new goog.dom.TagName("SMALL");
  goog.dom.TagName.SOURCE = new goog.dom.TagName("SOURCE");
  goog.dom.TagName.SPAN = new goog.dom.TagName("SPAN");
  goog.dom.TagName.STRIKE = new goog.dom.TagName("STRIKE");
  goog.dom.TagName.STRONG = new goog.dom.TagName("STRONG");
  goog.dom.TagName.STYLE = new goog.dom.TagName("STYLE");
  goog.dom.TagName.SUB = new goog.dom.TagName("SUB");
  goog.dom.TagName.SUMMARY = new goog.dom.TagName("SUMMARY");
  goog.dom.TagName.SUP = new goog.dom.TagName("SUP");
  goog.dom.TagName.SVG = new goog.dom.TagName("SVG");
  goog.dom.TagName.TABLE = new goog.dom.TagName("TABLE");
  goog.dom.TagName.TBODY = new goog.dom.TagName("TBODY");
  goog.dom.TagName.TD = new goog.dom.TagName("TD");
  goog.dom.TagName.TEMPLATE = new goog.dom.TagName("TEMPLATE");
  goog.dom.TagName.TEXTAREA = new goog.dom.TagName("TEXTAREA");
  goog.dom.TagName.TFOOT = new goog.dom.TagName("TFOOT");
  goog.dom.TagName.TH = new goog.dom.TagName("TH");
  goog.dom.TagName.THEAD = new goog.dom.TagName("THEAD");
  goog.dom.TagName.TIME = new goog.dom.TagName("TIME");
  goog.dom.TagName.TITLE = new goog.dom.TagName("TITLE");
  goog.dom.TagName.TR = new goog.dom.TagName("TR");
  goog.dom.TagName.TRACK = new goog.dom.TagName("TRACK");
  goog.dom.TagName.TT = new goog.dom.TagName("TT");
  goog.dom.TagName.U = new goog.dom.TagName("U");
  goog.dom.TagName.UL = new goog.dom.TagName("UL");
  goog.dom.TagName.VAR = new goog.dom.TagName("VAR");
  goog.dom.TagName.VIDEO = new goog.dom.TagName("VIDEO");
  goog.dom.TagName.WBR = new goog.dom.TagName("WBR");
  //[javascript/closure/dom/tags.js]
  goog.dom.tags = {};
  goog.dom.tags.VOID_TAGS_ = {area:!0, base:!0, br:!0, col:!0, command:!0, embed:!0, hr:!0, img:!0, input:!0, keygen:!0, link:!0, meta:!0, param:!0, source:!0, track:!0, wbr:!0};
  goog.dom.tags.isVoidTag = function($tagName$$) {
    return !0 === goog.dom.tags.VOID_TAGS_[$tagName$$];
  };
  //[javascript/closure/html/trustedtypes.js]
  goog.html = {};
  goog.html.trustedtypes = {};
  goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY = goog.TRUSTED_TYPES_POLICY_NAME ? goog.createTrustedTypesPolicy(goog.TRUSTED_TYPES_POLICY_NAME + "#html") : null;
  //[javascript/closure/string/typedstring.js]
  goog.string.TypedString = function() {
  };
  //[javascript/closure/string/const.js]
  goog.string.Const = function($opt_token$$, $opt_content$$) {
    this.stringConstValueWithSecurityContract__googStringSecurityPrivate_ = $opt_token$$ === goog.string.Const.GOOG_STRING_CONSTRUCTOR_TOKEN_PRIVATE_ && $opt_content$$ || "";
    this.STRING_CONST_TYPE_MARKER__GOOG_STRING_SECURITY_PRIVATE_ = goog.string.Const.TYPE_MARKER_;
  };
  goog.string.Const.prototype.implementsGoogStringTypedString = !0;
  goog.string.Const.prototype.getTypedStringValue = function() {
    return this.stringConstValueWithSecurityContract__googStringSecurityPrivate_;
  };
  goog.string.Const.prototype.toString = function() {
    return "Const{" + this.stringConstValueWithSecurityContract__googStringSecurityPrivate_ + "}";
  };
  goog.string.Const.unwrap = function($stringConst$$) {
    if ($stringConst$$ instanceof goog.string.Const && $stringConst$$.constructor === goog.string.Const && $stringConst$$.STRING_CONST_TYPE_MARKER__GOOG_STRING_SECURITY_PRIVATE_ === goog.string.Const.TYPE_MARKER_) {
      return $stringConst$$.stringConstValueWithSecurityContract__googStringSecurityPrivate_;
    }
    goog.asserts.fail("expected object of type Const, got '" + $stringConst$$ + "'");
    return "type_error:Const";
  };
  goog.string.Const.from = function($s$$) {
    return new goog.string.Const(goog.string.Const.GOOG_STRING_CONSTRUCTOR_TOKEN_PRIVATE_, $s$$);
  };
  goog.string.Const.TYPE_MARKER_ = {};
  goog.string.Const.GOOG_STRING_CONSTRUCTOR_TOKEN_PRIVATE_ = {};
  goog.string.Const.EMPTY = goog.string.Const.from("");
  //[javascript/closure/html/safescript.js]
  goog.html.SafeScript = function() {
    this.privateDoNotAccessOrElseSafeScriptWrappedValue_ = "";
    this.SAFE_SCRIPT_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = goog.html.SafeScript.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_;
  };
  goog.html.SafeScript.prototype.implementsGoogStringTypedString = !0;
  goog.html.SafeScript.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = {};
  goog.html.SafeScript.fromConstant = function($script$$) {
    $script$$ = goog.string.Const.unwrap($script$$);
    return 0 === $script$$.length ? goog.html.SafeScript.EMPTY : goog.html.SafeScript.createSafeScriptSecurityPrivateDoNotAccessOrElse($script$$);
  };
  goog.html.SafeScript.fromConstantAndArgs = function($code$$, $var_args$$) {
    for (var $args$$ = [], $i$$ = 1; $i$$ < arguments.length; $i$$++) {
      $args$$.push(goog.html.SafeScript.stringify_(arguments[$i$$]));
    }
    return goog.html.SafeScript.createSafeScriptSecurityPrivateDoNotAccessOrElse("(" + goog.string.Const.unwrap($code$$) + ")(" + $args$$.join(", ") + ");");
  };
  goog.html.SafeScript.fromJson = function($val$$) {
    return goog.html.SafeScript.createSafeScriptSecurityPrivateDoNotAccessOrElse(goog.html.SafeScript.stringify_($val$$));
  };
  goog.html.SafeScript.prototype.getTypedStringValue = function() {
    return this.privateDoNotAccessOrElseSafeScriptWrappedValue_.toString();
  };
  goog.DEBUG && (goog.html.SafeScript.prototype.toString = function() {
    return "SafeScript{" + this.privateDoNotAccessOrElseSafeScriptWrappedValue_ + "}";
  });
  goog.html.SafeScript.unwrap = function($safeScript$$) {
    return goog.html.SafeScript.unwrapTrustedScript($safeScript$$).toString();
  };
  goog.html.SafeScript.unwrapTrustedScript = function($safeScript$$) {
    if ($safeScript$$ instanceof goog.html.SafeScript && $safeScript$$.constructor === goog.html.SafeScript && $safeScript$$.SAFE_SCRIPT_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ === goog.html.SafeScript.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_) {
      return $safeScript$$.privateDoNotAccessOrElseSafeScriptWrappedValue_;
    }
    goog.asserts.fail("expected object of type SafeScript, got '" + $safeScript$$ + "' of type " + goog.typeOf($safeScript$$));
    return "type_error:SafeScript";
  };
  goog.html.SafeScript.stringify_ = function($json_val$$) {
    $json_val$$ = JSON.stringify($json_val$$);
    return $json_val$$.replace(/</g, "\\x3c");
  };
  goog.html.SafeScript.createSafeScriptSecurityPrivateDoNotAccessOrElse = function($script$$) {
    return (new goog.html.SafeScript).initSecurityPrivateDoNotAccessOrElse_($script$$);
  };
  goog.html.SafeScript.prototype.initSecurityPrivateDoNotAccessOrElse_ = function($script$$) {
    this.privateDoNotAccessOrElseSafeScriptWrappedValue_ = goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY ? goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY.createScript($script$$) : $script$$;
    return this;
  };
  goog.html.SafeScript.EMPTY = goog.html.SafeScript.createSafeScriptSecurityPrivateDoNotAccessOrElse("");
  //[javascript/closure/fs/url.js]
  goog.fs = {};
  goog.fs.url = {};
  goog.fs.url.createObjectUrl = function($blob$$) {
    return goog.fs.url.getUrlObject_().createObjectURL($blob$$);
  };
  goog.fs.url.revokeObjectUrl = function($url$$) {
    goog.fs.url.getUrlObject_().revokeObjectURL($url$$);
  };
  goog.fs.url.getUrlObject_ = function() {
    var $urlObject$$ = goog.fs.url.findUrlObject_();
    if (null != $urlObject$$) {
      return $urlObject$$;
    }
    throw Error("This browser doesn't seem to support blob URLs");
  };
  goog.fs.url.findUrlObject_ = function() {
    return goog.isDef(goog.global.URL) && goog.isDef(goog.global.URL.createObjectURL) ? goog.global.URL : goog.isDef(goog.global.webkitURL) && goog.isDef(goog.global.webkitURL.createObjectURL) ? goog.global.webkitURL : goog.isDef(goog.global.createObjectURL) ? goog.global : null;
  };
  goog.fs.url.browserSupportsObjectUrls = function() {
    return null != goog.fs.url.findUrlObject_();
  };
  //[javascript/closure/i18n/bidi.js]
  goog.i18n = {};
  goog.i18n.bidi = {};
  goog.i18n.bidi.FORCE_RTL = !1;
  goog.i18n.bidi.IS_RTL = goog.i18n.bidi.FORCE_RTL || ("ar" == goog.LOCALE.substring(0, 2).toLowerCase() || "fa" == goog.LOCALE.substring(0, 2).toLowerCase() || "he" == goog.LOCALE.substring(0, 2).toLowerCase() || "iw" == goog.LOCALE.substring(0, 2).toLowerCase() || "ps" == goog.LOCALE.substring(0, 2).toLowerCase() || "sd" == goog.LOCALE.substring(0, 2).toLowerCase() || "ug" == goog.LOCALE.substring(0, 2).toLowerCase() || "ur" == goog.LOCALE.substring(0, 2).toLowerCase() || "yi" == goog.LOCALE.substring(0,
  2).toLowerCase()) && (2 == goog.LOCALE.length || "-" == goog.LOCALE.substring(2, 3) || "_" == goog.LOCALE.substring(2, 3)) || 3 <= goog.LOCALE.length && "ckb" == goog.LOCALE.substring(0, 3).toLowerCase() && (3 == goog.LOCALE.length || "-" == goog.LOCALE.substring(3, 4) || "_" == goog.LOCALE.substring(3, 4)) || 7 <= goog.LOCALE.length && ("-" == goog.LOCALE.substring(2, 3) || "_" == goog.LOCALE.substring(2, 3)) && ("adlm" == goog.LOCALE.substring(3, 7).toLowerCase() || "arab" == goog.LOCALE.substring(3,
  7).toLowerCase() || "hebr" == goog.LOCALE.substring(3, 7).toLowerCase() || "nkoo" == goog.LOCALE.substring(3, 7).toLowerCase() || "rohg" == goog.LOCALE.substring(3, 7).toLowerCase() || "thaa" == goog.LOCALE.substring(3, 7).toLowerCase()) || 8 <= goog.LOCALE.length && ("-" == goog.LOCALE.substring(3, 4) || "_" == goog.LOCALE.substring(3, 4)) && ("adlm" == goog.LOCALE.substring(4, 8).toLowerCase() || "arab" == goog.LOCALE.substring(4, 8).toLowerCase() || "hebr" == goog.LOCALE.substring(4, 8).toLowerCase() ||
  "nkoo" == goog.LOCALE.substring(4, 8).toLowerCase() || "rohg" == goog.LOCALE.substring(4, 8).toLowerCase() || "thaa" == goog.LOCALE.substring(4, 8).toLowerCase());
  goog.i18n.bidi.Format = {LRE:"\u202a", RLE:"\u202b", PDF:"\u202c", LRM:"\u200e", RLM:"\u200f"};
  goog.i18n.bidi.Dir = {LTR:1, RTL:-1, NEUTRAL:0};
  goog.i18n.bidi.RIGHT = "right";
  goog.i18n.bidi.LEFT = "left";
  goog.i18n.bidi.I18N_RIGHT = goog.i18n.bidi.IS_RTL ? goog.i18n.bidi.LEFT : goog.i18n.bidi.RIGHT;
  goog.i18n.bidi.I18N_LEFT = goog.i18n.bidi.IS_RTL ? goog.i18n.bidi.RIGHT : goog.i18n.bidi.LEFT;
  goog.i18n.bidi.toDir = function($givenDir$$, $opt_noNeutral$$) {
    return "number" == typeof $givenDir$$ ? 0 < $givenDir$$ ? goog.i18n.bidi.Dir.LTR : 0 > $givenDir$$ ? goog.i18n.bidi.Dir.RTL : $opt_noNeutral$$ ? null : goog.i18n.bidi.Dir.NEUTRAL : null == $givenDir$$ ? null : $givenDir$$ ? goog.i18n.bidi.Dir.RTL : goog.i18n.bidi.Dir.LTR;
  };
  goog.i18n.bidi.ltrChars_ = "A-Za-z\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u02b8\u0300-\u0590\u0900-\u1fff\u200e\u2c00-\ud801\ud804-\ud839\ud83c-\udbff\uf900-\ufb1c\ufe00-\ufe6f\ufefd-\uffff";
  goog.i18n.bidi.rtlChars_ = "\u0591-\u06ef\u06fa-\u08ff\u200f\ud802-\ud803\ud83a-\ud83b\ufb1d-\ufdff\ufe70-\ufefc";
  goog.i18n.bidi.htmlSkipReg_ = /<[^>]*>|&[^;]+;/g;
  goog.i18n.bidi.stripHtmlIfNeeded_ = function($str$$, $opt_isStripNeeded$$) {
    return $opt_isStripNeeded$$ ? $str$$.replace(goog.i18n.bidi.htmlSkipReg_, "") : $str$$;
  };
  goog.i18n.bidi.rtlCharReg_ = new RegExp("[" + goog.i18n.bidi.rtlChars_ + "]");
  goog.i18n.bidi.ltrCharReg_ = new RegExp("[" + goog.i18n.bidi.ltrChars_ + "]");
  goog.i18n.bidi.hasAnyRtl = function($str$$, $opt_isHtml$$) {
    return goog.i18n.bidi.rtlCharReg_.test(goog.i18n.bidi.stripHtmlIfNeeded_($str$$, $opt_isHtml$$));
  };
  goog.i18n.bidi.hasRtlChar = goog.i18n.bidi.hasAnyRtl;
  goog.i18n.bidi.hasAnyLtr = function($str$$, $opt_isHtml$$) {
    return goog.i18n.bidi.ltrCharReg_.test(goog.i18n.bidi.stripHtmlIfNeeded_($str$$, $opt_isHtml$$));
  };
  goog.i18n.bidi.ltrRe_ = new RegExp("^[" + goog.i18n.bidi.ltrChars_ + "]");
  goog.i18n.bidi.rtlRe_ = new RegExp("^[" + goog.i18n.bidi.rtlChars_ + "]");
  goog.i18n.bidi.isRtlChar = function($str$$) {
    return goog.i18n.bidi.rtlRe_.test($str$$);
  };
  goog.i18n.bidi.isLtrChar = function($str$$) {
    return goog.i18n.bidi.ltrRe_.test($str$$);
  };
  goog.i18n.bidi.isNeutralChar = function($str$$) {
    return !goog.i18n.bidi.isLtrChar($str$$) && !goog.i18n.bidi.isRtlChar($str$$);
  };
  goog.i18n.bidi.ltrDirCheckRe_ = new RegExp("^[^" + goog.i18n.bidi.rtlChars_ + "]*[" + goog.i18n.bidi.ltrChars_ + "]");
  goog.i18n.bidi.rtlDirCheckRe_ = new RegExp("^[^" + goog.i18n.bidi.ltrChars_ + "]*[" + goog.i18n.bidi.rtlChars_ + "]");
  goog.i18n.bidi.startsWithRtl = function($str$$, $opt_isHtml$$) {
    return goog.i18n.bidi.rtlDirCheckRe_.test(goog.i18n.bidi.stripHtmlIfNeeded_($str$$, $opt_isHtml$$));
  };
  goog.i18n.bidi.isRtlText = goog.i18n.bidi.startsWithRtl;
  goog.i18n.bidi.startsWithLtr = function($str$$, $opt_isHtml$$) {
    return goog.i18n.bidi.ltrDirCheckRe_.test(goog.i18n.bidi.stripHtmlIfNeeded_($str$$, $opt_isHtml$$));
  };
  goog.i18n.bidi.isLtrText = goog.i18n.bidi.startsWithLtr;
  goog.i18n.bidi.isRequiredLtrRe_ = /^http:\/\/.*/;
  goog.i18n.bidi.isNeutralText = function($str$$, $opt_isHtml$$) {
    $str$$ = goog.i18n.bidi.stripHtmlIfNeeded_($str$$, $opt_isHtml$$);
    return goog.i18n.bidi.isRequiredLtrRe_.test($str$$) || !goog.i18n.bidi.hasAnyLtr($str$$) && !goog.i18n.bidi.hasAnyRtl($str$$);
  };
  goog.i18n.bidi.ltrExitDirCheckRe_ = new RegExp("[" + goog.i18n.bidi.ltrChars_ + "][^" + goog.i18n.bidi.rtlChars_ + "]*$");
  goog.i18n.bidi.rtlExitDirCheckRe_ = new RegExp("[" + goog.i18n.bidi.rtlChars_ + "][^" + goog.i18n.bidi.ltrChars_ + "]*$");
  goog.i18n.bidi.endsWithLtr = function($str$$, $opt_isHtml$$) {
    return goog.i18n.bidi.ltrExitDirCheckRe_.test(goog.i18n.bidi.stripHtmlIfNeeded_($str$$, $opt_isHtml$$));
  };
  goog.i18n.bidi.isLtrExitText = goog.i18n.bidi.endsWithLtr;
  goog.i18n.bidi.endsWithRtl = function($str$$, $opt_isHtml$$) {
    return goog.i18n.bidi.rtlExitDirCheckRe_.test(goog.i18n.bidi.stripHtmlIfNeeded_($str$$, $opt_isHtml$$));
  };
  goog.i18n.bidi.isRtlExitText = goog.i18n.bidi.endsWithRtl;
  goog.i18n.bidi.rtlLocalesRe_ = /^(ar|ckb|dv|he|iw|fa|nqo|ps|sd|ug|ur|yi|.*[-_](Adlm|Arab|Hebr|Nkoo|Rohg|Thaa))(?!.*[-_](Latn|Cyrl)($|-|_))($|-|_)/i;
  goog.i18n.bidi.isRtlLanguage = function($lang$$) {
    return goog.i18n.bidi.rtlLocalesRe_.test($lang$$);
  };
  goog.i18n.bidi.bracketGuardTextRe_ = /(\(.*?\)+)|(\[.*?\]+)|(\{.*?\}+)|(<.*?>+)/g;
  goog.i18n.bidi.guardBracketInText = function($s$$, $mark_opt_isRtlContext_useRtl$$) {
    $mark_opt_isRtlContext_useRtl$$ = ($mark_opt_isRtlContext_useRtl$$ = void 0 === $mark_opt_isRtlContext_useRtl$$ ? goog.i18n.bidi.hasAnyRtl($s$$) : $mark_opt_isRtlContext_useRtl$$) ? goog.i18n.bidi.Format.RLM : goog.i18n.bidi.Format.LRM;
    return $s$$.replace(goog.i18n.bidi.bracketGuardTextRe_, $mark_opt_isRtlContext_useRtl$$ + "$&" + $mark_opt_isRtlContext_useRtl$$);
  };
  goog.i18n.bidi.enforceRtlInHtml = function($html$$) {
    return "<" == $html$$.charAt(0) ? $html$$.replace(/<\w+/, "$& dir=rtl") : "\n<span dir=rtl>" + $html$$ + "</span>";
  };
  goog.i18n.bidi.enforceRtlInText = function($text$$) {
    return goog.i18n.bidi.Format.RLE + $text$$ + goog.i18n.bidi.Format.PDF;
  };
  goog.i18n.bidi.enforceLtrInHtml = function($html$$) {
    return "<" == $html$$.charAt(0) ? $html$$.replace(/<\w+/, "$& dir=ltr") : "\n<span dir=ltr>" + $html$$ + "</span>";
  };
  goog.i18n.bidi.enforceLtrInText = function($text$$) {
    return goog.i18n.bidi.Format.LRE + $text$$ + goog.i18n.bidi.Format.PDF;
  };
  goog.i18n.bidi.dimensionsRe_ = /:\s*([.\d][.\w]*)\s+([.\d][.\w]*)\s+([.\d][.\w]*)\s+([.\d][.\w]*)/g;
  goog.i18n.bidi.leftRe_ = /left/gi;
  goog.i18n.bidi.rightRe_ = /right/gi;
  goog.i18n.bidi.tempRe_ = /%%%%/g;
  goog.i18n.bidi.mirrorCSS = function($cssStr$$) {
    return $cssStr$$.replace(goog.i18n.bidi.dimensionsRe_, ":$1 $4 $3 $2").replace(goog.i18n.bidi.leftRe_, "%%%%").replace(goog.i18n.bidi.rightRe_, goog.i18n.bidi.LEFT).replace(goog.i18n.bidi.tempRe_, goog.i18n.bidi.RIGHT);
  };
  goog.i18n.bidi.doubleQuoteSubstituteRe_ = /([\u0591-\u05f2])"/g;
  goog.i18n.bidi.singleQuoteSubstituteRe_ = /([\u0591-\u05f2])'/g;
  goog.i18n.bidi.normalizeHebrewQuote = function($str$$) {
    return $str$$.replace(goog.i18n.bidi.doubleQuoteSubstituteRe_, "$1\u05f4").replace(goog.i18n.bidi.singleQuoteSubstituteRe_, "$1\u05f3");
  };
  goog.i18n.bidi.wordSeparatorRe_ = /\s+/;
  goog.i18n.bidi.hasNumeralsRe_ = /[\d\u06f0-\u06f9]/;
  goog.i18n.bidi.rtlDetectionThreshold_ = 0.40;
  goog.i18n.bidi.estimateDirection = function($str$$, $i$jscomp$70_opt_isHtml$$) {
    var $rtlCount$$ = 0, $totalCount$$ = 0, $hasWeaklyLtr$$ = !1;
    $str$$ = goog.i18n.bidi.stripHtmlIfNeeded_($str$$, $i$jscomp$70_opt_isHtml$$).split(goog.i18n.bidi.wordSeparatorRe_);
    for ($i$jscomp$70_opt_isHtml$$ = 0; $i$jscomp$70_opt_isHtml$$ < $str$$.length; $i$jscomp$70_opt_isHtml$$++) {
      var $token$$ = $str$$[$i$jscomp$70_opt_isHtml$$];
      goog.i18n.bidi.startsWithRtl($token$$) ? ($rtlCount$$++, $totalCount$$++) : goog.i18n.bidi.isRequiredLtrRe_.test($token$$) ? $hasWeaklyLtr$$ = !0 : goog.i18n.bidi.hasAnyLtr($token$$) ? $totalCount$$++ : goog.i18n.bidi.hasNumeralsRe_.test($token$$) && ($hasWeaklyLtr$$ = !0);
    }
    return 0 == $totalCount$$ ? $hasWeaklyLtr$$ ? goog.i18n.bidi.Dir.LTR : goog.i18n.bidi.Dir.NEUTRAL : $rtlCount$$ / $totalCount$$ > goog.i18n.bidi.rtlDetectionThreshold_ ? goog.i18n.bidi.Dir.RTL : goog.i18n.bidi.Dir.LTR;
  };
  goog.i18n.bidi.detectRtlDirectionality = function($str$$, $opt_isHtml$$) {
    return goog.i18n.bidi.estimateDirection($str$$, $opt_isHtml$$) == goog.i18n.bidi.Dir.RTL;
  };
  goog.i18n.bidi.setElementDirAndAlign = function($element$$, $dir$$) {
    $element$$ && ($dir$$ = goog.i18n.bidi.toDir($dir$$)) && ($element$$.style.textAlign = $dir$$ == goog.i18n.bidi.Dir.RTL ? goog.i18n.bidi.RIGHT : goog.i18n.bidi.LEFT, $element$$.dir = $dir$$ == goog.i18n.bidi.Dir.RTL ? "rtl" : "ltr");
  };
  goog.i18n.bidi.setElementDirByTextDirectionality = function($element$jscomp$12_htmlElement$$, $text$$) {
    switch(goog.i18n.bidi.estimateDirection($text$$)) {
      case goog.i18n.bidi.Dir.LTR:
        $element$jscomp$12_htmlElement$$.dir = "ltr";
        break;
      case goog.i18n.bidi.Dir.RTL:
        $element$jscomp$12_htmlElement$$.dir = "rtl";
        break;
      default:
        $element$jscomp$12_htmlElement$$.removeAttribute("dir");
    }
  };
  goog.i18n.bidi.DirectionalString = function() {
  };
  //[javascript/closure/html/trustedresourceurl.js]
  goog.html.TrustedResourceUrl = function() {
    this.privateDoNotAccessOrElseTrustedResourceUrlWrappedValue_ = "";
    this.trustedURL_ = null;
    this.TRUSTED_RESOURCE_URL_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = goog.html.TrustedResourceUrl.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_;
  };
  goog.html.TrustedResourceUrl.prototype.implementsGoogStringTypedString = !0;
  goog.html.TrustedResourceUrl.prototype.getTypedStringValue = function() {
    return this.privateDoNotAccessOrElseTrustedResourceUrlWrappedValue_.toString();
  };
  goog.html.TrustedResourceUrl.prototype.implementsGoogI18nBidiDirectionalString = !0;
  goog.html.TrustedResourceUrl.prototype.getDirection = function() {
    return goog.i18n.bidi.Dir.LTR;
  };
  goog.html.TrustedResourceUrl.prototype.cloneWithParams = function($searchParams$$, $opt_hashParams$$) {
    var $url$$ = goog.html.TrustedResourceUrl.unwrap(this), $parts$$ = goog.html.TrustedResourceUrl.URL_PARAM_PARSER_.exec($url$$);
    $url$$ = $parts$$[1];
    var $urlSearch$$ = $parts$$[2] || "";
    $parts$$ = $parts$$[3] || "";
    return goog.html.TrustedResourceUrl.createTrustedResourceUrlSecurityPrivateDoNotAccessOrElse($url$$ + goog.html.TrustedResourceUrl.stringifyParams_("?", $urlSearch$$, $searchParams$$) + goog.html.TrustedResourceUrl.stringifyParams_("#", $parts$$, $opt_hashParams$$));
  };
  goog.DEBUG && (goog.html.TrustedResourceUrl.prototype.toString = function() {
    return "TrustedResourceUrl{" + this.privateDoNotAccessOrElseTrustedResourceUrlWrappedValue_ + "}";
  });
  goog.html.TrustedResourceUrl.unwrap = function($trustedResourceUrl$$) {
    return goog.html.TrustedResourceUrl.unwrapTrustedScriptURL($trustedResourceUrl$$).toString();
  };
  goog.html.TrustedResourceUrl.unwrapTrustedScriptURL = function($trustedResourceUrl$$) {
    if ($trustedResourceUrl$$ instanceof goog.html.TrustedResourceUrl && $trustedResourceUrl$$.constructor === goog.html.TrustedResourceUrl && $trustedResourceUrl$$.TRUSTED_RESOURCE_URL_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ === goog.html.TrustedResourceUrl.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_) {
      return $trustedResourceUrl$$.privateDoNotAccessOrElseTrustedResourceUrlWrappedValue_;
    }
    goog.asserts.fail("expected object of type TrustedResourceUrl, got '" + $trustedResourceUrl$$ + "' of type " + goog.typeOf($trustedResourceUrl$$));
    return "type_error:TrustedResourceUrl";
  };
  goog.html.TrustedResourceUrl.unwrapTrustedURL = function($trustedResourceUrl$$) {
    return $trustedResourceUrl$$.trustedURL_ ? $trustedResourceUrl$$.trustedURL_ : goog.html.TrustedResourceUrl.unwrap($trustedResourceUrl$$);
  };
  goog.html.TrustedResourceUrl.format = function($format$jscomp$16_result$$, $args$$) {
    var $formatStr$$ = goog.string.Const.unwrap($format$jscomp$16_result$$);
    if (!goog.html.TrustedResourceUrl.BASE_URL_.test($formatStr$$)) {
      throw Error("Invalid TrustedResourceUrl format: " + $formatStr$$);
    }
    $format$jscomp$16_result$$ = $formatStr$$.replace(goog.html.TrustedResourceUrl.FORMAT_MARKER_, function($arg$jscomp$7_match$$, $id$$) {
      if (!Object.prototype.hasOwnProperty.call($args$$, $id$$)) {
        throw Error('Found marker, "' + $id$$ + '", in format string, "' + $formatStr$$ + '", but no valid label mapping found in args: ' + JSON.stringify($args$$));
      }
      $arg$jscomp$7_match$$ = $args$$[$id$$];
      return $arg$jscomp$7_match$$ instanceof goog.string.Const ? goog.string.Const.unwrap($arg$jscomp$7_match$$) : encodeURIComponent(String($arg$jscomp$7_match$$));
    });
    return goog.html.TrustedResourceUrl.createTrustedResourceUrlSecurityPrivateDoNotAccessOrElse($format$jscomp$16_result$$);
  };
  goog.html.TrustedResourceUrl.FORMAT_MARKER_ = /%{(\w+)}/g;
  goog.html.TrustedResourceUrl.BASE_URL_ = /^((https:)?\/\/[0-9a-z.:[\]-]+\/|\/[^/\\]|[^:/\\%]+\/|[^:/\\%]*[?#]|about:blank#)/i;
  goog.html.TrustedResourceUrl.URL_PARAM_PARSER_ = /^([^?#]*)(\?[^#]*)?(#[\s\S]*)?/;
  goog.html.TrustedResourceUrl.formatWithParams = function($format$jscomp$17_url$$, $args$$, $searchParams$$, $opt_hashParams$$) {
    $format$jscomp$17_url$$ = goog.html.TrustedResourceUrl.format($format$jscomp$17_url$$, $args$$);
    return $format$jscomp$17_url$$.cloneWithParams($searchParams$$, $opt_hashParams$$);
  };
  goog.html.TrustedResourceUrl.fromConstant = function($url$$) {
    return goog.html.TrustedResourceUrl.createTrustedResourceUrlSecurityPrivateDoNotAccessOrElse(goog.string.Const.unwrap($url$$));
  };
  goog.html.TrustedResourceUrl.fromConstants = function($parts$$) {
    for (var $unwrapped$$ = "", $i$$ = 0; $i$$ < $parts$$.length; $i$$++) {
      $unwrapped$$ += goog.string.Const.unwrap($parts$$[$i$$]);
    }
    return goog.html.TrustedResourceUrl.createTrustedResourceUrlSecurityPrivateDoNotAccessOrElse($unwrapped$$);
  };
  goog.html.TrustedResourceUrl.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = {};
  goog.html.TrustedResourceUrl.createTrustedResourceUrlSecurityPrivateDoNotAccessOrElse = function($url$$) {
    var $trustedResourceUrl$$ = new goog.html.TrustedResourceUrl;
    $trustedResourceUrl$$.privateDoNotAccessOrElseTrustedResourceUrlWrappedValue_ = goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY ? goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY.createScriptURL($url$$) : $url$$;
    goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY && ($trustedResourceUrl$$.trustedURL_ = goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY.createURL($url$$));
    return $trustedResourceUrl$$;
  };
  goog.html.TrustedResourceUrl.stringifyParams_ = function($prefix$$, $currentString$$, $params$$) {
    if (null == $params$$) {
      return $currentString$$;
    }
    if (goog.isString($params$$)) {
      return $params$$ ? $prefix$$ + encodeURIComponent($params$$) : "";
    }
    for (var $key$$ in $params$$) {
      var $outputValues_value$$ = $params$$[$key$$];
      $outputValues_value$$ = goog.isArray($outputValues_value$$) ? $outputValues_value$$ : [$outputValues_value$$];
      for (var $i$$ = 0; $i$$ < $outputValues_value$$.length; $i$$++) {
        var $outputValue$$ = $outputValues_value$$[$i$$];
        null != $outputValue$$ && ($currentString$$ || ($currentString$$ = $prefix$$), $currentString$$ += ($currentString$$.length > $prefix$$.length ? "&" : "") + encodeURIComponent($key$$) + "=" + encodeURIComponent(String($outputValue$$)));
      }
    }
    return $currentString$$;
  };
  //[javascript/closure/html/safeurl.js]
  goog.html.SafeUrl = function() {
    this.privateDoNotAccessOrElseSafeUrlWrappedValue_ = "";
    this.SAFE_URL_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = goog.html.SafeUrl.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_;
  };
  goog.html.SafeUrl.INNOCUOUS_STRING = "about:invalid#zClosurez";
  goog.html.SafeUrl.prototype.implementsGoogStringTypedString = !0;
  goog.html.SafeUrl.prototype.getTypedStringValue = function() {
    return this.privateDoNotAccessOrElseSafeUrlWrappedValue_.toString();
  };
  goog.html.SafeUrl.prototype.implementsGoogI18nBidiDirectionalString = !0;
  goog.html.SafeUrl.prototype.getDirection = function() {
    return goog.i18n.bidi.Dir.LTR;
  };
  goog.DEBUG && (goog.html.SafeUrl.prototype.toString = function() {
    return "SafeUrl{" + this.privateDoNotAccessOrElseSafeUrlWrappedValue_ + "}";
  });
  goog.html.SafeUrl.unwrap = function($safeUrl$$) {
    return goog.html.SafeUrl.unwrapTrustedURL($safeUrl$$).toString();
  };
  goog.html.SafeUrl.unwrapTrustedURL = function($safeUrl$$) {
    if ($safeUrl$$ instanceof goog.html.SafeUrl && $safeUrl$$.constructor === goog.html.SafeUrl && $safeUrl$$.SAFE_URL_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ === goog.html.SafeUrl.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_) {
      return $safeUrl$$.privateDoNotAccessOrElseSafeUrlWrappedValue_;
    }
    goog.asserts.fail("expected object of type SafeUrl, got '" + $safeUrl$$ + "' of type " + goog.typeOf($safeUrl$$));
    return "type_error:SafeUrl";
  };
  goog.html.SafeUrl.fromConstant = function($url$$) {
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse(goog.string.Const.unwrap($url$$));
  };
  goog.html.SAFE_MIME_TYPE_PATTERN_ = /^(?:audio\/(?:3gpp2|3gpp|aac|L16|midi|mp3|mp4|mpeg|oga|ogg|opus|x-m4a|x-wav|wav|webm)|image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp|x-icon)|text\/csv|video\/(?:mpeg|mp4|ogg|webm|quicktime))(?:;\w+=(?:\w+|"[\w;=]+"))*$/i;
  goog.html.SafeUrl.isSafeMimeType = function($mimeType$$) {
    return goog.html.SAFE_MIME_TYPE_PATTERN_.test($mimeType$$);
  };
  goog.html.SafeUrl.fromBlob = function($blob$jscomp$13_url$$) {
    $blob$jscomp$13_url$$ = goog.html.SAFE_MIME_TYPE_PATTERN_.test($blob$jscomp$13_url$$.type) ? goog.fs.url.createObjectUrl($blob$jscomp$13_url$$) : goog.html.SafeUrl.INNOCUOUS_STRING;
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($blob$jscomp$13_url$$);
  };
  goog.html.DATA_URL_PATTERN_ = /^data:([^,]*);base64,[a-z0-9+\/]+=*$/i;
  goog.html.SafeUrl.fromDataUrl = function($dataUrl_filteredDataUrl$$) {
    $dataUrl_filteredDataUrl$$ = $dataUrl_filteredDataUrl$$.replace(/(%0A|%0D)/g, "");
    var $match$$ = $dataUrl_filteredDataUrl$$.match(goog.html.DATA_URL_PATTERN_);
    $match$$ = $match$$ && goog.html.SAFE_MIME_TYPE_PATTERN_.test($match$$[1]);
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($match$$ ? $dataUrl_filteredDataUrl$$ : goog.html.SafeUrl.INNOCUOUS_STRING);
  };
  goog.html.SafeUrl.fromTelUrl = function($telUrl$$) {
    goog.string.internal.caseInsensitiveStartsWith($telUrl$$, "tel:") || ($telUrl$$ = goog.html.SafeUrl.INNOCUOUS_STRING);
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($telUrl$$);
  };
  goog.html.SIP_URL_PATTERN_ = /^sip[s]?:[+a-z0-9_.!$%&'*\/=^`{|}~-]+@([a-z0-9-]+\.)+[a-z0-9]{2,63}$/i;
  goog.html.SafeUrl.fromSipUrl = function($sipUrl$$) {
    goog.html.SIP_URL_PATTERN_.test(decodeURIComponent($sipUrl$$)) || ($sipUrl$$ = goog.html.SafeUrl.INNOCUOUS_STRING);
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($sipUrl$$);
  };
  goog.html.SafeUrl.fromFacebookMessengerUrl = function($facebookMessengerUrl$$) {
    goog.string.internal.caseInsensitiveStartsWith($facebookMessengerUrl$$, "fb-messenger://share") || ($facebookMessengerUrl$$ = goog.html.SafeUrl.INNOCUOUS_STRING);
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($facebookMessengerUrl$$);
  };
  goog.html.SafeUrl.fromSmsUrl = function($smsUrl$$) {
    goog.string.internal.caseInsensitiveStartsWith($smsUrl$$, "sms:") && goog.html.SafeUrl.isSmsUrlBodyValid_($smsUrl$$) || ($smsUrl$$ = goog.html.SafeUrl.INNOCUOUS_STRING);
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($smsUrl$$);
  };
  goog.html.SafeUrl.isSmsUrlBodyValid_ = function($bodyValue_smsUrl$$) {
    var $bodyParams_hash$$ = $bodyValue_smsUrl$$.indexOf("#");
    0 < $bodyParams_hash$$ && ($bodyValue_smsUrl$$ = $bodyValue_smsUrl$$.substring(0, $bodyParams_hash$$));
    $bodyParams_hash$$ = $bodyValue_smsUrl$$.match(/[?&]body=/gi);
    if (!$bodyParams_hash$$) {
      return !0;
    }
    if (1 < $bodyParams_hash$$.length) {
      return !1;
    }
    $bodyValue_smsUrl$$ = $bodyValue_smsUrl$$.match(/[?&]body=([^&]*)/)[1];
    if (!$bodyValue_smsUrl$$) {
      return !0;
    }
    try {
      decodeURIComponent($bodyValue_smsUrl$$);
    } catch ($error$$) {
      return !1;
    }
    return /^(?:[a-z0-9\-_.~]|%[0-9a-f]{2})+$/i.test($bodyValue_smsUrl$$);
  };
  goog.html.SafeUrl.fromSshUrl = function($sshUrl$$) {
    goog.string.internal.caseInsensitiveStartsWith($sshUrl$$, "ssh://") || ($sshUrl$$ = goog.html.SafeUrl.INNOCUOUS_STRING);
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($sshUrl$$);
  };
  goog.html.SafeUrl.sanitizeChromeExtensionUrl = function($url$$, $extensionId$$) {
    return goog.html.SafeUrl.sanitizeExtensionUrl_(/^chrome-extension:\/\/([^\/]+)\//, $url$$, $extensionId$$);
  };
  goog.html.SafeUrl.sanitizeFirefoxExtensionUrl = function($url$$, $extensionId$$) {
    return goog.html.SafeUrl.sanitizeExtensionUrl_(/^moz-extension:\/\/([^\/]+)\//, $url$$, $extensionId$$);
  };
  goog.html.SafeUrl.sanitizeEdgeExtensionUrl = function($url$$, $extensionId$$) {
    return goog.html.SafeUrl.sanitizeExtensionUrl_(/^ms-browser-extension:\/\/([^\/]+)\//, $url$$, $extensionId$$);
  };
  goog.html.SafeUrl.sanitizeExtensionUrl_ = function($extractedExtensionId_matches_scheme$$, $url$$, $acceptedExtensionIds_extensionId$$) {
    ($extractedExtensionId_matches_scheme$$ = $extractedExtensionId_matches_scheme$$.exec($url$$)) ? ($extractedExtensionId_matches_scheme$$ = $extractedExtensionId_matches_scheme$$[1], $acceptedExtensionIds_extensionId$$ = $acceptedExtensionIds_extensionId$$ instanceof goog.string.Const ? [goog.string.Const.unwrap($acceptedExtensionIds_extensionId$$)] : $acceptedExtensionIds_extensionId$$.map(function($x$$) {
      return goog.string.Const.unwrap($x$$);
    }), -1 == $acceptedExtensionIds_extensionId$$.indexOf($extractedExtensionId_matches_scheme$$) && ($url$$ = goog.html.SafeUrl.INNOCUOUS_STRING)) : $url$$ = goog.html.SafeUrl.INNOCUOUS_STRING;
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($url$$);
  };
  goog.html.SafeUrl.fromTrustedResourceUrl = function($trustedResourceUrl$$) {
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse(goog.html.TrustedResourceUrl.unwrap($trustedResourceUrl$$));
  };
  goog.html.SAFE_URL_PATTERN_ = /^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i;
  goog.html.SafeUrl.SAFE_URL_PATTERN = goog.html.SAFE_URL_PATTERN_;
  goog.html.SafeUrl.sanitize = function($url$$) {
    if ($url$$ instanceof goog.html.SafeUrl) {
      return $url$$;
    }
    $url$$ = "object" == typeof $url$$ && $url$$.implementsGoogStringTypedString ? $url$$.getTypedStringValue() : String($url$$);
    goog.html.SAFE_URL_PATTERN_.test($url$$) || ($url$$ = goog.html.SafeUrl.INNOCUOUS_STRING);
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($url$$);
  };
  goog.html.SafeUrl.sanitizeAssertUnchanged = function($url$$, $opt_allowDataUrl_safeUrl$$) {
    if ($url$$ instanceof goog.html.SafeUrl) {
      return $url$$;
    }
    $url$$ = "object" == typeof $url$$ && $url$$.implementsGoogStringTypedString ? $url$$.getTypedStringValue() : String($url$$);
    if ($opt_allowDataUrl_safeUrl$$ && /^data:/i.test($url$$) && ($opt_allowDataUrl_safeUrl$$ = goog.html.SafeUrl.fromDataUrl($url$$), $opt_allowDataUrl_safeUrl$$.getTypedStringValue() == $url$$)) {
      return $opt_allowDataUrl_safeUrl$$;
    }
    goog.asserts.assert(goog.html.SAFE_URL_PATTERN_.test($url$$), "%s does not match the safe URL pattern", $url$$) || ($url$$ = goog.html.SafeUrl.INNOCUOUS_STRING);
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($url$$);
  };
  goog.html.SafeUrl.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = {};
  goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse = function($url$$) {
    var $safeUrl$$ = new goog.html.SafeUrl;
    $safeUrl$$.privateDoNotAccessOrElseSafeUrlWrappedValue_ = goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY ? goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY.createURL($url$$) : $url$$;
    return $safeUrl$$;
  };
  goog.html.SafeUrl.ABOUT_BLANK = goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse("about:blank");
  //[javascript/closure/html/safestyle.js]
  goog.html.SafeStyle = function() {
    this.privateDoNotAccessOrElseSafeStyleWrappedValue_ = "";
    this.SAFE_STYLE_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = goog.html.SafeStyle.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_;
  };
  goog.html.SafeStyle.prototype.implementsGoogStringTypedString = !0;
  goog.html.SafeStyle.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = {};
  goog.html.SafeStyle.fromConstant = function($style_styleString$$) {
    $style_styleString$$ = goog.string.Const.unwrap($style_styleString$$);
    if (0 === $style_styleString$$.length) {
      return goog.html.SafeStyle.EMPTY;
    }
    goog.asserts.assert(goog.string.internal.endsWith($style_styleString$$, ";"), "Last character of style string is not ';': " + $style_styleString$$);
    goog.asserts.assert(goog.string.internal.contains($style_styleString$$, ":"), "Style string must contain at least one ':', to specify a \"name: value\" pair: " + $style_styleString$$);
    return goog.html.SafeStyle.createSafeStyleSecurityPrivateDoNotAccessOrElse($style_styleString$$);
  };
  goog.html.SafeStyle.prototype.getTypedStringValue = function() {
    return this.privateDoNotAccessOrElseSafeStyleWrappedValue_;
  };
  goog.DEBUG && (goog.html.SafeStyle.prototype.toString = function() {
    return "SafeStyle{" + this.privateDoNotAccessOrElseSafeStyleWrappedValue_ + "}";
  });
  goog.html.SafeStyle.unwrap = function($safeStyle$$) {
    if ($safeStyle$$ instanceof goog.html.SafeStyle && $safeStyle$$.constructor === goog.html.SafeStyle && $safeStyle$$.SAFE_STYLE_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ === goog.html.SafeStyle.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_) {
      return $safeStyle$$.privateDoNotAccessOrElseSafeStyleWrappedValue_;
    }
    goog.asserts.fail("expected object of type SafeStyle, got '" + $safeStyle$$ + "' of type " + goog.typeOf($safeStyle$$));
    return "type_error:SafeStyle";
  };
  goog.html.SafeStyle.createSafeStyleSecurityPrivateDoNotAccessOrElse = function($style$$) {
    return (new goog.html.SafeStyle).initSecurityPrivateDoNotAccessOrElse_($style$$);
  };
  goog.html.SafeStyle.prototype.initSecurityPrivateDoNotAccessOrElse_ = function($style$$) {
    this.privateDoNotAccessOrElseSafeStyleWrappedValue_ = $style$$;
    return this;
  };
  goog.html.SafeStyle.EMPTY = goog.html.SafeStyle.createSafeStyleSecurityPrivateDoNotAccessOrElse("");
  goog.html.SafeStyle.INNOCUOUS_STRING = "zClosurez";
  goog.html.SafeStyle.create = function($map$$) {
    var $style$$ = "", $name$$;
    for ($name$$ in $map$$) {
      if (!/^[-_a-zA-Z0-9]+$/.test($name$$)) {
        throw Error("Name allows only [-_a-zA-Z0-9], got: " + $name$$);
      }
      var $value$$ = $map$$[$name$$];
      null != $value$$ && ($value$$ = goog.isArray($value$$) ? goog.array.map($value$$, goog.html.SafeStyle.sanitizePropertyValue_).join(" ") : goog.html.SafeStyle.sanitizePropertyValue_($value$$), $style$$ += $name$$ + ":" + $value$$ + ";");
    }
    return $style$$ ? goog.html.SafeStyle.createSafeStyleSecurityPrivateDoNotAccessOrElse($style$$) : goog.html.SafeStyle.EMPTY;
  };
  goog.html.SafeStyle.sanitizePropertyValue_ = function($result$jscomp$10_url$jscomp$37_value$$) {
    if ($result$jscomp$10_url$jscomp$37_value$$ instanceof goog.html.SafeUrl) {
      return $result$jscomp$10_url$jscomp$37_value$$ = goog.html.SafeUrl.unwrap($result$jscomp$10_url$jscomp$37_value$$), 'url("' + $result$jscomp$10_url$jscomp$37_value$$.replace(/</g, "%3c").replace(/[\\"]/g, "\\$&") + '")';
    }
    $result$jscomp$10_url$jscomp$37_value$$ = $result$jscomp$10_url$jscomp$37_value$$ instanceof goog.string.Const ? goog.string.Const.unwrap($result$jscomp$10_url$jscomp$37_value$$) : goog.html.SafeStyle.sanitizePropertyValueString_(String($result$jscomp$10_url$jscomp$37_value$$));
    if (/[{;}]/.test($result$jscomp$10_url$jscomp$37_value$$)) {
      throw new goog.asserts.AssertionError("Value does not allow [{;}], got: %s.", [$result$jscomp$10_url$jscomp$37_value$$]);
    }
    return $result$jscomp$10_url$jscomp$37_value$$;
  };
  goog.html.SafeStyle.sanitizePropertyValueString_ = function($value$$) {
    var $valueWithoutFunctions$$ = $value$$.replace(goog.html.SafeStyle.FUNCTIONS_RE_, "$1").replace(goog.html.SafeStyle.FUNCTIONS_RE_, "$1").replace(goog.html.SafeStyle.URL_RE_, "url");
    if (goog.html.SafeStyle.VALUE_RE_.test($valueWithoutFunctions$$)) {
      if (goog.html.SafeStyle.COMMENT_RE_.test($value$$)) {
        return goog.asserts.fail("String value disallows comments, got: " + $value$$), goog.html.SafeStyle.INNOCUOUS_STRING;
      }
      if (!goog.html.SafeStyle.hasBalancedQuotes_($value$$)) {
        return goog.asserts.fail("String value requires balanced quotes, got: " + $value$$), goog.html.SafeStyle.INNOCUOUS_STRING;
      }
      if (!goog.html.SafeStyle.hasBalancedSquareBrackets_($value$$)) {
        return goog.asserts.fail("String value requires balanced square brackets and one identifier per pair of brackets, got: " + $value$$), goog.html.SafeStyle.INNOCUOUS_STRING;
      }
    } else {
      return goog.asserts.fail("String value allows only " + goog.html.SafeStyle.VALUE_ALLOWED_CHARS_ + " and simple functions, got: " + $value$$), goog.html.SafeStyle.INNOCUOUS_STRING;
    }
    return goog.html.SafeStyle.sanitizeUrl_($value$$);
  };
  goog.html.SafeStyle.hasBalancedQuotes_ = function($value$$) {
    for (var $outsideSingle$$ = !0, $outsideDouble$$ = !0, $i$$ = 0; $i$$ < $value$$.length; $i$$++) {
      var $c$$ = $value$$.charAt($i$$);
      "'" == $c$$ && $outsideDouble$$ ? $outsideSingle$$ = !$outsideSingle$$ : '"' == $c$$ && $outsideSingle$$ && ($outsideDouble$$ = !$outsideDouble$$);
    }
    return $outsideSingle$$ && $outsideDouble$$;
  };
  goog.html.SafeStyle.hasBalancedSquareBrackets_ = function($value$$) {
    for (var $outside$$ = !0, $tokenRe$$ = /^[-_a-zA-Z0-9]$/, $i$$ = 0; $i$$ < $value$$.length; $i$$++) {
      var $c$$ = $value$$.charAt($i$$);
      if ("]" == $c$$) {
        if ($outside$$) {
          return !1;
        }
        $outside$$ = !0;
      } else {
        if ("[" == $c$$) {
          if (!$outside$$) {
            return !1;
          }
          $outside$$ = !1;
        } else {
          if (!$outside$$ && !$tokenRe$$.test($c$$)) {
            return !1;
          }
        }
      }
    }
    return $outside$$;
  };
  goog.html.SafeStyle.VALUE_ALLOWED_CHARS_ = "[-,.\"'%_!# a-zA-Z0-9\\[\\]]";
  goog.html.SafeStyle.VALUE_RE_ = new RegExp("^" + goog.html.SafeStyle.VALUE_ALLOWED_CHARS_ + "+$");
  goog.html.SafeStyle.URL_RE_ = /\b(url\([ \t\n]*)('[ -&(-\[\]-~]*'|"[ !#-\[\]-~]*"|[!#-&*-\[\]-~]*)([ \t\n]*\))/g;
  goog.html.SafeStyle.FUNCTIONS_RE_ = /\b(hsl|hsla|rgb|rgba|matrix|calc|minmax|fit-content|repeat|(rotate|scale|translate)(X|Y|Z|3d)?)\([-+*/0-9a-z.%\[\], ]+\)/g;
  goog.html.SafeStyle.COMMENT_RE_ = /\/\*/;
  goog.html.SafeStyle.sanitizeUrl_ = function($value$$) {
    return $value$$.replace(goog.html.SafeStyle.URL_RE_, function($match$jscomp$0$$, $before$$, $url$$, $after$$) {
      var $quote$$ = "";
      $url$$ = $url$$.replace(/^(['"])(.*)\1$/, function($match$$, $start$$, $inside$$) {
        $quote$$ = $start$$;
        return $inside$$;
      });
      $match$jscomp$0$$ = goog.html.SafeUrl.sanitize($url$$).getTypedStringValue();
      return $before$$ + $quote$$ + $match$jscomp$0$$ + $quote$$ + $after$$;
    });
  };
  goog.html.SafeStyle.concat = function($var_args$$) {
    var $style$$ = "", $addArgument$$ = function($argument$$) {
      goog.isArray($argument$$) ? goog.array.forEach($argument$$, $addArgument$$) : $style$$ += goog.html.SafeStyle.unwrap($argument$$);
    };
    goog.array.forEach(arguments, $addArgument$$);
    return $style$$ ? goog.html.SafeStyle.createSafeStyleSecurityPrivateDoNotAccessOrElse($style$$) : goog.html.SafeStyle.EMPTY;
  };
  //[javascript/closure/html/safestylesheet.js]
  goog.html.SafeStyleSheet = function() {
    this.privateDoNotAccessOrElseSafeStyleSheetWrappedValue_ = "";
    this.SAFE_STYLE_SHEET_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = goog.html.SafeStyleSheet.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_;
  };
  goog.html.SafeStyleSheet.prototype.implementsGoogStringTypedString = !0;
  goog.html.SafeStyleSheet.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = {};
  goog.html.SafeStyleSheet.createRule = function($selector$$, $style$$) {
    if (goog.string.internal.contains($selector$$, "<")) {
      throw Error("Selector does not allow '<', got: " + $selector$$);
    }
    var $selectorToCheck$$ = $selector$$.replace(/('|")((?!\1)[^\r\n\f\\]|\\[\s\S])*\1/g, "");
    if (!/^[-_a-zA-Z0-9#.:* ,>+~[\]()=^$|]+$/.test($selectorToCheck$$)) {
      throw Error("Selector allows only [-_a-zA-Z0-9#.:* ,>+~[\\]()=^$|] and strings, got: " + $selector$$);
    }
    if (!goog.html.SafeStyleSheet.hasBalancedBrackets_($selectorToCheck$$)) {
      throw Error("() and [] in selector must be balanced, got: " + $selector$$);
    }
    $style$$ instanceof goog.html.SafeStyle || ($style$$ = goog.html.SafeStyle.create($style$$));
    $selector$$ = $selector$$ + "{" + goog.html.SafeStyle.unwrap($style$$).replace(/</g, "\\3C ") + "}";
    return goog.html.SafeStyleSheet.createSafeStyleSheetSecurityPrivateDoNotAccessOrElse($selector$$);
  };
  goog.html.SafeStyleSheet.hasBalancedBrackets_ = function($s$$) {
    for (var $brackets$$ = {"(":")", "[":"]"}, $expectedBrackets$$ = [], $i$$ = 0; $i$$ < $s$$.length; $i$$++) {
      var $ch$$ = $s$$[$i$$];
      if ($brackets$$[$ch$$]) {
        $expectedBrackets$$.push($brackets$$[$ch$$]);
      } else {
        if (goog.object.contains($brackets$$, $ch$$) && $expectedBrackets$$.pop() != $ch$$) {
          return !1;
        }
      }
    }
    return 0 == $expectedBrackets$$.length;
  };
  goog.html.SafeStyleSheet.concat = function($var_args$$) {
    var $result$$ = "", $addArgument$$ = function($argument$$) {
      goog.isArray($argument$$) ? goog.array.forEach($argument$$, $addArgument$$) : $result$$ += goog.html.SafeStyleSheet.unwrap($argument$$);
    };
    goog.array.forEach(arguments, $addArgument$$);
    return goog.html.SafeStyleSheet.createSafeStyleSheetSecurityPrivateDoNotAccessOrElse($result$$);
  };
  goog.html.SafeStyleSheet.fromConstant = function($styleSheet$$) {
    $styleSheet$$ = goog.string.Const.unwrap($styleSheet$$);
    if (0 === $styleSheet$$.length) {
      return goog.html.SafeStyleSheet.EMPTY;
    }
    goog.asserts.assert(!goog.string.internal.contains($styleSheet$$, "<"), "Forbidden '<' character in style sheet string: " + $styleSheet$$);
    return goog.html.SafeStyleSheet.createSafeStyleSheetSecurityPrivateDoNotAccessOrElse($styleSheet$$);
  };
  goog.html.SafeStyleSheet.prototype.getTypedStringValue = function() {
    return this.privateDoNotAccessOrElseSafeStyleSheetWrappedValue_;
  };
  goog.DEBUG && (goog.html.SafeStyleSheet.prototype.toString = function() {
    return "SafeStyleSheet{" + this.privateDoNotAccessOrElseSafeStyleSheetWrappedValue_ + "}";
  });
  goog.html.SafeStyleSheet.unwrap = function($safeStyleSheet$$) {
    if ($safeStyleSheet$$ instanceof goog.html.SafeStyleSheet && $safeStyleSheet$$.constructor === goog.html.SafeStyleSheet && $safeStyleSheet$$.SAFE_STYLE_SHEET_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ === goog.html.SafeStyleSheet.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_) {
      return $safeStyleSheet$$.privateDoNotAccessOrElseSafeStyleSheetWrappedValue_;
    }
    goog.asserts.fail("expected object of type SafeStyleSheet, got '" + $safeStyleSheet$$ + "' of type " + goog.typeOf($safeStyleSheet$$));
    return "type_error:SafeStyleSheet";
  };
  goog.html.SafeStyleSheet.createSafeStyleSheetSecurityPrivateDoNotAccessOrElse = function($styleSheet$$) {
    return (new goog.html.SafeStyleSheet).initSecurityPrivateDoNotAccessOrElse_($styleSheet$$);
  };
  goog.html.SafeStyleSheet.prototype.initSecurityPrivateDoNotAccessOrElse_ = function($styleSheet$$) {
    this.privateDoNotAccessOrElseSafeStyleSheetWrappedValue_ = $styleSheet$$;
    return this;
  };
  goog.html.SafeStyleSheet.EMPTY = goog.html.SafeStyleSheet.createSafeStyleSheetSecurityPrivateDoNotAccessOrElse("");
  //[javascript/closure/html/safehtml.js]
  goog.html.SafeHtml = function() {
    this.privateDoNotAccessOrElseSafeHtmlWrappedValue_ = "";
    this.SAFE_HTML_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = goog.html.SafeHtml.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_;
    this.dir_ = null;
  };
  goog.html.SafeHtml.prototype.implementsGoogI18nBidiDirectionalString = !0;
  goog.html.SafeHtml.prototype.getDirection = function() {
    return this.dir_;
  };
  goog.html.SafeHtml.prototype.implementsGoogStringTypedString = !0;
  goog.html.SafeHtml.prototype.getTypedStringValue = function() {
    return this.privateDoNotAccessOrElseSafeHtmlWrappedValue_.toString();
  };
  goog.DEBUG && (goog.html.SafeHtml.prototype.toString = function() {
    return "SafeHtml{" + this.privateDoNotAccessOrElseSafeHtmlWrappedValue_ + "}";
  });
  goog.html.SafeHtml.unwrap = function($safeHtml$$) {
    return goog.html.SafeHtml.unwrapTrustedHTML($safeHtml$$).toString();
  };
  goog.html.SafeHtml.unwrapTrustedHTML = function($safeHtml$$) {
    if ($safeHtml$$ instanceof goog.html.SafeHtml && $safeHtml$$.constructor === goog.html.SafeHtml && $safeHtml$$.SAFE_HTML_TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ === goog.html.SafeHtml.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_) {
      return $safeHtml$$.privateDoNotAccessOrElseSafeHtmlWrappedValue_;
    }
    goog.asserts.fail("expected object of type SafeHtml, got '" + $safeHtml$$ + "' of type " + goog.typeOf($safeHtml$$));
    return "type_error:SafeHtml";
  };
  goog.html.SafeHtml.htmlEscape = function($textAsString_textOrHtml$$) {
    if ($textAsString_textOrHtml$$ instanceof goog.html.SafeHtml) {
      return $textAsString_textOrHtml$$;
    }
    var $textIsObject$$ = "object" == typeof $textAsString_textOrHtml$$, $dir$$ = null;
    $textIsObject$$ && $textAsString_textOrHtml$$.implementsGoogI18nBidiDirectionalString && ($dir$$ = $textAsString_textOrHtml$$.getDirection());
    $textAsString_textOrHtml$$ = $textIsObject$$ && $textAsString_textOrHtml$$.implementsGoogStringTypedString ? $textAsString_textOrHtml$$.getTypedStringValue() : String($textAsString_textOrHtml$$);
    return goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse(goog.string.internal.htmlEscape($textAsString_textOrHtml$$), $dir$$);
  };
  goog.html.SafeHtml.htmlEscapePreservingNewlines = function($html$jscomp$2_textOrHtml$$) {
    if ($html$jscomp$2_textOrHtml$$ instanceof goog.html.SafeHtml) {
      return $html$jscomp$2_textOrHtml$$;
    }
    $html$jscomp$2_textOrHtml$$ = goog.html.SafeHtml.htmlEscape($html$jscomp$2_textOrHtml$$);
    return goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse(goog.string.internal.newLineToBr(goog.html.SafeHtml.unwrap($html$jscomp$2_textOrHtml$$)), $html$jscomp$2_textOrHtml$$.getDirection());
  };
  goog.html.SafeHtml.htmlEscapePreservingNewlinesAndSpaces = function($html$jscomp$3_textOrHtml$$) {
    if ($html$jscomp$3_textOrHtml$$ instanceof goog.html.SafeHtml) {
      return $html$jscomp$3_textOrHtml$$;
    }
    $html$jscomp$3_textOrHtml$$ = goog.html.SafeHtml.htmlEscape($html$jscomp$3_textOrHtml$$);
    return goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse(goog.string.internal.whitespaceEscape(goog.html.SafeHtml.unwrap($html$jscomp$3_textOrHtml$$)), $html$jscomp$3_textOrHtml$$.getDirection());
  };
  goog.html.SafeHtml.from = goog.html.SafeHtml.htmlEscape;
  goog.html.SafeHtml.VALID_NAMES_IN_TAG_ = /^[a-zA-Z0-9-]+$/;
  goog.html.SafeHtml.URL_ATTRIBUTES_ = {action:!0, cite:!0, data:!0, formaction:!0, href:!0, manifest:!0, poster:!0, src:!0};
  goog.html.SafeHtml.NOT_ALLOWED_TAG_NAMES_ = {APPLET:!0, BASE:!0, EMBED:!0, IFRAME:!0, LINK:!0, MATH:!0, META:!0, OBJECT:!0, SCRIPT:!0, STYLE:!0, SVG:!0, TEMPLATE:!0};
  goog.html.SafeHtml.create = function($tagName$$, $opt_attributes$$, $opt_content$$) {
    goog.html.SafeHtml.verifyTagName(String($tagName$$));
    return goog.html.SafeHtml.createSafeHtmlTagSecurityPrivateDoNotAccessOrElse(String($tagName$$), $opt_attributes$$, $opt_content$$);
  };
  goog.html.SafeHtml.verifyTagName = function($tagName$$) {
    if (!goog.html.SafeHtml.VALID_NAMES_IN_TAG_.test($tagName$$)) {
      throw Error("Invalid tag name <" + $tagName$$ + ">.");
    }
    if ($tagName$$.toUpperCase() in goog.html.SafeHtml.NOT_ALLOWED_TAG_NAMES_) {
      throw Error("Tag name <" + $tagName$$ + "> is not allowed for SafeHtml.");
    }
  };
  goog.html.SafeHtml.createIframe = function($defaultAttributes_opt_src$$, $opt_srcdoc$$, $attributes$jscomp$1_opt_attributes$$, $opt_content$$) {
    $defaultAttributes_opt_src$$ && goog.html.TrustedResourceUrl.unwrap($defaultAttributes_opt_src$$);
    var $fixedAttributes$$ = {};
    $fixedAttributes$$.src = $defaultAttributes_opt_src$$ || null;
    $fixedAttributes$$.srcdoc = $opt_srcdoc$$ && goog.html.SafeHtml.unwrap($opt_srcdoc$$);
    $defaultAttributes_opt_src$$ = {sandbox:""};
    $attributes$jscomp$1_opt_attributes$$ = goog.html.SafeHtml.combineAttributes($fixedAttributes$$, $defaultAttributes_opt_src$$, $attributes$jscomp$1_opt_attributes$$);
    return goog.html.SafeHtml.createSafeHtmlTagSecurityPrivateDoNotAccessOrElse("iframe", $attributes$jscomp$1_opt_attributes$$, $opt_content$$);
  };
  goog.html.SafeHtml.createSandboxIframe = function($attributes$jscomp$2_opt_src$$, $opt_srcdoc$$, $opt_attributes$$, $opt_content$$) {
    if (!goog.html.SafeHtml.canUseSandboxIframe()) {
      throw Error("The browser does not support sandboxed iframes.");
    }
    var $fixedAttributes$$ = {};
    $fixedAttributes$$.src = $attributes$jscomp$2_opt_src$$ ? goog.html.SafeUrl.unwrap(goog.html.SafeUrl.sanitize($attributes$jscomp$2_opt_src$$)) : null;
    $fixedAttributes$$.srcdoc = $opt_srcdoc$$ || null;
    $fixedAttributes$$.sandbox = "";
    $attributes$jscomp$2_opt_src$$ = goog.html.SafeHtml.combineAttributes($fixedAttributes$$, {}, $opt_attributes$$);
    return goog.html.SafeHtml.createSafeHtmlTagSecurityPrivateDoNotAccessOrElse("iframe", $attributes$jscomp$2_opt_src$$, $opt_content$$);
  };
  goog.html.SafeHtml.canUseSandboxIframe = function() {
    return goog.global.HTMLIFrameElement && "sandbox" in goog.global.HTMLIFrameElement.prototype;
  };
  goog.html.SafeHtml.createScriptSrc = function($fixedAttributes$jscomp$2_src$$, $attributes$jscomp$3_opt_attributes$$) {
    goog.html.TrustedResourceUrl.unwrap($fixedAttributes$jscomp$2_src$$);
    $fixedAttributes$jscomp$2_src$$ = {src:$fixedAttributes$jscomp$2_src$$};
    var $defaultAttributes$$ = {};
    $attributes$jscomp$3_opt_attributes$$ = goog.html.SafeHtml.combineAttributes($fixedAttributes$jscomp$2_src$$, $defaultAttributes$$, $attributes$jscomp$3_opt_attributes$$);
    return goog.html.SafeHtml.createSafeHtmlTagSecurityPrivateDoNotAccessOrElse("script", $attributes$jscomp$3_opt_attributes$$);
  };
  goog.html.SafeHtml.createScript = function($htmlContent_script$$, $opt_attributes$$) {
    for (var $attr_content$$ in $opt_attributes$$) {
      var $attrLower_i$$ = $attr_content$$.toLowerCase();
      if ("language" == $attrLower_i$$ || "src" == $attrLower_i$$ || "text" == $attrLower_i$$ || "type" == $attrLower_i$$) {
        throw Error('Cannot set "' + $attrLower_i$$ + '" attribute');
      }
    }
    $attr_content$$ = "";
    $htmlContent_script$$ = goog.array.concat($htmlContent_script$$);
    for ($attrLower_i$$ = 0; $attrLower_i$$ < $htmlContent_script$$.length; $attrLower_i$$++) {
      $attr_content$$ += goog.html.SafeScript.unwrap($htmlContent_script$$[$attrLower_i$$]);
    }
    $htmlContent_script$$ = goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse($attr_content$$, goog.i18n.bidi.Dir.NEUTRAL);
    return goog.html.SafeHtml.createSafeHtmlTagSecurityPrivateDoNotAccessOrElse("script", $opt_attributes$$, $htmlContent_script$$);
  };
  goog.html.SafeHtml.createStyle = function($htmlContent$jscomp$1_styleSheet$$, $attributes$jscomp$4_opt_attributes$$) {
    var $content$jscomp$1_fixedAttributes$$ = {type:"text/css"}, $defaultAttributes$jscomp$2_i$$ = {};
    $attributes$jscomp$4_opt_attributes$$ = goog.html.SafeHtml.combineAttributes($content$jscomp$1_fixedAttributes$$, $defaultAttributes$jscomp$2_i$$, $attributes$jscomp$4_opt_attributes$$);
    $content$jscomp$1_fixedAttributes$$ = "";
    $htmlContent$jscomp$1_styleSheet$$ = goog.array.concat($htmlContent$jscomp$1_styleSheet$$);
    for ($defaultAttributes$jscomp$2_i$$ = 0; $defaultAttributes$jscomp$2_i$$ < $htmlContent$jscomp$1_styleSheet$$.length; $defaultAttributes$jscomp$2_i$$++) {
      $content$jscomp$1_fixedAttributes$$ += goog.html.SafeStyleSheet.unwrap($htmlContent$jscomp$1_styleSheet$$[$defaultAttributes$jscomp$2_i$$]);
    }
    $htmlContent$jscomp$1_styleSheet$$ = goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse($content$jscomp$1_fixedAttributes$$, goog.i18n.bidi.Dir.NEUTRAL);
    return goog.html.SafeHtml.createSafeHtmlTagSecurityPrivateDoNotAccessOrElse("style", $attributes$jscomp$4_opt_attributes$$, $htmlContent$jscomp$1_styleSheet$$);
  };
  goog.html.SafeHtml.createMetaRefresh = function($unwrappedUrl_url$$, $attributes$$) {
    $unwrappedUrl_url$$ = goog.html.SafeUrl.unwrap(goog.html.SafeUrl.sanitize($unwrappedUrl_url$$));
    (goog.labs.userAgent.browser.isIE() || goog.labs.userAgent.browser.isEdge()) && goog.string.internal.contains($unwrappedUrl_url$$, ";") && ($unwrappedUrl_url$$ = "'" + $unwrappedUrl_url$$.replace(/'/g, "%27") + "'");
    $attributes$$ = {"http-equiv":"refresh", content:($attributes$$ || 0) + "; url=" + $unwrappedUrl_url$$};
    return goog.html.SafeHtml.createSafeHtmlTagSecurityPrivateDoNotAccessOrElse("meta", $attributes$$);
  };
  goog.html.SafeHtml.getAttrNameAndValue_ = function($tagName$$, $name$$, $value$$) {
    if ($value$$ instanceof goog.string.Const) {
      $value$$ = goog.string.Const.unwrap($value$$);
    } else {
      if ("style" == $name$$.toLowerCase()) {
        $value$$ = goog.html.SafeHtml.getStyleValue_($value$$);
      } else {
        if (/^on/i.test($name$$)) {
          throw Error('Attribute "' + $name$$ + '" requires goog.string.Const value, "' + $value$$ + '" given.');
        }
        if ($name$$.toLowerCase() in goog.html.SafeHtml.URL_ATTRIBUTES_) {
          if ($value$$ instanceof goog.html.TrustedResourceUrl) {
            $value$$ = goog.html.TrustedResourceUrl.unwrap($value$$);
          } else {
            if ($value$$ instanceof goog.html.SafeUrl) {
              $value$$ = goog.html.SafeUrl.unwrap($value$$);
            } else {
              if (goog.isString($value$$)) {
                $value$$ = goog.html.SafeUrl.sanitize($value$$).getTypedStringValue();
              } else {
                throw Error('Attribute "' + $name$$ + '" on tag "' + $tagName$$ + '" requires goog.html.SafeUrl, goog.string.Const, or string, value "' + $value$$ + '" given.');
              }
            }
          }
        }
      }
    }
    $value$$.implementsGoogStringTypedString && ($value$$ = $value$$.getTypedStringValue());
    goog.asserts.assert(goog.isString($value$$) || goog.isNumber($value$$), "String or number value expected, got " + typeof $value$$ + " with value: " + $value$$);
    return $name$$ + '="' + goog.string.internal.htmlEscape(String($value$$)) + '"';
  };
  goog.html.SafeHtml.getStyleValue_ = function($value$$) {
    if (!goog.isObject($value$$)) {
      throw Error('The "style" attribute requires goog.html.SafeStyle or map of style properties, ' + typeof $value$$ + " given: " + $value$$);
    }
    $value$$ instanceof goog.html.SafeStyle || ($value$$ = goog.html.SafeStyle.create($value$$));
    return goog.html.SafeStyle.unwrap($value$$);
  };
  goog.html.SafeHtml.createWithDir = function($dir$$, $html$jscomp$4_tagName$$, $opt_attributes$$, $opt_content$$) {
    $html$jscomp$4_tagName$$ = goog.html.SafeHtml.create($html$jscomp$4_tagName$$, $opt_attributes$$, $opt_content$$);
    $html$jscomp$4_tagName$$.dir_ = $dir$$;
    return $html$jscomp$4_tagName$$;
  };
  goog.html.SafeHtml.join = function($separator_separatorHtml$$, $parts$$) {
    $separator_separatorHtml$$ = goog.html.SafeHtml.htmlEscape($separator_separatorHtml$$);
    var $dir$$ = $separator_separatorHtml$$.getDirection(), $content$$ = [], $addArgument$$ = function($argument$jscomp$2_html$$) {
      goog.isArray($argument$jscomp$2_html$$) ? goog.array.forEach($argument$jscomp$2_html$$, $addArgument$$) : ($argument$jscomp$2_html$$ = goog.html.SafeHtml.htmlEscape($argument$jscomp$2_html$$), $content$$.push(goog.html.SafeHtml.unwrap($argument$jscomp$2_html$$)), $argument$jscomp$2_html$$ = $argument$jscomp$2_html$$.getDirection(), $dir$$ == goog.i18n.bidi.Dir.NEUTRAL ? $dir$$ = $argument$jscomp$2_html$$ : $argument$jscomp$2_html$$ != goog.i18n.bidi.Dir.NEUTRAL && $dir$$ != $argument$jscomp$2_html$$ &&
      ($dir$$ = null));
    };
    goog.array.forEach($parts$$, $addArgument$$);
    return goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse($content$$.join(goog.html.SafeHtml.unwrap($separator_separatorHtml$$)), $dir$$);
  };
  goog.html.SafeHtml.concat = function($var_args$$) {
    return goog.html.SafeHtml.join(goog.html.SafeHtml.EMPTY, Array.prototype.slice.call(arguments));
  };
  goog.html.SafeHtml.concatWithDir = function($dir$$, $var_args$$) {
    var $html$$ = goog.html.SafeHtml.concat(goog.array.slice(arguments, 1));
    $html$$.dir_ = $dir$$;
    return $html$$;
  };
  goog.html.SafeHtml.TYPE_MARKER_GOOG_HTML_SECURITY_PRIVATE_ = {};
  goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse = function($html$$, $dir$$) {
    return (new goog.html.SafeHtml).initSecurityPrivateDoNotAccessOrElse_($html$$, $dir$$);
  };
  goog.html.SafeHtml.prototype.initSecurityPrivateDoNotAccessOrElse_ = function($html$$, $dir$$) {
    this.privateDoNotAccessOrElseSafeHtmlWrappedValue_ = goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY ? goog.html.trustedtypes.PRIVATE_DO_NOT_ACCESS_OR_ELSE_POLICY.createHTML($html$$) : $html$$;
    this.dir_ = $dir$$;
    return this;
  };
  goog.html.SafeHtml.createSafeHtmlTagSecurityPrivateDoNotAccessOrElse = function($dirAttribute_tagName$$, $opt_attributes$$, $content$jscomp$3_opt_content$$) {
    var $dir$jscomp$7_html$$ = null, $result$$ = "<" + $dirAttribute_tagName$$;
    $result$$ += goog.html.SafeHtml.stringifyAttributes($dirAttribute_tagName$$, $opt_attributes$$);
    goog.isDefAndNotNull($content$jscomp$3_opt_content$$) ? goog.isArray($content$jscomp$3_opt_content$$) || ($content$jscomp$3_opt_content$$ = [$content$jscomp$3_opt_content$$]) : $content$jscomp$3_opt_content$$ = [];
    goog.dom.tags.isVoidTag($dirAttribute_tagName$$.toLowerCase()) ? (goog.asserts.assert(!$content$jscomp$3_opt_content$$.length, "Void tag <" + $dirAttribute_tagName$$ + "> does not allow content."), $result$$ += ">") : ($dir$jscomp$7_html$$ = goog.html.SafeHtml.concat($content$jscomp$3_opt_content$$), $result$$ += ">" + goog.html.SafeHtml.unwrap($dir$jscomp$7_html$$) + "</" + $dirAttribute_tagName$$ + ">", $dir$jscomp$7_html$$ = $dir$jscomp$7_html$$.getDirection());
    ($dirAttribute_tagName$$ = $opt_attributes$$ && $opt_attributes$$.dir) && ($dir$jscomp$7_html$$ = /^(ltr|rtl|auto)$/i.test($dirAttribute_tagName$$) ? goog.i18n.bidi.Dir.NEUTRAL : null);
    return goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse($result$$, $dir$jscomp$7_html$$);
  };
  goog.html.SafeHtml.stringifyAttributes = function($tagName$$, $opt_attributes$$) {
    var $result$$ = "";
    if ($opt_attributes$$) {
      for (var $name$$ in $opt_attributes$$) {
        if (!goog.html.SafeHtml.VALID_NAMES_IN_TAG_.test($name$$)) {
          throw Error('Invalid attribute name "' + $name$$ + '".');
        }
        var $value$$ = $opt_attributes$$[$name$$];
        goog.isDefAndNotNull($value$$) && ($result$$ += " " + goog.html.SafeHtml.getAttrNameAndValue_($tagName$$, $name$$, $value$$));
      }
    }
    return $result$$;
  };
  goog.html.SafeHtml.combineAttributes = function($fixedAttributes$$, $defaultAttributes$$, $opt_attributes$$) {
    var $combinedAttributes$$ = {}, $name$$;
    for ($name$$ in $fixedAttributes$$) {
      goog.asserts.assert($name$$.toLowerCase() == $name$$, "Must be lower case"), $combinedAttributes$$[$name$$] = $fixedAttributes$$[$name$$];
    }
    for ($name$$ in $defaultAttributes$$) {
      goog.asserts.assert($name$$.toLowerCase() == $name$$, "Must be lower case"), $combinedAttributes$$[$name$$] = $defaultAttributes$$[$name$$];
    }
    for ($name$$ in $opt_attributes$$) {
      var $nameLower$$ = $name$$.toLowerCase();
      if ($nameLower$$ in $fixedAttributes$$) {
        throw Error('Cannot override "' + $nameLower$$ + '" attribute, got "' + $name$$ + '" with value "' + $opt_attributes$$[$name$$] + '"');
      }
      $nameLower$$ in $defaultAttributes$$ && delete $combinedAttributes$$[$nameLower$$];
      $combinedAttributes$$[$name$$] = $opt_attributes$$[$name$$];
    }
    return $combinedAttributes$$;
  };
  goog.html.SafeHtml.DOCTYPE_HTML = goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse("<!DOCTYPE html>", goog.i18n.bidi.Dir.NEUTRAL);
  goog.html.SafeHtml.EMPTY = goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse("", goog.i18n.bidi.Dir.NEUTRAL);
  goog.html.SafeHtml.BR = goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse("<br>", goog.i18n.bidi.Dir.NEUTRAL);
  //[javascript/closure/html/uncheckedconversions.js]
  goog.html.uncheckedconversions = {};
  goog.html.uncheckedconversions.safeHtmlFromStringKnownToSatisfyTypeContract = function($justification$$, $html$$, $opt_dir$$) {
    goog.asserts.assertString(goog.string.Const.unwrap($justification$$), "must provide justification");
    goog.asserts.assert(!goog.string.internal.isEmptyOrWhitespace(goog.string.Const.unwrap($justification$$)), "must provide non-empty justification");
    return goog.html.SafeHtml.createSafeHtmlSecurityPrivateDoNotAccessOrElse($html$$, $opt_dir$$ || null);
  };
  goog.html.uncheckedconversions.safeScriptFromStringKnownToSatisfyTypeContract = function($justification$$, $script$$) {
    goog.asserts.assertString(goog.string.Const.unwrap($justification$$), "must provide justification");
    goog.asserts.assert(!goog.string.internal.isEmptyOrWhitespace(goog.string.Const.unwrap($justification$$)), "must provide non-empty justification");
    return goog.html.SafeScript.createSafeScriptSecurityPrivateDoNotAccessOrElse($script$$);
  };
  goog.html.uncheckedconversions.safeStyleFromStringKnownToSatisfyTypeContract = function($justification$$, $style$$) {
    goog.asserts.assertString(goog.string.Const.unwrap($justification$$), "must provide justification");
    goog.asserts.assert(!goog.string.internal.isEmptyOrWhitespace(goog.string.Const.unwrap($justification$$)), "must provide non-empty justification");
    return goog.html.SafeStyle.createSafeStyleSecurityPrivateDoNotAccessOrElse($style$$);
  };
  goog.html.uncheckedconversions.safeStyleSheetFromStringKnownToSatisfyTypeContract = function($justification$$, $styleSheet$$) {
    goog.asserts.assertString(goog.string.Const.unwrap($justification$$), "must provide justification");
    goog.asserts.assert(!goog.string.internal.isEmptyOrWhitespace(goog.string.Const.unwrap($justification$$)), "must provide non-empty justification");
    return goog.html.SafeStyleSheet.createSafeStyleSheetSecurityPrivateDoNotAccessOrElse($styleSheet$$);
  };
  goog.html.uncheckedconversions.safeUrlFromStringKnownToSatisfyTypeContract = function($justification$$, $url$$) {
    goog.asserts.assertString(goog.string.Const.unwrap($justification$$), "must provide justification");
    goog.asserts.assert(!goog.string.internal.isEmptyOrWhitespace(goog.string.Const.unwrap($justification$$)), "must provide non-empty justification");
    return goog.html.SafeUrl.createSafeUrlSecurityPrivateDoNotAccessOrElse($url$$);
  };
  goog.html.uncheckedconversions.trustedResourceUrlFromStringKnownToSatisfyTypeContract = function($justification$$, $url$$) {
    goog.asserts.assertString(goog.string.Const.unwrap($justification$$), "must provide justification");
    goog.asserts.assert(!goog.string.internal.isEmptyOrWhitespace(goog.string.Const.unwrap($justification$$)), "must provide non-empty justification");
    return goog.html.TrustedResourceUrl.createTrustedResourceUrlSecurityPrivateDoNotAccessOrElse($url$$);
  };
  //[javascript/closure/dom/safe.js]
  goog.dom.safe = {};
  goog.dom.safe.InsertAdjacentHtmlPosition = {AFTERBEGIN:"afterbegin", AFTEREND:"afterend", BEFOREBEGIN:"beforebegin", BEFOREEND:"beforeend"};
  goog.dom.safe.insertAdjacentHtml = function($node$$, $position$$, $html$$) {
    $node$$.insertAdjacentHTML($position$$, goog.html.SafeHtml.unwrapTrustedHTML($html$$));
  };
  goog.dom.safe.SET_INNER_HTML_DISALLOWED_TAGS_ = {MATH:!0, SCRIPT:!0, STYLE:!0, SVG:!0, TEMPLATE:!0};
  goog.dom.safe.isInnerHtmlCleanupRecursive_ = goog.functions.cacheReturnValue(function() {
    if (goog.DEBUG && "undefined" === typeof document) {
      return !1;
    }
    var $div$$ = document.createElement("div"), $childDiv_innerChild$$ = document.createElement("div");
    $childDiv_innerChild$$.appendChild(document.createElement("div"));
    $div$$.appendChild($childDiv_innerChild$$);
    if (goog.DEBUG && !$div$$.firstChild) {
      return !1;
    }
    $childDiv_innerChild$$ = $div$$.firstChild.firstChild;
    $div$$.innerHTML = goog.html.SafeHtml.unwrapTrustedHTML(goog.html.SafeHtml.EMPTY);
    return !$childDiv_innerChild$$.parentElement;
  });
  goog.dom.safe.unsafeSetInnerHtmlDoNotUseOrElse = function($elem$$, $html$$) {
    if (goog.dom.safe.isInnerHtmlCleanupRecursive_()) {
      for (; $elem$$.lastChild;) {
        $elem$$.removeChild($elem$$.lastChild);
      }
    }
    $elem$$.innerHTML = goog.html.SafeHtml.unwrapTrustedHTML($html$$);
  };
  goog.dom.safe.setInnerHtml = function($elem$$, $html$$) {
    if (goog.asserts.ENABLE_ASSERTS) {
      var $tagName$$ = $elem$$.tagName.toUpperCase();
      if (goog.dom.safe.SET_INNER_HTML_DISALLOWED_TAGS_[$tagName$$]) {
        throw Error("goog.dom.safe.setInnerHtml cannot be used to set content of " + $elem$$.tagName + ".");
      }
    }
    goog.dom.safe.unsafeSetInnerHtmlDoNotUseOrElse($elem$$, $html$$);
  };
  goog.dom.safe.setOuterHtml = function($elem$$, $html$$) {
    $elem$$.outerHTML = goog.html.SafeHtml.unwrapTrustedHTML($html$$);
  };
  goog.dom.safe.setFormElementAction = function($form$$, $safeUrl$jscomp$4_url$$) {
    $safeUrl$jscomp$4_url$$ = $safeUrl$jscomp$4_url$$ instanceof goog.html.SafeUrl ? $safeUrl$jscomp$4_url$$ : goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$4_url$$);
    goog.dom.asserts.assertIsHTMLFormElement($form$$).action = goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$4_url$$);
  };
  goog.dom.safe.setButtonFormAction = function($button$$, $safeUrl$jscomp$5_url$$) {
    $safeUrl$jscomp$5_url$$ = $safeUrl$jscomp$5_url$$ instanceof goog.html.SafeUrl ? $safeUrl$jscomp$5_url$$ : goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$5_url$$);
    goog.dom.asserts.assertIsHTMLButtonElement($button$$).formAction = goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$5_url$$);
  };
  goog.dom.safe.setInputFormAction = function($input$$, $safeUrl$jscomp$6_url$$) {
    $safeUrl$jscomp$6_url$$ = $safeUrl$jscomp$6_url$$ instanceof goog.html.SafeUrl ? $safeUrl$jscomp$6_url$$ : goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$6_url$$);
    goog.dom.asserts.assertIsHTMLInputElement($input$$).formAction = goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$6_url$$);
  };
  goog.dom.safe.setStyle = function($elem$$, $style$$) {
    $elem$$.style.cssText = goog.html.SafeStyle.unwrap($style$$);
  };
  goog.dom.safe.documentWrite = function($doc$$, $html$$) {
    $doc$$.write(goog.html.SafeHtml.unwrapTrustedHTML($html$$));
  };
  goog.dom.safe.setAnchorHref = function($anchor$$, $safeUrl$jscomp$7_url$$) {
    goog.dom.asserts.assertIsHTMLAnchorElement($anchor$$);
    $safeUrl$jscomp$7_url$$ = $safeUrl$jscomp$7_url$$ instanceof goog.html.SafeUrl ? $safeUrl$jscomp$7_url$$ : goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$7_url$$);
    $anchor$$.href = goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$7_url$$);
  };
  goog.dom.safe.setImageSrc = function($imageElement$$, $safeUrl$jscomp$8_url$$) {
    goog.dom.asserts.assertIsHTMLImageElement($imageElement$$);
    if (!($safeUrl$jscomp$8_url$$ instanceof goog.html.SafeUrl)) {
      var $allowDataUrl$$ = /^data:image\//i.test($safeUrl$jscomp$8_url$$);
      $safeUrl$jscomp$8_url$$ = goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$8_url$$, $allowDataUrl$$);
    }
    $imageElement$$.src = goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$8_url$$);
  };
  goog.dom.safe.setAudioSrc = function($audioElement$$, $safeUrl$jscomp$9_url$$) {
    goog.dom.asserts.assertIsHTMLAudioElement($audioElement$$);
    if (!($safeUrl$jscomp$9_url$$ instanceof goog.html.SafeUrl)) {
      var $allowDataUrl$$ = /^data:audio\//i.test($safeUrl$jscomp$9_url$$);
      $safeUrl$jscomp$9_url$$ = goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$9_url$$, $allowDataUrl$$);
    }
    $audioElement$$.src = goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$9_url$$);
  };
  goog.dom.safe.setVideoSrc = function($videoElement$$, $safeUrl$jscomp$10_url$$) {
    goog.dom.asserts.assertIsHTMLVideoElement($videoElement$$);
    if (!($safeUrl$jscomp$10_url$$ instanceof goog.html.SafeUrl)) {
      var $allowDataUrl$$ = /^data:video\//i.test($safeUrl$jscomp$10_url$$);
      $safeUrl$jscomp$10_url$$ = goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$10_url$$, $allowDataUrl$$);
    }
    $videoElement$$.src = goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$10_url$$);
  };
  goog.dom.safe.setEmbedSrc = function($embed$$, $url$$) {
    goog.dom.asserts.assertIsHTMLEmbedElement($embed$$);
    $embed$$.src = goog.html.TrustedResourceUrl.unwrapTrustedScriptURL($url$$);
  };
  goog.dom.safe.setFrameSrc = function($frame$$, $url$$) {
    goog.dom.asserts.assertIsHTMLFrameElement($frame$$);
    $frame$$.src = goog.html.TrustedResourceUrl.unwrapTrustedURL($url$$);
  };
  goog.dom.safe.setIframeSrc = function($iframe$$, $url$$) {
    goog.dom.asserts.assertIsHTMLIFrameElement($iframe$$);
    $iframe$$.src = goog.html.TrustedResourceUrl.unwrapTrustedURL($url$$);
  };
  goog.dom.safe.setIframeSrcdoc = function($iframe$$, $html$$) {
    goog.dom.asserts.assertIsHTMLIFrameElement($iframe$$);
    $iframe$$.srcdoc = goog.html.SafeHtml.unwrapTrustedHTML($html$$);
  };
  goog.dom.safe.setLinkHrefAndRel = function($link$$, $url$$, $rel$$) {
    goog.dom.asserts.assertIsHTMLLinkElement($link$$);
    $link$$.rel = $rel$$;
    goog.string.internal.caseInsensitiveContains($rel$$, "stylesheet") ? (goog.asserts.assert($url$$ instanceof goog.html.TrustedResourceUrl, 'URL must be TrustedResourceUrl because "rel" contains "stylesheet"'), $link$$.href = goog.html.TrustedResourceUrl.unwrapTrustedURL($url$$)) : $link$$.href = $url$$ instanceof goog.html.TrustedResourceUrl ? goog.html.TrustedResourceUrl.unwrapTrustedURL($url$$) : $url$$ instanceof goog.html.SafeUrl ? goog.html.SafeUrl.unwrapTrustedURL($url$$) : goog.html.SafeUrl.unwrapTrustedURL(goog.html.SafeUrl.sanitizeAssertUnchanged($url$$));
  };
  goog.dom.safe.setObjectData = function($object$$, $url$$) {
    goog.dom.asserts.assertIsHTMLObjectElement($object$$);
    $object$$.data = goog.html.TrustedResourceUrl.unwrapTrustedScriptURL($url$$);
  };
  goog.dom.safe.setScriptSrc = function($script$$, $nonce$jscomp$3_url$$) {
    goog.dom.asserts.assertIsHTMLScriptElement($script$$);
    $script$$.src = goog.html.TrustedResourceUrl.unwrapTrustedScriptURL($nonce$jscomp$3_url$$);
    ($nonce$jscomp$3_url$$ = goog.getScriptNonce()) && $script$$.setAttribute("nonce", $nonce$jscomp$3_url$$);
  };
  goog.dom.safe.setScriptContent = function($script$$, $content$jscomp$4_nonce$$) {
    goog.dom.asserts.assertIsHTMLScriptElement($script$$);
    $script$$.text = goog.html.SafeScript.unwrapTrustedScript($content$jscomp$4_nonce$$);
    ($content$jscomp$4_nonce$$ = goog.getScriptNonce()) && $script$$.setAttribute("nonce", $content$jscomp$4_nonce$$);
  };
  goog.dom.safe.setLocationHref = function($loc$$, $safeUrl$jscomp$11_url$$) {
    goog.dom.asserts.assertIsLocation($loc$$);
    $safeUrl$jscomp$11_url$$ = $safeUrl$jscomp$11_url$$ instanceof goog.html.SafeUrl ? $safeUrl$jscomp$11_url$$ : goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$11_url$$);
    $loc$$.href = goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$11_url$$);
  };
  goog.dom.safe.assignLocation = function($loc$$, $safeUrl$jscomp$12_url$$) {
    goog.dom.asserts.assertIsLocation($loc$$);
    $safeUrl$jscomp$12_url$$ = $safeUrl$jscomp$12_url$$ instanceof goog.html.SafeUrl ? $safeUrl$jscomp$12_url$$ : goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$12_url$$);
    $loc$$.assign(goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$12_url$$));
  };
  goog.dom.safe.replaceLocation = function($loc$$, $safeUrl$jscomp$13_url$$) {
    goog.dom.asserts.assertIsLocation($loc$$);
    $safeUrl$jscomp$13_url$$ = $safeUrl$jscomp$13_url$$ instanceof goog.html.SafeUrl ? $safeUrl$jscomp$13_url$$ : goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$13_url$$);
    $loc$$.replace(goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$13_url$$));
  };
  goog.dom.safe.openInWindow = function($safeUrl$jscomp$14_url$$, $opt_openerWin_win$$, $opt_name$$, $opt_specs$$, $opt_replace$$) {
    $safeUrl$jscomp$14_url$$ = $safeUrl$jscomp$14_url$$ instanceof goog.html.SafeUrl ? $safeUrl$jscomp$14_url$$ : goog.html.SafeUrl.sanitizeAssertUnchanged($safeUrl$jscomp$14_url$$);
    $opt_openerWin_win$$ = $opt_openerWin_win$$ || goog.global;
    return $opt_openerWin_win$$.open(goog.html.SafeUrl.unwrapTrustedURL($safeUrl$jscomp$14_url$$), $opt_name$$ ? goog.string.Const.unwrap($opt_name$$) : "", $opt_specs$$, $opt_replace$$);
  };
  goog.dom.safe.parseFromStringHtml = function($parser$$, $html$$) {
    return goog.dom.safe.parseFromString($parser$$, $html$$, "text/html");
  };
  goog.dom.safe.parseFromString = function($parser$$, $content$$, $type$$) {
    return $parser$$.parseFromString(goog.html.SafeHtml.unwrapTrustedHTML($content$$), $type$$);
  };
  goog.dom.safe.createImageFromBlob = function($blob$jscomp$14_image$$) {
    if (!/^image\/.*/g.test($blob$jscomp$14_image$$.type)) {
      throw Error("goog.dom.safe.createImageFromBlob only accepts MIME type image/.*.");
    }
    var $objectUrl$$ = goog.global.URL.createObjectURL($blob$jscomp$14_image$$);
    $blob$jscomp$14_image$$ = new goog.global.Image;
    $blob$jscomp$14_image$$.onload = function() {
      goog.global.URL.revokeObjectURL($objectUrl$$);
    };
    goog.dom.safe.setImageSrc($blob$jscomp$14_image$$, goog.html.uncheckedconversions.safeUrlFromStringKnownToSatisfyTypeContract(goog.string.Const.from("Image blob URL."), $objectUrl$$));
    return $blob$jscomp$14_image$$;
  };
  //[javascript/closure/string/string.js]
  goog.string.DETECT_DOUBLE_ESCAPING = !1;
  goog.string.FORCE_NON_DOM_HTML_UNESCAPING = !1;
  goog.string.Unicode = {NBSP:"\u00a0"};
  goog.string.startsWith = goog.string.internal.startsWith;
  goog.string.endsWith = goog.string.internal.endsWith;
  goog.string.caseInsensitiveStartsWith = goog.string.internal.caseInsensitiveStartsWith;
  goog.string.caseInsensitiveEndsWith = goog.string.internal.caseInsensitiveEndsWith;
  goog.string.caseInsensitiveEquals = goog.string.internal.caseInsensitiveEquals;
  goog.string.subs = function($str$$, $var_args$$) {
    for (var $splitParts$$ = $str$$.split("%s"), $returnString$$ = "", $subsArguments$$ = Array.prototype.slice.call(arguments, 1); $subsArguments$$.length && 1 < $splitParts$$.length;) {
      $returnString$$ += $splitParts$$.shift() + $subsArguments$$.shift();
    }
    return $returnString$$ + $splitParts$$.join("%s");
  };
  goog.string.collapseWhitespace = function($str$$) {
    return $str$$.replace(/[\s\xa0]+/g, " ").replace(/^\s+|\s+$/g, "");
  };
  goog.string.isEmptyOrWhitespace = goog.string.internal.isEmptyOrWhitespace;
  goog.string.isEmptyString = function($str$$) {
    return 0 == $str$$.length;
  };
  goog.string.isEmpty = goog.string.isEmptyOrWhitespace;
  goog.string.isEmptyOrWhitespaceSafe = function($str$$) {
    return goog.string.isEmptyOrWhitespace(goog.string.makeSafe($str$$));
  };
  goog.string.isEmptySafe = goog.string.isEmptyOrWhitespaceSafe;
  goog.string.isBreakingWhitespace = function($str$$) {
    return !/[^\t\n\r ]/.test($str$$);
  };
  goog.string.isAlpha = function($str$$) {
    return !/[^a-zA-Z]/.test($str$$);
  };
  goog.string.isNumeric = function($str$$) {
    return !/[^0-9]/.test($str$$);
  };
  goog.string.isAlphaNumeric = function($str$$) {
    return !/[^a-zA-Z0-9]/.test($str$$);
  };
  goog.string.isSpace = function($ch$$) {
    return " " == $ch$$;
  };
  goog.string.isUnicodeChar = function($ch$$) {
    return 1 == $ch$$.length && " " <= $ch$$ && "~" >= $ch$$ || "\u0080" <= $ch$$ && "\ufffd" >= $ch$$;
  };
  goog.string.stripNewlines = function($str$$) {
    return $str$$.replace(/(\r\n|\r|\n)+/g, " ");
  };
  goog.string.canonicalizeNewlines = function($str$$) {
    return $str$$.replace(/(\r\n|\r|\n)/g, "\n");
  };
  goog.string.normalizeWhitespace = function($str$$) {
    return $str$$.replace(/\xa0|\s/g, " ");
  };
  goog.string.normalizeSpaces = function($str$$) {
    return $str$$.replace(/\xa0|[ \t]+/g, " ");
  };
  goog.string.collapseBreakingSpaces = function($str$$) {
    return $str$$.replace(/[\t\r\n ]+/g, " ").replace(/^[\t\r\n ]+|[\t\r\n ]+$/g, "");
  };
  goog.string.trim = goog.string.internal.trim;
  goog.string.trimLeft = function($str$$) {
    return $str$$.replace(/^[\s\xa0]+/, "");
  };
  goog.string.trimRight = function($str$$) {
    return $str$$.replace(/[\s\xa0]+$/, "");
  };
  goog.string.caseInsensitiveCompare = goog.string.internal.caseInsensitiveCompare;
  goog.string.numberAwareCompare_ = function($num1_str1$$, $num2_str2$$, $a$$) {
    if ($num1_str1$$ == $num2_str2$$) {
      return 0;
    }
    if (!$num1_str1$$) {
      return -1;
    }
    if (!$num2_str2$$) {
      return 1;
    }
    for (var $tokens1$$ = $num1_str1$$.toLowerCase().match($a$$), $tokens2$$ = $num2_str2$$.toLowerCase().match($a$$), $count$$ = Math.min($tokens1$$.length, $tokens2$$.length), $i$$ = 0; $i$$ < $count$$; $i$$++) {
      $a$$ = $tokens1$$[$i$$];
      var $b$$ = $tokens2$$[$i$$];
      if ($a$$ != $b$$) {
        return $num1_str1$$ = parseInt($a$$, 10), !isNaN($num1_str1$$) && ($num2_str2$$ = parseInt($b$$, 10), !isNaN($num2_str2$$) && $num1_str1$$ - $num2_str2$$) ? $num1_str1$$ - $num2_str2$$ : $a$$ < $b$$ ? -1 : 1;
      }
    }
    return $tokens1$$.length != $tokens2$$.length ? $tokens1$$.length - $tokens2$$.length : $num1_str1$$ < $num2_str2$$ ? -1 : 1;
  };
  goog.string.intAwareCompare = function($str1$$, $str2$$) {
    return goog.string.numberAwareCompare_($str1$$, $str2$$, /\d+|\D+/g);
  };
  goog.string.floatAwareCompare = function($str1$$, $str2$$) {
    return goog.string.numberAwareCompare_($str1$$, $str2$$, /\d+|\.\d+|\D+/g);
  };
  goog.string.numerateCompare = goog.string.floatAwareCompare;
  goog.string.urlEncode = function($str$$) {
    return encodeURIComponent(String($str$$));
  };
  goog.string.urlDecode = function($str$$) {
    return decodeURIComponent($str$$.replace(/\+/g, " "));
  };
  goog.string.newLineToBr = goog.string.internal.newLineToBr;
  goog.string.htmlEscape = function($str$$, $opt_isLikelyToContainHtmlChars$$) {
    $str$$ = goog.string.internal.htmlEscape($str$$, $opt_isLikelyToContainHtmlChars$$);
    goog.string.DETECT_DOUBLE_ESCAPING && ($str$$ = $str$$.replace(goog.string.E_RE_, "&#101;"));
    return $str$$;
  };
  goog.string.E_RE_ = /e/g;
  goog.string.unescapeEntities = function($str$$) {
    return goog.string.contains($str$$, "&") ? !goog.string.FORCE_NON_DOM_HTML_UNESCAPING && "document" in goog.global ? goog.string.unescapeEntitiesUsingDom_($str$$) : goog.string.unescapePureXmlEntities_($str$$) : $str$$;
  };
  goog.string.unescapeEntitiesWithDocument = function($str$$, $document$$) {
    return goog.string.contains($str$$, "&") ? goog.string.unescapeEntitiesUsingDom_($str$$, $document$$) : $str$$;
  };
  goog.string.unescapeEntitiesUsingDom_ = function($str$$, $opt_document$$) {
    var $seen$$ = {"&amp;":"&", "&lt;":"<", "&gt;":">", "&quot;":'"'};
    var $div$$ = $opt_document$$ ? $opt_document$$.createElement("div") : goog.global.document.createElement("div");
    return $str$$.replace(goog.string.HTML_ENTITY_PATTERN_, function($s$$, $entity_n$$) {
      var $value$$ = $seen$$[$s$$];
      if ($value$$) {
        return $value$$;
      }
      "#" == $entity_n$$.charAt(0) && ($entity_n$$ = Number("0" + $entity_n$$.substr(1)), isNaN($entity_n$$) || ($value$$ = String.fromCharCode($entity_n$$)));
      $value$$ || (goog.dom.safe.setInnerHtml($div$$, goog.html.uncheckedconversions.safeHtmlFromStringKnownToSatisfyTypeContract(goog.string.Const.from("Single HTML entity."), $s$$ + " ")), $value$$ = $div$$.firstChild.nodeValue.slice(0, -1));
      return $seen$$[$s$$] = $value$$;
    });
  };
  goog.string.unescapePureXmlEntities_ = function($str$$) {
    return $str$$.replace(/&([^;]+);/g, function($s$$, $entity$jscomp$1_n$$) {
      switch($entity$jscomp$1_n$$) {
        case "amp":
          return "&";
        case "lt":
          return "<";
        case "gt":
          return ">";
        case "quot":
          return '"';
        default:
          return "#" != $entity$jscomp$1_n$$.charAt(0) || ($entity$jscomp$1_n$$ = Number("0" + $entity$jscomp$1_n$$.substr(1)), isNaN($entity$jscomp$1_n$$)) ? $s$$ : String.fromCharCode($entity$jscomp$1_n$$);
      }
    });
  };
  goog.string.HTML_ENTITY_PATTERN_ = /&([^;\s<&]+);?/g;
  goog.string.whitespaceEscape = function($str$$, $opt_xml$$) {
    return goog.string.newLineToBr($str$$.replace(/  /g, " &#160;"), $opt_xml$$);
  };
  goog.string.preserveSpaces = function($str$$) {
    return $str$$.replace(/(^|[\n ]) /g, "$1" + goog.string.Unicode.NBSP);
  };
  goog.string.stripQuotes = function($str$$, $quoteChars$$) {
    for (var $length$$ = $quoteChars$$.length, $i$$ = 0; $i$$ < $length$$; $i$$++) {
      var $quoteChar$$ = 1 == $length$$ ? $quoteChars$$ : $quoteChars$$.charAt($i$$);
      if ($str$$.charAt(0) == $quoteChar$$ && $str$$.charAt($str$$.length - 1) == $quoteChar$$) {
        return $str$$.substring(1, $str$$.length - 1);
      }
    }
    return $str$$;
  };
  goog.string.truncate = function($str$$, $chars$$, $opt_protectEscapedCharacters$$) {
    $opt_protectEscapedCharacters$$ && ($str$$ = goog.string.unescapeEntities($str$$));
    $str$$.length > $chars$$ && ($str$$ = $str$$.substring(0, $chars$$ - 3) + "...");
    $opt_protectEscapedCharacters$$ && ($str$$ = goog.string.htmlEscape($str$$));
    return $str$$;
  };
  goog.string.truncateMiddle = function($str$$, $chars$$, $opt_protectEscapedCharacters$$, $endPos_opt_trailingChars$$) {
    $opt_protectEscapedCharacters$$ && ($str$$ = goog.string.unescapeEntities($str$$));
    if ($endPos_opt_trailingChars$$ && $str$$.length > $chars$$) {
      $endPos_opt_trailingChars$$ > $chars$$ && ($endPos_opt_trailingChars$$ = $chars$$);
      var $endPoint_half$$ = $str$$.length - $endPos_opt_trailingChars$$;
      $chars$$ -= $endPos_opt_trailingChars$$;
      $str$$ = $str$$.substring(0, $chars$$) + "..." + $str$$.substring($endPoint_half$$);
    } else {
      $str$$.length > $chars$$ && ($endPoint_half$$ = Math.floor($chars$$ / 2), $endPos_opt_trailingChars$$ = $str$$.length - $endPoint_half$$, $endPoint_half$$ += $chars$$ % 2, $str$$ = $str$$.substring(0, $endPoint_half$$) + "..." + $str$$.substring($endPos_opt_trailingChars$$));
    }
    $opt_protectEscapedCharacters$$ && ($str$$ = goog.string.htmlEscape($str$$));
    return $str$$;
  };
  goog.string.specialEscapeChars_ = {"\x00":"\\0", "\b":"\\b", "\f":"\\f", "\n":"\\n", "\r":"\\r", "\t":"\\t", "\x0B":"\\x0B", '"':'\\"', "\\":"\\\\", "<":"<"};
  goog.string.jsEscapeCache_ = {"'":"\\'"};
  goog.string.quote = function($s$$) {
    $s$$ = String($s$$);
    for (var $sb$$ = ['"'], $i$$ = 0; $i$$ < $s$$.length; $i$$++) {
      var $ch$$ = $s$$.charAt($i$$), $cc$$ = $ch$$.charCodeAt(0);
      $sb$$[$i$$ + 1] = goog.string.specialEscapeChars_[$ch$$] || (31 < $cc$$ && 127 > $cc$$ ? $ch$$ : goog.string.escapeChar($ch$$));
    }
    $sb$$.push('"');
    return $sb$$.join("");
  };
  goog.string.escapeString = function($str$$) {
    for (var $sb$$ = [], $i$$ = 0; $i$$ < $str$$.length; $i$$++) {
      $sb$$[$i$$] = goog.string.escapeChar($str$$.charAt($i$$));
    }
    return $sb$$.join("");
  };
  goog.string.escapeChar = function($c$$) {
    if ($c$$ in goog.string.jsEscapeCache_) {
      return goog.string.jsEscapeCache_[$c$$];
    }
    if ($c$$ in goog.string.specialEscapeChars_) {
      return goog.string.jsEscapeCache_[$c$$] = goog.string.specialEscapeChars_[$c$$];
    }
    var $cc$$ = $c$$.charCodeAt(0);
    if (31 < $cc$$ && 127 > $cc$$) {
      var $rv$$ = $c$$;
    } else {
      if (256 > $cc$$) {
        if ($rv$$ = "\\x", 16 > $cc$$ || 256 < $cc$$) {
          $rv$$ += "0";
        }
      } else {
        $rv$$ = "\\u", 4096 > $cc$$ && ($rv$$ += "0");
      }
      $rv$$ += $cc$$.toString(16).toUpperCase();
    }
    return goog.string.jsEscapeCache_[$c$$] = $rv$$;
  };
  goog.string.contains = goog.string.internal.contains;
  goog.string.caseInsensitiveContains = goog.string.internal.caseInsensitiveContains;
  goog.string.countOf = function($s$$, $ss$$) {
    return $s$$ && $ss$$ ? $s$$.split($ss$$).length - 1 : 0;
  };
  goog.string.removeAt = function($s$$, $index$$, $stringLength$$) {
    var $resultStr$$ = $s$$;
    0 <= $index$$ && $index$$ < $s$$.length && 0 < $stringLength$$ && ($resultStr$$ = $s$$.substr(0, $index$$) + $s$$.substr($index$$ + $stringLength$$, $s$$.length - $index$$ - $stringLength$$));
    return $resultStr$$;
  };
  goog.string.remove = function($str$$, $substr$$) {
    return $str$$.replace($substr$$, "");
  };
  goog.string.removeAll = function($s$$, $re$jscomp$1_ss$$) {
    $re$jscomp$1_ss$$ = new RegExp(goog.string.regExpEscape($re$jscomp$1_ss$$), "g");
    return $s$$.replace($re$jscomp$1_ss$$, "");
  };
  goog.string.replaceAll = function($s$$, $re$jscomp$2_ss$$, $replacement$$) {
    $re$jscomp$2_ss$$ = new RegExp(goog.string.regExpEscape($re$jscomp$2_ss$$), "g");
    return $s$$.replace($re$jscomp$2_ss$$, $replacement$$.replace(/\$/g, "$$$$"));
  };
  goog.string.regExpEscape = function($s$$) {
    return String($s$$).replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, "\\$1").replace(/\x08/g, "\\x08");
  };
  goog.string.repeat = String.prototype.repeat ? function($string$$, $length$$) {
    return $string$$.repeat($length$$);
  } : function($string$$, $length$$) {
    return Array($length$$ + 1).join($string$$);
  };
  goog.string.padNumber = function($num$jscomp$5_s$$, $length$$, $index$jscomp$81_opt_precision$$) {
    $num$jscomp$5_s$$ = goog.isDef($index$jscomp$81_opt_precision$$) ? $num$jscomp$5_s$$.toFixed($index$jscomp$81_opt_precision$$) : String($num$jscomp$5_s$$);
    $index$jscomp$81_opt_precision$$ = $num$jscomp$5_s$$.indexOf(".");
    -1 == $index$jscomp$81_opt_precision$$ && ($index$jscomp$81_opt_precision$$ = $num$jscomp$5_s$$.length);
    return goog.string.repeat("0", Math.max(0, $length$$ - $index$jscomp$81_opt_precision$$)) + $num$jscomp$5_s$$;
  };
  goog.string.makeSafe = function($obj$$) {
    return null == $obj$$ ? "" : String($obj$$);
  };
  goog.string.buildString = function($var_args$$) {
    return Array.prototype.join.call(arguments, "");
  };
  goog.string.getRandomString = function() {
    return Math.floor(2147483648 * Math.random()).toString(36) + Math.abs(Math.floor(2147483648 * Math.random()) ^ goog.now()).toString(36);
  };
  goog.string.compareVersions = goog.string.internal.compareVersions;
  goog.string.hashCode = function($str$$) {
    for (var $result$$ = 0, $i$$ = 0; $i$$ < $str$$.length; ++$i$$) {
      $result$$ = 31 * $result$$ + $str$$.charCodeAt($i$$) >>> 0;
    }
    return $result$$;
  };
  goog.string.uniqueStringCounter_ = 2147483648 * Math.random() | 0;
  goog.string.createUniqueString = function() {
    return "goog_" + goog.string.uniqueStringCounter_++;
  };
  goog.string.toNumber = function($str$$) {
    var $num$$ = Number($str$$);
    return 0 == $num$$ && goog.string.isEmptyOrWhitespace($str$$) ? NaN : $num$$;
  };
  goog.string.isLowerCamelCase = function($str$$) {
    return /^[a-z]+([A-Z][a-z]*)*$/.test($str$$);
  };
  goog.string.isUpperCamelCase = function($str$$) {
    return /^([A-Z][a-z]*)+$/.test($str$$);
  };
  goog.string.toCamelCase = function($str$$) {
    return String($str$$).replace(/\-([a-z])/g, function($all$$, $match$$) {
      return $match$$.toUpperCase();
    });
  };
  goog.string.toSelectorCase = function($str$$) {
    return String($str$$).replace(/([A-Z])/g, "-$1").toLowerCase();
  };
  goog.string.toTitleCase = function($str$$, $delimiters_opt_delimiters_regexp$$) {
    $delimiters_opt_delimiters_regexp$$ = ($delimiters_opt_delimiters_regexp$$ = goog.isString($delimiters_opt_delimiters_regexp$$) ? goog.string.regExpEscape($delimiters_opt_delimiters_regexp$$) : "\\s") ? "|[" + $delimiters_opt_delimiters_regexp$$ + "]+" : "";
    $delimiters_opt_delimiters_regexp$$ = new RegExp("(^" + $delimiters_opt_delimiters_regexp$$ + ")([a-z])", "g");
    return $str$$.replace($delimiters_opt_delimiters_regexp$$, function($all$$, $p1$$, $p2$$) {
      return $p1$$ + $p2$$.toUpperCase();
    });
  };
  goog.string.capitalize = function($str$$) {
    return String($str$$.charAt(0)).toUpperCase() + String($str$$.substr(1)).toLowerCase();
  };
  goog.string.parseInt = function($value$$) {
    isFinite($value$$) && ($value$$ = String($value$$));
    return goog.isString($value$$) ? /^\s*-?0x/i.test($value$$) ? parseInt($value$$, 16) : parseInt($value$$, 10) : NaN;
  };
  goog.string.splitLimit = function($parts$jscomp$6_str$$, $separator$$, $limit$$) {
    $parts$jscomp$6_str$$ = $parts$jscomp$6_str$$.split($separator$$);
    for (var $returnVal$$ = []; 0 < $limit$$ && $parts$jscomp$6_str$$.length;) {
      $returnVal$$.push($parts$jscomp$6_str$$.shift()), $limit$$--;
    }
    $parts$jscomp$6_str$$.length && $returnVal$$.push($parts$jscomp$6_str$$.join($separator$$));
    return $returnVal$$;
  };
  goog.string.lastComponent = function($str$$, $separators$$) {
    if ($separators$$) {
      "string" == typeof $separators$$ && ($separators$$ = [$separators$$]);
    } else {
      return $str$$;
    }
    for (var $lastSeparatorIndex$$ = -1, $i$$ = 0; $i$$ < $separators$$.length; $i$$++) {
      if ("" != $separators$$[$i$$]) {
        var $currentSeparatorIndex$$ = $str$$.lastIndexOf($separators$$[$i$$]);
        $currentSeparatorIndex$$ > $lastSeparatorIndex$$ && ($lastSeparatorIndex$$ = $currentSeparatorIndex$$);
      }
    }
    return -1 == $lastSeparatorIndex$$ ? $str$$ : $str$$.slice($lastSeparatorIndex$$ + 1);
  };
  goog.string.editDistance = function($a$$, $b$$) {
    var $v0$$ = [], $v1$$ = [];
    if ($a$$ == $b$$) {
      return 0;
    }
    if (!$a$$.length || !$b$$.length) {
      return Math.max($a$$.length, $b$$.length);
    }
    for (var $i$$ = 0; $i$$ < $b$$.length + 1; $i$$++) {
      $v0$$[$i$$] = $i$$;
    }
    for ($i$$ = 0; $i$$ < $a$$.length; $i$$++) {
      $v1$$[0] = $i$$ + 1;
      for (var $j$$ = 0; $j$$ < $b$$.length; $j$$++) {
        var $cost$$ = Number($a$$[$i$$] != $b$$[$j$$]);
        $v1$$[$j$$ + 1] = Math.min($v1$$[$j$$] + 1, $v0$$[$j$$ + 1] + 1, $v0$$[$j$$] + $cost$$);
      }
      for ($j$$ = 0; $j$$ < $v0$$.length; $j$$++) {
        $v0$$[$j$$] = $v1$$[$j$$];
      }
    }
    return $v1$$[$b$$.length];
  };
  //[javascript/closure/labs/useragent/engine.js]
  goog.labs.userAgent.engine = {};
  goog.labs.userAgent.engine.isPresto = function() {
    return goog.labs.userAgent.util.matchUserAgent("Presto");
  };
  goog.labs.userAgent.engine.isTrident = function() {
    return goog.labs.userAgent.util.matchUserAgent("Trident") || goog.labs.userAgent.util.matchUserAgent("MSIE");
  };
  goog.labs.userAgent.engine.isEdge = function() {
    return goog.labs.userAgent.util.matchUserAgent("Edge");
  };
  goog.labs.userAgent.engine.isWebKit = function() {
    return goog.labs.userAgent.util.matchUserAgentIgnoreCase("WebKit") && !goog.labs.userAgent.engine.isEdge();
  };
  goog.labs.userAgent.engine.isGecko = function() {
    return goog.labs.userAgent.util.matchUserAgent("Gecko") && !goog.labs.userAgent.engine.isWebKit() && !goog.labs.userAgent.engine.isTrident() && !goog.labs.userAgent.engine.isEdge();
  };
  goog.labs.userAgent.engine.getVersion = function() {
    var $browserTuple_tuples_userAgentString$$ = goog.labs.userAgent.util.getUserAgent();
    if ($browserTuple_tuples_userAgentString$$) {
      $browserTuple_tuples_userAgentString$$ = goog.labs.userAgent.util.extractVersionTuples($browserTuple_tuples_userAgentString$$);
      var $engineTuple$$ = goog.labs.userAgent.engine.getEngineTuple_($browserTuple_tuples_userAgentString$$);
      if ($engineTuple$$) {
        return "Gecko" == $engineTuple$$[0] ? goog.labs.userAgent.engine.getVersionForKey_($browserTuple_tuples_userAgentString$$, "Firefox") : $engineTuple$$[1];
      }
      $browserTuple_tuples_userAgentString$$ = $browserTuple_tuples_userAgentString$$[0];
      var $info_match$$;
      if ($browserTuple_tuples_userAgentString$$ && ($info_match$$ = $browserTuple_tuples_userAgentString$$[2]) && ($info_match$$ = /Trident\/([^\s;]+)/.exec($info_match$$))) {
        return $info_match$$[1];
      }
    }
    return "";
  };
  goog.labs.userAgent.engine.getEngineTuple_ = function($tuples$$) {
    if (!goog.labs.userAgent.engine.isEdge()) {
      return $tuples$$[1];
    }
    for (var $i$$ = 0; $i$$ < $tuples$$.length; $i$$++) {
      var $tuple$$ = $tuples$$[$i$$];
      if ("Edge" == $tuple$$[0]) {
        return $tuple$$;
      }
    }
  };
  goog.labs.userAgent.engine.isVersionOrHigher = function($version$$) {
    return 0 <= goog.string.compareVersions(goog.labs.userAgent.engine.getVersion(), $version$$);
  };
  goog.labs.userAgent.engine.getVersionForKey_ = function($pair_tuples$$, $key$$) {
    return ($pair_tuples$$ = goog.array.find($pair_tuples$$, function($pair$$) {
      return $key$$ == $pair$$[0];
    })) && $pair_tuples$$[1] || "";
  };
  //[javascript/closure/labs/useragent/platform.js]
  goog.labs.userAgent.platform = {};
  goog.labs.userAgent.platform.isAndroid = function() {
    return goog.labs.userAgent.util.matchUserAgent("Android");
  };
  goog.labs.userAgent.platform.isIpod = function() {
    return goog.labs.userAgent.util.matchUserAgent("iPod");
  };
  goog.labs.userAgent.platform.isIphone = function() {
    return goog.labs.userAgent.util.matchUserAgent("iPhone") && !goog.labs.userAgent.util.matchUserAgent("iPod") && !goog.labs.userAgent.util.matchUserAgent("iPad");
  };
  goog.labs.userAgent.platform.isIpad = function() {
    return goog.labs.userAgent.util.matchUserAgent("iPad");
  };
  goog.labs.userAgent.platform.isIos = function() {
    return goog.labs.userAgent.platform.isIphone() || goog.labs.userAgent.platform.isIpad() || goog.labs.userAgent.platform.isIpod();
  };
  goog.labs.userAgent.platform.isMacintosh = function() {
    return goog.labs.userAgent.util.matchUserAgent("Macintosh");
  };
  goog.labs.userAgent.platform.isLinux = function() {
    return goog.labs.userAgent.util.matchUserAgent("Linux");
  };
  goog.labs.userAgent.platform.isWindows = function() {
    return goog.labs.userAgent.util.matchUserAgent("Windows");
  };
  goog.labs.userAgent.platform.isChromeOS = function() {
    return goog.labs.userAgent.util.matchUserAgent("CrOS");
  };
  goog.labs.userAgent.platform.isChromecast = function() {
    return goog.labs.userAgent.util.matchUserAgent("CrKey");
  };
  goog.labs.userAgent.platform.isKaiOS = function() {
    return goog.labs.userAgent.util.matchUserAgentIgnoreCase("KaiOS");
  };
  goog.labs.userAgent.platform.isGo2Phone = function() {
    return goog.labs.userAgent.util.matchUserAgentIgnoreCase("GAFP");
  };
  goog.labs.userAgent.platform.getVersion = function() {
    var $match$jscomp$8_userAgentString$$ = goog.labs.userAgent.util.getUserAgent(), $re$jscomp$3_version$$ = "";
    goog.labs.userAgent.platform.isWindows() ? ($re$jscomp$3_version$$ = /Windows (?:NT|Phone) ([0-9.]+)/, $re$jscomp$3_version$$ = ($match$jscomp$8_userAgentString$$ = $re$jscomp$3_version$$.exec($match$jscomp$8_userAgentString$$)) ? $match$jscomp$8_userAgentString$$[1] : "0.0") : goog.labs.userAgent.platform.isIos() ? ($re$jscomp$3_version$$ = /(?:iPhone|iPod|iPad|CPU)\s+OS\s+(\S+)/, $re$jscomp$3_version$$ = ($match$jscomp$8_userAgentString$$ = $re$jscomp$3_version$$.exec($match$jscomp$8_userAgentString$$)) &&
    $match$jscomp$8_userAgentString$$[1].replace(/_/g, ".")) : goog.labs.userAgent.platform.isMacintosh() ? ($re$jscomp$3_version$$ = /Mac OS X ([0-9_.]+)/, $re$jscomp$3_version$$ = ($match$jscomp$8_userAgentString$$ = $re$jscomp$3_version$$.exec($match$jscomp$8_userAgentString$$)) ? $match$jscomp$8_userAgentString$$[1].replace(/_/g, ".") : "10") : goog.labs.userAgent.platform.isKaiOS() ? ($re$jscomp$3_version$$ = /(?:KaiOS)\/(\S+)/i, $re$jscomp$3_version$$ = ($match$jscomp$8_userAgentString$$ = $re$jscomp$3_version$$.exec($match$jscomp$8_userAgentString$$)) &&
    $match$jscomp$8_userAgentString$$[1]) : goog.labs.userAgent.platform.isAndroid() ? ($re$jscomp$3_version$$ = /Android\s+([^\);]+)(\)|;)/, $re$jscomp$3_version$$ = ($match$jscomp$8_userAgentString$$ = $re$jscomp$3_version$$.exec($match$jscomp$8_userAgentString$$)) && $match$jscomp$8_userAgentString$$[1]) : goog.labs.userAgent.platform.isChromeOS() && ($re$jscomp$3_version$$ = /(?:CrOS\s+(?:i686|x86_64)\s+([0-9.]+))/, $re$jscomp$3_version$$ = ($match$jscomp$8_userAgentString$$ = $re$jscomp$3_version$$.exec($match$jscomp$8_userAgentString$$)) &&
    $match$jscomp$8_userAgentString$$[1]);
    return $re$jscomp$3_version$$ || "";
  };
  goog.labs.userAgent.platform.isVersionOrHigher = function($version$$) {
    return 0 <= goog.string.compareVersions(goog.labs.userAgent.platform.getVersion(), $version$$);
  };
  //[javascript/closure/reflect/reflect.js]
  goog.reflect = {};
  goog.reflect.object = function($type$$, $object$$) {
    return $object$$;
  };
  goog.reflect.objectProperty = function($prop$$) {
    return $prop$$;
  };
  goog.reflect.sinkValue = function($x$$) {
    goog.reflect.sinkValue[" "]($x$$);
    return $x$$;
  };
  goog.reflect.sinkValue[" "] = goog.nullFunction;
  goog.reflect.canAccessProperty = function($obj$$, $prop$$) {
    try {
      return goog.reflect.sinkValue($obj$$[$prop$$]), !0;
    } catch ($e$$) {
    }
    return !1;
  };
  goog.reflect.cache = function($cacheObj$$, $key$$, $valueFn$$, $opt_keyFn_storedKey$$) {
    $opt_keyFn_storedKey$$ = $opt_keyFn_storedKey$$ ? $opt_keyFn_storedKey$$($key$$) : $key$$;
    return Object.prototype.hasOwnProperty.call($cacheObj$$, $opt_keyFn_storedKey$$) ? $cacheObj$$[$opt_keyFn_storedKey$$] : $cacheObj$$[$opt_keyFn_storedKey$$] = $valueFn$$($key$$);
  };
  //[javascript/closure/useragent/useragent.js]
  goog.userAgent = {};
  goog.userAgent.ASSUME_IE = !1;
  goog.userAgent.ASSUME_EDGE = !1;
  goog.userAgent.ASSUME_GECKO = !1;
  goog.userAgent.ASSUME_WEBKIT = !1;
  goog.userAgent.ASSUME_MOBILE_WEBKIT = !1;
  goog.userAgent.ASSUME_OPERA = !1;
  goog.userAgent.ASSUME_ANY_VERSION = !1;
  goog.userAgent.BROWSER_KNOWN_ = goog.userAgent.ASSUME_IE || goog.userAgent.ASSUME_EDGE || goog.userAgent.ASSUME_GECKO || goog.userAgent.ASSUME_MOBILE_WEBKIT || goog.userAgent.ASSUME_WEBKIT || goog.userAgent.ASSUME_OPERA;
  goog.userAgent.getUserAgentString = function() {
    return goog.labs.userAgent.util.getUserAgent();
  };
  goog.userAgent.getNavigatorTyped = function() {
    return goog.global.navigator || null;
  };
  goog.userAgent.getNavigator = function() {
    return goog.userAgent.getNavigatorTyped();
  };
  goog.userAgent.OPERA = goog.userAgent.BROWSER_KNOWN_ ? goog.userAgent.ASSUME_OPERA : goog.labs.userAgent.browser.isOpera();
  goog.userAgent.IE = goog.userAgent.BROWSER_KNOWN_ ? goog.userAgent.ASSUME_IE : goog.labs.userAgent.browser.isIE();
  goog.userAgent.EDGE = goog.userAgent.BROWSER_KNOWN_ ? goog.userAgent.ASSUME_EDGE : goog.labs.userAgent.engine.isEdge();
  goog.userAgent.EDGE_OR_IE = goog.userAgent.EDGE || goog.userAgent.IE;
  goog.userAgent.GECKO = goog.userAgent.BROWSER_KNOWN_ ? goog.userAgent.ASSUME_GECKO : goog.labs.userAgent.engine.isGecko();
  goog.userAgent.WEBKIT = goog.userAgent.BROWSER_KNOWN_ ? goog.userAgent.ASSUME_WEBKIT || goog.userAgent.ASSUME_MOBILE_WEBKIT : goog.labs.userAgent.engine.isWebKit();
  goog.userAgent.isMobile_ = function() {
    return goog.userAgent.WEBKIT && goog.labs.userAgent.util.matchUserAgent("Mobile");
  };
  goog.userAgent.MOBILE = goog.userAgent.ASSUME_MOBILE_WEBKIT || goog.userAgent.isMobile_();
  goog.userAgent.SAFARI = goog.userAgent.WEBKIT;
  goog.userAgent.determinePlatform_ = function() {
    var $navigator$$ = goog.userAgent.getNavigatorTyped();
    return $navigator$$ && $navigator$$.platform || "";
  };
  goog.userAgent.PLATFORM = goog.userAgent.determinePlatform_();
  goog.userAgent.ASSUME_MAC = !1;
  goog.userAgent.ASSUME_WINDOWS = !1;
  goog.userAgent.ASSUME_LINUX = !1;
  goog.userAgent.ASSUME_X11 = !1;
  goog.userAgent.ASSUME_ANDROID = !1;
  goog.userAgent.ASSUME_IPHONE = !1;
  goog.userAgent.ASSUME_IPAD = !1;
  goog.userAgent.ASSUME_IPOD = !1;
  goog.userAgent.ASSUME_KAIOS = !1;
  goog.userAgent.ASSUME_GO2PHONE = !1;
  goog.userAgent.PLATFORM_KNOWN_ = goog.userAgent.ASSUME_MAC || goog.userAgent.ASSUME_WINDOWS || goog.userAgent.ASSUME_LINUX || goog.userAgent.ASSUME_X11 || goog.userAgent.ASSUME_ANDROID || goog.userAgent.ASSUME_IPHONE || goog.userAgent.ASSUME_IPAD || goog.userAgent.ASSUME_IPOD;
  goog.userAgent.MAC = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_MAC : goog.labs.userAgent.platform.isMacintosh();
  goog.userAgent.WINDOWS = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_WINDOWS : goog.labs.userAgent.platform.isWindows();
  goog.userAgent.isLegacyLinux_ = function() {
    return goog.labs.userAgent.platform.isLinux() || goog.labs.userAgent.platform.isChromeOS();
  };
  goog.userAgent.LINUX = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_LINUX : goog.userAgent.isLegacyLinux_();
  goog.userAgent.isX11_ = function() {
    var $navigator$$ = goog.userAgent.getNavigatorTyped();
    return !!$navigator$$ && goog.string.contains($navigator$$.appVersion || "", "X11");
  };
  goog.userAgent.X11 = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_X11 : goog.userAgent.isX11_();
  goog.userAgent.ANDROID = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_ANDROID : goog.labs.userAgent.platform.isAndroid();
  goog.userAgent.IPHONE = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_IPHONE : goog.labs.userAgent.platform.isIphone();
  goog.userAgent.IPAD = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_IPAD : goog.labs.userAgent.platform.isIpad();
  goog.userAgent.IPOD = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_IPOD : goog.labs.userAgent.platform.isIpod();
  goog.userAgent.IOS = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_IPHONE || goog.userAgent.ASSUME_IPAD || goog.userAgent.ASSUME_IPOD : goog.labs.userAgent.platform.isIos();
  goog.userAgent.KAIOS = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_KAIOS : goog.labs.userAgent.platform.isKaiOS();
  goog.userAgent.GO2PHONE = goog.userAgent.PLATFORM_KNOWN_ ? goog.userAgent.ASSUME_GO2PHONE : goog.labs.userAgent.platform.isGo2Phone();
  goog.userAgent.determineVersion_ = function() {
    var $version$$ = "", $arr$$ = goog.userAgent.getVersionRegexResult_();
    $arr$$ && ($version$$ = $arr$$ ? $arr$$[1] : "");
    return goog.userAgent.IE && ($arr$$ = goog.userAgent.getDocumentMode_(), null != $arr$$ && $arr$$ > parseFloat($version$$)) ? String($arr$$) : $version$$;
  };
  goog.userAgent.getVersionRegexResult_ = function() {
    var $userAgent$$ = goog.userAgent.getUserAgentString();
    if (goog.userAgent.GECKO) {
      return /rv:([^\);]+)(\)|;)/.exec($userAgent$$);
    }
    if (goog.userAgent.EDGE) {
      return /Edge\/([\d\.]+)/.exec($userAgent$$);
    }
    if (goog.userAgent.IE) {
      return /\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec($userAgent$$);
    }
    if (goog.userAgent.WEBKIT) {
      return /WebKit\/(\S+)/.exec($userAgent$$);
    }
    if (goog.userAgent.OPERA) {
      return /(?:Version)[ \/]?(\S+)/.exec($userAgent$$);
    }
  };
  goog.userAgent.getDocumentMode_ = function() {
    var $doc$$ = goog.global.document;
    return $doc$$ ? $doc$$.documentMode : void 0;
  };
  goog.userAgent.VERSION = goog.userAgent.determineVersion_();
  goog.userAgent.compare = function($v1$$, $v2$$) {
    return goog.string.compareVersions($v1$$, $v2$$);
  };
  goog.userAgent.isVersionOrHigherCache_ = {};
  goog.userAgent.isVersionOrHigher = function($version$$) {
    return goog.userAgent.ASSUME_ANY_VERSION || goog.reflect.cache(goog.userAgent.isVersionOrHigherCache_, $version$$, function() {
      return 0 <= goog.string.compareVersions(goog.userAgent.VERSION, $version$$);
    });
  };
  goog.userAgent.isVersion = goog.userAgent.isVersionOrHigher;
  goog.userAgent.isDocumentModeOrHigher = function($documentMode$$) {
    return Number(goog.userAgent.DOCUMENT_MODE) >= $documentMode$$;
  };
  goog.userAgent.isDocumentMode = goog.userAgent.isDocumentModeOrHigher;
  var JSCompiler_inline_result$jscomp$3;
  var doc$jscomp$inline_8 = goog.global.document, mode$jscomp$inline_9 = goog.userAgent.getDocumentMode_();
  JSCompiler_inline_result$jscomp$3 = doc$jscomp$inline_8 && goog.userAgent.IE ? mode$jscomp$inline_9 || ("CSS1Compat" == doc$jscomp$inline_8.compatMode ? parseInt(goog.userAgent.VERSION, 10) : 5) : void 0;
  goog.userAgent.DOCUMENT_MODE = JSCompiler_inline_result$jscomp$3;
  //[javascript/closure/debug/debug.js]
  goog.debug.LOGGING_ENABLED = goog.DEBUG;
  goog.debug.FORCE_SLOPPY_STACKS = !1;
  goog.debug.catchErrors = function($logFunc$$, $opt_cancel$$, $opt_target$jscomp$1_target$$) {
    $opt_target$jscomp$1_target$$ = $opt_target$jscomp$1_target$$ || goog.global;
    var $oldErrorHandler$$ = $opt_target$jscomp$1_target$$.onerror, $retVal$$ = !!$opt_cancel$$;
    goog.userAgent.WEBKIT && !goog.userAgent.isVersionOrHigher("535.3") && ($retVal$$ = !$retVal$$);
    $opt_target$jscomp$1_target$$.onerror = function($message$$, $url$$, $line$$, $opt_col$$, $opt_error$$) {
      $oldErrorHandler$$ && $oldErrorHandler$$($message$$, $url$$, $line$$, $opt_col$$, $opt_error$$);
      $logFunc$$({message:$message$$, fileName:$url$$, line:$line$$, lineNumber:$line$$, col:$opt_col$$, error:$opt_error$$});
      return $retVal$$;
    };
  };
  goog.debug.expose = function($obj$$, $opt_showFn$$) {
    if ("undefined" == typeof $obj$$) {
      return "undefined";
    }
    if (null == $obj$$) {
      return "NULL";
    }
    var $str$$ = [], $x$$;
    for ($x$$ in $obj$$) {
      if ($opt_showFn$$ || !goog.isFunction($obj$$[$x$$])) {
        var $s$$ = $x$$ + " = ";
        try {
          $s$$ += $obj$$[$x$$];
        } catch ($e$$) {
          $s$$ += "*** " + $e$$ + " ***";
        }
        $str$$.push($s$$);
      }
    }
    return $str$$.join("\n");
  };
  goog.debug.deepExpose = function($i$jscomp$86_obj$$, $opt_showFn$$) {
    var $str$$ = [], $uidsToCleanup$$ = [], $ancestorUids$$ = {}, $helper$$ = function($obj$$, $space$$) {
      var $nestspace$$ = $space$$ + "  ";
      try {
        if (goog.isDef($obj$$)) {
          if (goog.isNull($obj$$)) {
            $str$$.push("NULL");
          } else {
            if (goog.isString($obj$$)) {
              $str$$.push('"' + $obj$$.replace(/\n/g, "\n" + $space$$) + '"');
            } else {
              if (goog.isFunction($obj$$)) {
                $str$$.push(String($obj$$).replace(/\n/g, "\n" + $space$$));
              } else {
                if (goog.isObject($obj$$)) {
                  goog.hasUid($obj$$) || $uidsToCleanup$$.push($obj$$);
                  var $uid$$ = goog.getUid($obj$$);
                  if ($ancestorUids$$[$uid$$]) {
                    $str$$.push("*** reference loop detected (id=" + $uid$$ + ") ***");
                  } else {
                    $ancestorUids$$[$uid$$] = !0;
                    $str$$.push("{");
                    for (var $x$$ in $obj$$) {
                      if ($opt_showFn$$ || !goog.isFunction($obj$$[$x$$])) {
                        $str$$.push("\n"), $str$$.push($nestspace$$), $str$$.push($x$$ + " = "), $helper$$($obj$$[$x$$], $nestspace$$);
                      }
                    }
                    $str$$.push("\n" + $space$$ + "}");
                    delete $ancestorUids$$[$uid$$];
                  }
                } else {
                  $str$$.push($obj$$);
                }
              }
            }
          }
        } else {
          $str$$.push("undefined");
        }
      } catch ($e$$) {
        $str$$.push("*** " + $e$$ + " ***");
      }
    };
    $helper$$($i$jscomp$86_obj$$, "");
    for ($i$jscomp$86_obj$$ = 0; $i$jscomp$86_obj$$ < $uidsToCleanup$$.length; $i$jscomp$86_obj$$++) {
      goog.removeUid($uidsToCleanup$$[$i$jscomp$86_obj$$]);
    }
    return $str$$.join("");
  };
  goog.debug.exposeArray = function($arr$$) {
    for (var $str$$ = [], $i$$ = 0; $i$$ < $arr$$.length; $i$$++) {
      goog.isArray($arr$$[$i$$]) ? $str$$.push(goog.debug.exposeArray($arr$$[$i$$])) : $str$$.push($arr$$[$i$$]);
    }
    return "[ " + $str$$.join(", ") + " ]";
  };
  goog.debug.normalizeErrorObject = function($err$$) {
    var $ctorName_href_message$$ = goog.getObjectByName("window.location.href");
    null == $err$$ && ($err$$ = 'Unknown Error of type "null/undefined"');
    if (goog.isString($err$$)) {
      return {message:$err$$, name:"Unknown error", lineNumber:"Not available", fileName:$ctorName_href_message$$, stack:"Not available"};
    }
    var $threwError$$ = !1;
    try {
      var $lineNumber$$ = $err$$.lineNumber || $err$$.line || "Not available";
    } catch ($e$$) {
      $lineNumber$$ = "Not available", $threwError$$ = !0;
    }
    try {
      var $fileName$$ = $err$$.fileName || $err$$.filename || $err$$.sourceURL || goog.global.$googDebugFname || $ctorName_href_message$$;
    } catch ($e$1$$) {
      $fileName$$ = "Not available", $threwError$$ = !0;
    }
    return !$threwError$$ && $err$$.lineNumber && $err$$.fileName && $err$$.stack && $err$$.message && $err$$.name ? $err$$ : ($ctorName_href_message$$ = $err$$.message, null == $ctorName_href_message$$ && ($err$$.constructor && $err$$.constructor instanceof Function ? ($ctorName_href_message$$ = $err$$.constructor.name ? $err$$.constructor.name : goog.debug.getFunctionName($err$$.constructor), $ctorName_href_message$$ = 'Unknown Error of type "' + $ctorName_href_message$$ + '"') : $ctorName_href_message$$ =
    "Unknown Error of unknown type"), {message:$ctorName_href_message$$, name:$err$$.name || "UnknownError", lineNumber:$lineNumber$$, fileName:$fileName$$, stack:$err$$.stack || "Not available"});
  };
  goog.debug.enhanceError = function($err$jscomp$11_error$$, $opt_message$$) {
    $err$jscomp$11_error$$ instanceof Error || ($err$jscomp$11_error$$ = Error($err$jscomp$11_error$$), Error.captureStackTrace && Error.captureStackTrace($err$jscomp$11_error$$, goog.debug.enhanceError));
    $err$jscomp$11_error$$.stack || ($err$jscomp$11_error$$.stack = goog.debug.getStacktrace(goog.debug.enhanceError));
    if ($opt_message$$) {
      for (var $x$$ = 0; $err$jscomp$11_error$$["message" + $x$$];) {
        ++$x$$;
      }
      $err$jscomp$11_error$$["message" + $x$$] = String($opt_message$$);
    }
    return $err$jscomp$11_error$$;
  };
  goog.debug.enhanceErrorWithContext = function($err$jscomp$12_error$$, $opt_context$$) {
    $err$jscomp$12_error$$ = goog.debug.enhanceError($err$jscomp$12_error$$);
    if ($opt_context$$) {
      for (var $key$$ in $opt_context$$) {
        goog.debug.errorcontext.addErrorContext($err$jscomp$12_error$$, $key$$, $opt_context$$[$key$$]);
      }
    }
    return $err$jscomp$12_error$$;
  };
  goog.debug.getStacktraceSimple = function($opt_depth$$) {
    if (!goog.debug.FORCE_SLOPPY_STACKS) {
      var $sb$jscomp$2_stack$$ = goog.debug.getNativeStackTrace_(goog.debug.getStacktraceSimple);
      if ($sb$jscomp$2_stack$$) {
        return $sb$jscomp$2_stack$$;
      }
    }
    $sb$jscomp$2_stack$$ = [];
    for (var $fn$$ = arguments.callee.caller, $depth$$ = 0; $fn$$ && (!$opt_depth$$ || $depth$$ < $opt_depth$$);) {
      $sb$jscomp$2_stack$$.push(goog.debug.getFunctionName($fn$$));
      $sb$jscomp$2_stack$$.push("()\n");
      try {
        $fn$$ = $fn$$.caller;
      } catch ($e$$) {
        $sb$jscomp$2_stack$$.push("[exception trying to get caller]\n");
        break;
      }
      $depth$$++;
      if ($depth$$ >= goog.debug.MAX_STACK_DEPTH) {
        $sb$jscomp$2_stack$$.push("[...long stack...]");
        break;
      }
    }
    $opt_depth$$ && $depth$$ >= $opt_depth$$ ? $sb$jscomp$2_stack$$.push("[...reached max depth limit...]") : $sb$jscomp$2_stack$$.push("[end]");
    return $sb$jscomp$2_stack$$.join("");
  };
  goog.debug.MAX_STACK_DEPTH = 50;
  goog.debug.getNativeStackTrace_ = function($fn$jscomp$9_stack$$) {
    var $tempErr$$ = Error();
    if (Error.captureStackTrace) {
      return Error.captureStackTrace($tempErr$$, $fn$jscomp$9_stack$$), String($tempErr$$.stack);
    }
    try {
      throw $tempErr$$;
    } catch ($e$$) {
      $tempErr$$ = $e$$;
    }
    return ($fn$jscomp$9_stack$$ = $tempErr$$.stack) ? String($fn$jscomp$9_stack$$) : null;
  };
  goog.debug.getStacktrace = function($fn$$) {
    if (!goog.debug.FORCE_SLOPPY_STACKS) {
      var $contextFn_stack$$ = $fn$$ || goog.debug.getStacktrace;
      $contextFn_stack$$ = goog.debug.getNativeStackTrace_($contextFn_stack$$);
    }
    $contextFn_stack$$ || ($contextFn_stack$$ = goog.debug.getStacktraceHelper_($fn$$ || arguments.callee.caller, []));
    return $contextFn_stack$$;
  };
  goog.debug.getStacktraceHelper_ = function($fn$$, $visited$$) {
    var $sb$$ = [];
    if (goog.array.contains($visited$$, $fn$$)) {
      $sb$$.push("[...circular reference...]");
    } else {
      if ($fn$$ && $visited$$.length < goog.debug.MAX_STACK_DEPTH) {
        $sb$$.push(goog.debug.getFunctionName($fn$$) + "(");
        for (var $args$$ = $fn$$.arguments, $i$$ = 0; $args$$ && $i$$ < $args$$.length; $i$$++) {
          0 < $i$$ && $sb$$.push(", ");
          var $arg$$ = $args$$[$i$$];
          switch(typeof $arg$$) {
            case "object":
              $arg$$ = $arg$$ ? "object" : "null";
              break;
            case "string":
              break;
            case "number":
              $arg$$ = String($arg$$);
              break;
            case "boolean":
              $arg$$ = $arg$$ ? "true" : "false";
              break;
            case "function":
              $arg$$ = ($arg$$ = goog.debug.getFunctionName($arg$$)) ? $arg$$ : "[fn]";
              break;
            default:
              $arg$$ = typeof $arg$$;
          }
          40 < $arg$$.length && ($arg$$ = $arg$$.substr(0, 40) + "...");
          $sb$$.push($arg$$);
        }
        $visited$$.push($fn$$);
        $sb$$.push(")\n");
        try {
          $sb$$.push(goog.debug.getStacktraceHelper_($fn$$.caller, $visited$$));
        } catch ($e$$) {
          $sb$$.push("[exception trying to get caller]\n");
        }
      } else {
        $fn$$ ? $sb$$.push("[...long stack...]") : $sb$$.push("[end]");
      }
    }
    return $sb$$.join("");
  };
  goog.debug.getFunctionName = function($fn$$) {
    if (goog.debug.fnNameCache_[$fn$$]) {
      return goog.debug.fnNameCache_[$fn$$];
    }
    $fn$$ = String($fn$$);
    if (!goog.debug.fnNameCache_[$fn$$]) {
      var $matches$jscomp$1_method$$ = /function\s+([^\(]+)/m.exec($fn$$);
      $matches$jscomp$1_method$$ ? ($matches$jscomp$1_method$$ = $matches$jscomp$1_method$$[1], goog.debug.fnNameCache_[$fn$$] = $matches$jscomp$1_method$$) : goog.debug.fnNameCache_[$fn$$] = "[Anonymous]";
    }
    return goog.debug.fnNameCache_[$fn$$];
  };
  goog.debug.makeWhitespaceVisible = function($string$$) {
    return $string$$.replace(/ /g, "[_]").replace(/\f/g, "[f]").replace(/\n/g, "[n]\n").replace(/\r/g, "[r]").replace(/\t/g, "[t]");
  };
  goog.debug.runtimeType = function($value$$) {
    return $value$$ instanceof Function ? $value$$.displayName || $value$$.name || "unknown type name" : $value$$ instanceof Object ? $value$$.constructor.displayName || $value$$.constructor.name || Object.prototype.toString.call($value$$) : null === $value$$ ? "null" : typeof $value$$;
  };
  goog.debug.fnNameCache_ = {};
  goog.debug.freezeInternal_ = goog.DEBUG && Object.freeze || function($arg$$) {
    return $arg$$;
  };
  goog.debug.freeze = function($arg$$) {
    return goog.debug.freezeInternal_($arg$$);
  };
  //[javascript/closure/debug/logrecord.js]
  goog.debug.LogRecord = function($level$$, $msg$$, $loggerName$$, $opt_time$$, $opt_sequenceNumber$$) {
    this.reset($level$$, $msg$$, $loggerName$$, $opt_time$$, $opt_sequenceNumber$$);
  };
  goog.debug.LogRecord.prototype.exception_ = null;
  goog.debug.LogRecord.ENABLE_SEQUENCE_NUMBERS = !0;
  goog.debug.LogRecord.nextSequenceNumber_ = 0;
  goog.debug.LogRecord.prototype.reset = function($level$$, $msg$$, $loggerName$$, $opt_time$$, $opt_sequenceNumber$$) {
    goog.debug.LogRecord.ENABLE_SEQUENCE_NUMBERS && ("number" == typeof $opt_sequenceNumber$$ || goog.debug.LogRecord.nextSequenceNumber_++);
    $opt_time$$ || goog.now();
    this.level_ = $level$$;
    this.msg_ = $msg$$;
    delete this.exception_;
  };
  goog.debug.LogRecord.prototype.setException = function($exception$$) {
    this.exception_ = $exception$$;
  };
  goog.debug.LogRecord.prototype.setLevel = function($level$$) {
    this.level_ = $level$$;
  };
  //[javascript/closure/debug/logbuffer.js]
  goog.debug.LogBuffer = function() {
    goog.asserts.assert(goog.debug.LogBuffer.isBufferingEnabled(), "Cannot use goog.debug.LogBuffer without defining goog.debug.LogBuffer.CAPACITY.");
    this.clear();
  };
  goog.debug.LogBuffer.getInstance = function() {
    goog.debug.LogBuffer.instance_ || (goog.debug.LogBuffer.instance_ = new goog.debug.LogBuffer);
    return goog.debug.LogBuffer.instance_;
  };
  goog.debug.LogBuffer.CAPACITY = 0;
  goog.debug.LogBuffer.prototype.addRecord = function($level$$, $msg$$, $loggerName$$) {
    var $curIndex_ret$$ = (this.curIndex_ + 1) % goog.debug.LogBuffer.CAPACITY;
    this.curIndex_ = $curIndex_ret$$;
    if (this.isFull_) {
      return $curIndex_ret$$ = this.buffer_[$curIndex_ret$$], $curIndex_ret$$.reset($level$$, $msg$$, $loggerName$$), $curIndex_ret$$;
    }
    this.isFull_ = $curIndex_ret$$ == goog.debug.LogBuffer.CAPACITY - 1;
    return this.buffer_[$curIndex_ret$$] = new goog.debug.LogRecord($level$$, $msg$$, $loggerName$$);
  };
  goog.debug.LogBuffer.isBufferingEnabled = function() {
    return 0 < goog.debug.LogBuffer.CAPACITY;
  };
  goog.debug.LogBuffer.prototype.clear = function() {
    this.buffer_ = Array(goog.debug.LogBuffer.CAPACITY);
    this.curIndex_ = -1;
    this.isFull_ = !1;
  };
  //[javascript/closure/debug/logger.js]
  goog.debug.Logger = function($name$$) {
    this.name_ = $name$$;
    this.handlers_ = this.children_ = this.level_ = this.parent_ = null;
  };
  goog.debug.Logger.ROOT_LOGGER_NAME = "";
  goog.debug.Logger.ENABLE_HIERARCHY = !0;
  goog.debug.Logger.ENABLE_PROFILER_LOGGING = !1;
  goog.debug.Logger.ENABLE_HIERARCHY || (goog.debug.Logger.rootHandlers_ = []);
  goog.debug.Logger.Level = function($name$$, $value$$) {
    this.name = $name$$;
    this.value = $value$$;
  };
  goog.debug.Logger.Level.prototype.toString = function() {
    return this.name;
  };
  goog.debug.Logger.Level.OFF = new goog.debug.Logger.Level("OFF", Infinity);
  goog.debug.Logger.Level.SHOUT = new goog.debug.Logger.Level("SHOUT", 1200);
  goog.debug.Logger.Level.SEVERE = new goog.debug.Logger.Level("SEVERE", 1000);
  goog.debug.Logger.Level.WARNING = new goog.debug.Logger.Level("WARNING", 900);
  goog.debug.Logger.Level.INFO = new goog.debug.Logger.Level("INFO", 800);
  goog.debug.Logger.Level.CONFIG = new goog.debug.Logger.Level("CONFIG", 700);
  goog.debug.Logger.Level.FINE = new goog.debug.Logger.Level("FINE", 500);
  goog.debug.Logger.Level.FINER = new goog.debug.Logger.Level("FINER", 400);
  goog.debug.Logger.Level.FINEST = new goog.debug.Logger.Level("FINEST", 300);
  goog.debug.Logger.Level.ALL = new goog.debug.Logger.Level("ALL", 0);
  goog.debug.Logger.Level.PREDEFINED_LEVELS = [goog.debug.Logger.Level.OFF, goog.debug.Logger.Level.SHOUT, goog.debug.Logger.Level.SEVERE, goog.debug.Logger.Level.WARNING, goog.debug.Logger.Level.INFO, goog.debug.Logger.Level.CONFIG, goog.debug.Logger.Level.FINE, goog.debug.Logger.Level.FINER, goog.debug.Logger.Level.FINEST, goog.debug.Logger.Level.ALL];
  goog.debug.Logger.Level.predefinedLevelsCache_ = null;
  goog.debug.Logger.Level.createPredefinedLevelsCache_ = function() {
    goog.debug.Logger.Level.predefinedLevelsCache_ = {};
    for (var $i$$ = 0, $level$$; $level$$ = goog.debug.Logger.Level.PREDEFINED_LEVELS[$i$$]; $i$$++) {
      goog.debug.Logger.Level.predefinedLevelsCache_[$level$$.value] = $level$$, goog.debug.Logger.Level.predefinedLevelsCache_[$level$$.name] = $level$$;
    }
  };
  goog.debug.Logger.Level.getPredefinedLevel = function($name$$) {
    goog.debug.Logger.Level.predefinedLevelsCache_ || goog.debug.Logger.Level.createPredefinedLevelsCache_();
    return goog.debug.Logger.Level.predefinedLevelsCache_[$name$$] || null;
  };
  goog.debug.Logger.Level.getPredefinedLevelByValue = function($value$$) {
    goog.debug.Logger.Level.predefinedLevelsCache_ || goog.debug.Logger.Level.createPredefinedLevelsCache_();
    if ($value$$ in goog.debug.Logger.Level.predefinedLevelsCache_) {
      return goog.debug.Logger.Level.predefinedLevelsCache_[$value$$];
    }
    for (var $i$$ = 0; $i$$ < goog.debug.Logger.Level.PREDEFINED_LEVELS.length; ++$i$$) {
      var $level$$ = goog.debug.Logger.Level.PREDEFINED_LEVELS[$i$$];
      if ($level$$.value <= $value$$) {
        return $level$$;
      }
    }
    return null;
  };
  goog.debug.Logger.getLogger = function($name$$) {
    return goog.debug.LogManager.getLogger($name$$);
  };
  goog.debug.Logger.logToProfilers = function($msg$$) {
    if (goog.debug.Logger.ENABLE_PROFILER_LOGGING) {
      var $console$$ = goog.global.msWriteProfilerMark;
      $console$$ ? $console$$($msg$$) : ($console$$ = goog.global.console) && $console$$.timeStamp && $console$$.timeStamp($msg$$);
    }
  };
  goog.debug.Logger.prototype.getName = function() {
    return this.name_;
  };
  goog.debug.Logger.prototype.addHandler = function($handler$$) {
    goog.debug.LOGGING_ENABLED && (goog.debug.Logger.ENABLE_HIERARCHY ? (this.handlers_ || (this.handlers_ = []), this.handlers_.push($handler$$)) : (goog.asserts.assert(!this.name_, "Cannot call addHandler on a non-root logger when goog.debug.Logger.ENABLE_HIERARCHY is false."), goog.debug.Logger.rootHandlers_.push($handler$$)));
  };
  goog.debug.Logger.prototype.removeHandler = function($handler$$) {
    if (goog.debug.LOGGING_ENABLED) {
      var $handlers$$ = goog.debug.Logger.ENABLE_HIERARCHY ? this.handlers_ : goog.debug.Logger.rootHandlers_;
      return !!$handlers$$ && goog.array.remove($handlers$$, $handler$$);
    }
    return !1;
  };
  goog.debug.Logger.prototype.getParent = function() {
    return this.parent_;
  };
  goog.debug.Logger.prototype.getChildren = function() {
    this.children_ || (this.children_ = {});
    return this.children_;
  };
  goog.debug.Logger.prototype.setLevel = function($level$$) {
    goog.debug.LOGGING_ENABLED && (goog.debug.Logger.ENABLE_HIERARCHY ? this.level_ = $level$$ : (goog.asserts.assert(!this.name_, "Cannot call setLevel() on a non-root logger when goog.debug.Logger.ENABLE_HIERARCHY is false."), goog.debug.Logger.rootLevel_ = $level$$));
  };
  goog.debug.Logger.prototype.getEffectiveLevel = function() {
    if (!goog.debug.LOGGING_ENABLED) {
      return goog.debug.Logger.Level.OFF;
    }
    if (!goog.debug.Logger.ENABLE_HIERARCHY) {
      return goog.debug.Logger.rootLevel_;
    }
    if (this.level_) {
      return this.level_;
    }
    if (this.parent_) {
      return this.parent_.getEffectiveLevel();
    }
    goog.asserts.fail("Root logger has no level set.");
    return null;
  };
  goog.debug.Logger.prototype.isLoggable = function($level$$) {
    return goog.debug.LOGGING_ENABLED && $level$$.value >= this.getEffectiveLevel().value;
  };
  goog.debug.Logger.prototype.log = function($level$$, $msg$$, $opt_exception$$) {
    goog.debug.LOGGING_ENABLED && this.isLoggable($level$$) && (goog.isFunction($msg$$) && ($msg$$ = $msg$$()), this.doLogRecord_(this.getLogRecord($level$$, $msg$$, $opt_exception$$)));
  };
  goog.debug.Logger.prototype.getLogRecord = function($level$$, $msg$$, $opt_exception$$) {
    $level$$ = goog.debug.LogBuffer.isBufferingEnabled() ? goog.debug.LogBuffer.getInstance().addRecord($level$$, $msg$$, this.name_) : new goog.debug.LogRecord($level$$, String($msg$$), this.name_);
    $opt_exception$$ && $level$$.setException($opt_exception$$);
    return $level$$;
  };
  goog.debug.Logger.prototype.severe = function($msg$$, $opt_exception$$) {
    goog.debug.LOGGING_ENABLED && this.log(goog.debug.Logger.Level.SEVERE, $msg$$, $opt_exception$$);
  };
  goog.debug.Logger.prototype.warning = function($msg$$, $opt_exception$$) {
    goog.debug.LOGGING_ENABLED && this.log(goog.debug.Logger.Level.WARNING, $msg$$, $opt_exception$$);
  };
  goog.debug.Logger.prototype.info = function($msg$$, $opt_exception$$) {
    goog.debug.LOGGING_ENABLED && this.log(goog.debug.Logger.Level.INFO, $msg$$, $opt_exception$$);
  };
  goog.debug.Logger.prototype.fine = function($msg$$, $opt_exception$$) {
    goog.debug.LOGGING_ENABLED && this.log(goog.debug.Logger.Level.FINE, $msg$$, $opt_exception$$);
  };
  goog.debug.Logger.prototype.doLogRecord_ = function($logRecord$$) {
    goog.debug.Logger.ENABLE_PROFILER_LOGGING && goog.debug.Logger.logToProfilers("log:" + $logRecord$$.msg_);
    if (goog.debug.Logger.ENABLE_HIERARCHY) {
      for (var $i$jscomp$92_target$$ = this; $i$jscomp$92_target$$;) {
        $i$jscomp$92_target$$.callPublish_($logRecord$$), $i$jscomp$92_target$$ = $i$jscomp$92_target$$.getParent();
      }
    } else {
      $i$jscomp$92_target$$ = 0;
      for (var $handler$$; $handler$$ = goog.debug.Logger.rootHandlers_[$i$jscomp$92_target$$++];) {
        $handler$$($logRecord$$);
      }
    }
  };
  goog.debug.Logger.prototype.callPublish_ = function($logRecord$$) {
    if (this.handlers_) {
      for (var $i$$ = 0, $handler$$; $handler$$ = this.handlers_[$i$$]; $i$$++) {
        $handler$$($logRecord$$);
      }
    }
  };
  goog.debug.Logger.prototype.setParent_ = function($parent$$) {
    this.parent_ = $parent$$;
  };
  goog.debug.Logger.prototype.addChild_ = function($name$$, $logger$$) {
    this.getChildren()[$name$$] = $logger$$;
  };
  goog.debug.LogManager = {};
  goog.debug.LogManager.loggers_ = {};
  goog.debug.LogManager.rootLogger_ = null;
  goog.debug.LogManager.initialize = function() {
    goog.debug.LogManager.rootLogger_ || (goog.debug.LogManager.rootLogger_ = new goog.debug.Logger(goog.debug.Logger.ROOT_LOGGER_NAME), goog.debug.LogManager.loggers_[goog.debug.Logger.ROOT_LOGGER_NAME] = goog.debug.LogManager.rootLogger_, goog.debug.LogManager.rootLogger_.setLevel(goog.debug.Logger.Level.CONFIG));
  };
  goog.debug.LogManager.getLoggers = function() {
    return goog.debug.LogManager.loggers_;
  };
  goog.debug.LogManager.getRoot = function() {
    goog.debug.LogManager.initialize();
    return goog.debug.LogManager.rootLogger_;
  };
  goog.debug.LogManager.getLogger = function($name$$) {
    goog.debug.LogManager.initialize();
    var $ret$$ = goog.debug.LogManager.loggers_[$name$$];
    return $ret$$ || goog.debug.LogManager.createLogger_($name$$);
  };
  goog.debug.LogManager.createFunctionForCatchErrors = function($opt_logger$$) {
    return function($info$$) {
      var $logger$$ = $opt_logger$$ || goog.debug.LogManager.getRoot();
      $logger$$.severe("Error: " + $info$$.message + " (" + $info$$.fileName + " @ Line: " + $info$$.line + ")");
    };
  };
  goog.debug.LogManager.createLogger_ = function($name$$) {
    var $logger$$ = new goog.debug.Logger($name$$);
    if (goog.debug.Logger.ENABLE_HIERARCHY) {
      var $lastDotIndex_leafName$$ = $name$$.lastIndexOf("."), $parentLogger_parentName$$ = $name$$.substr(0, $lastDotIndex_leafName$$);
      $lastDotIndex_leafName$$ = $name$$.substr($lastDotIndex_leafName$$ + 1);
      $parentLogger_parentName$$ = goog.debug.LogManager.getLogger($parentLogger_parentName$$);
      $parentLogger_parentName$$.addChild_($lastDotIndex_leafName$$, $logger$$);
      $logger$$.setParent_($parentLogger_parentName$$);
    }
    return goog.debug.LogManager.loggers_[$name$$] = $logger$$;
  };
  //[javascript/closure/log/log.js]
  goog.log = {};
  goog.log.ENABLED = goog.debug.LOGGING_ENABLED;
  goog.log.ROOT_LOGGER_NAME = goog.debug.Logger.ROOT_LOGGER_NAME;
  goog.log.Logger = goog.debug.Logger;
  goog.log.Level = goog.debug.Logger.Level;
  goog.log.LogRecord = goog.debug.LogRecord;
  goog.log.getLogger = function($logger$jscomp$3_name$$, $opt_level$$) {
    return goog.log.ENABLED ? ($logger$jscomp$3_name$$ = goog.debug.LogManager.getLogger($logger$jscomp$3_name$$), $opt_level$$ && $logger$jscomp$3_name$$ && $logger$jscomp$3_name$$.setLevel($opt_level$$), $logger$jscomp$3_name$$) : null;
  };
  goog.log.addHandler = function($logger$$, $handler$$) {
    goog.log.ENABLED && $logger$$ && $logger$$.addHandler($handler$$);
  };
  goog.log.removeHandler = function($logger$$, $handler$$) {
    return goog.log.ENABLED && $logger$$ ? $logger$$.removeHandler($handler$$) : !1;
  };
  goog.log.log = function($logger$$, $level$$, $msg$$, $opt_exception$$) {
    goog.log.ENABLED && $logger$$ && $logger$$.log($level$$, $msg$$, $opt_exception$$);
  };
  goog.log.error = function($logger$$, $msg$$, $opt_exception$$) {
    goog.log.ENABLED && $logger$$ && $logger$$.severe($msg$$, $opt_exception$$);
  };
  goog.log.warning = function($logger$$, $msg$$, $opt_exception$$) {
    goog.log.ENABLED && $logger$$ && $logger$$.warning($msg$$, $opt_exception$$);
  };
  goog.log.info = function($logger$$, $msg$$, $opt_exception$$) {
    goog.log.ENABLED && $logger$$ && $logger$$.info($msg$$, $opt_exception$$);
  };
  goog.log.fine = function($logger$$, $msg$$, $opt_exception$$) {
    goog.log.ENABLED && $logger$$ && $logger$$.fine($msg$$, $opt_exception$$);
  };
  //[java/com/google/nbu/paisa/microapps/js/messaging/messaging.js]
  var module$exports$boq$paisamicroappsjs$messaging$Messaging = {}, module$contents$boq$paisamicroappsjs$messaging$Messaging_MessageType = {REQUEST:0, RESPONSE:1};
  module$exports$boq$paisamicroappsjs$messaging$Messaging.MessagingManager = function() {
  };
  module$exports$boq$paisamicroappsjs$messaging$Messaging.MessagingManager.connectWithViewer = function($win$$, $messageChannel$$, $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$) {
    $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$ = void 0 === $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$ ? {} : $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$;
    $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$ = void 0 === $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$.env ? module$exports$boq$paisamicroappsjs$messaging$constants.Environment.PROD : $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$.env;
    try {
      var $JSCompiler_inline_result$$ = $win$$.self !== $win$$.top;
    } catch ($e$$) {
      $JSCompiler_inline_result$$ = !0;
    }
    if (!$JSCompiler_inline_result$$) {
      throw Error("Unsupported environment: expected running in iframe");
    }
    $JSCompiler_inline_result$$ = $messageChannel$$.port1;
    $JSCompiler_inline_result$$ = new module$exports$boq$paisamicroappsjs$messaging$Messaging.Messaging($JSCompiler_inline_result$$, !0);
    var $connectionRequestMessage$$ = {type:module$contents$boq$paisamicroappsjs$messaging$Messaging_MessageType.REQUEST, name:module$exports$boq$paisamicroappsjs$messaging$constants.MessageName.REQUEST_CONNECTION};
    a: {
      if (goog.DEBUG && $win$$.microapps_local_viewer_origin) {
        $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$ = $win$$.microapps_local_viewer_origin;
      } else {
        var $viewerOrigin$$ = "https://microapps";
        switch($$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$) {
          case module$exports$boq$paisamicroappsjs$messaging$constants.Environment.PROD:
            $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$ = $viewerOrigin$$ + ".google.com/";
            break a;
          case module$exports$boq$paisamicroappsjs$messaging$constants.Environment.AUTOPUSH:
            $viewerOrigin$$ += "-autopush";
            break;
          case module$exports$boq$paisamicroappsjs$messaging$constants.Environment.PREPROD:
            $viewerOrigin$$ += "-preprod";
            break;
          case module$exports$boq$paisamicroappsjs$messaging$constants.Environment.PROD_TT:
            $viewerOrigin$$ += "-prod-tt";
            break;
          case module$exports$boq$paisamicroappsjs$messaging$constants.Environment.DAILY_1:
          case module$exports$boq$paisamicroappsjs$messaging$constants.Environment.DAILY_2:
          case module$exports$boq$paisamicroappsjs$messaging$constants.Environment.DAILY_3:
          case module$exports$boq$paisamicroappsjs$messaging$constants.Environment.DAILY_4:
          case module$exports$boq$paisamicroappsjs$messaging$constants.Environment.DAILY_5:
            $viewerOrigin$$ += "-" + $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$;
            break;
          default:
            throw Error("unknown environment");
        }
        $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$ = $viewerOrigin$$ + ".sandbox.google.com/";
      }
    }
    $win$$.parent.postMessage($connectionRequestMessage$$, $$jscomp$destructuring$var0_$jscomp$destructuring$var1_JSCompiler_inline_result$$, [$messageChannel$$.port2]);
    return $JSCompiler_inline_result$$;
  };
  module$exports$boq$paisamicroappsjs$messaging$Messaging.Messaging = function($port$$, $waitForConnectEnabled$$) {
    var $$jscomp$this$$ = this;
    $waitForConnectEnabled$$ = void 0 === $waitForConnectEnabled$$ ? !1 : $waitForConnectEnabled$$;
    this.port_ = $port$$;
    this.waitForConnectEnabled_ = !!$waitForConnectEnabled$$;
    this.requestIdCounter_ = 0;
    this.sentMessages_ = new Map;
    this.messageHandler_ = null;
    this.logger_ = (0,goog.log.getLogger)("boq.paisamicroappsjs.Messaging");
    this.waitForConnectPromise_ = new Promise(function($resolve$$) {
      return $$jscomp$this$$.waitForConnectResolver_ = $resolve$$;
    });
    this.port_.addEventListener("message", this.handleMessage_.bind(this));
    this.port_.start();
  };
  module$exports$boq$paisamicroappsjs$messaging$Messaging.Messaging.prototype.handleMessage_ = function($event$jscomp$5_message$$) {
    ($event$jscomp$5_message$$ = $event$jscomp$5_message$$.data) && (goog.isDefAndNotNull($event$jscomp$5_message$$.id) ? $event$jscomp$5_message$$.type === module$contents$boq$paisamicroappsjs$messaging$Messaging_MessageType.REQUEST ? this.handleRequest_($event$jscomp$5_message$$) : $event$jscomp$5_message$$.type === module$contents$boq$paisamicroappsjs$messaging$Messaging_MessageType.RESPONSE && this.handleResponse_($event$jscomp$5_message$$) : (0,goog.log.error)(this.logger_, 'Invalid message: expected field "id" not found.'));
  };
  module$exports$boq$paisamicroappsjs$messaging$Messaging.Messaging.prototype.handleRequest_ = function($message$$) {
    var $$jscomp$this$$ = this;
    if ($message$$.name === module$exports$boq$paisamicroappsjs$messaging$constants.MessageName.ACCEPT_CONNECTION) {
      (0,goog.log.info)(this.logger_, "Viewer connection accepted."), this.waitForConnectResolver_();
    } else {
      if (this.messageHandler_) {
        var $promise$$ = this.messageHandler_($message$$);
        if ($message$$.isResponseRequired) {
          var $requestId$$ = $message$$.id;
          $promise$$ ? $promise$$.then(function($data$$) {
            $$jscomp$this$$.sendResponse_($requestId$$, $message$$.name, $data$$);
          }, function($failure$$) {
            $$jscomp$this$$.sendResponseError_($requestId$$, $message$$.name, $failure$$);
          }) : ((0,goog.log.error)(this.logger_, "expected response but none given: " + $message$$.name), this.sendResponseError_($requestId$$, $message$$.name, Error("No response")));
        }
      } else {
        (0,goog.log.error)(this.logger_, "Failed to handle message: " + $message$$.name + ", no handler is available.");
      }
    }
  };
  module$exports$boq$paisamicroappsjs$messaging$Messaging.Messaging.prototype.handleResponse_ = function($message$$) {
    var $requestId$$ = $message$$.id, $pendingRequest$$ = this.sentMessages_.get($requestId$$);
    $pendingRequest$$ && (this.sentMessages_["delete"]($requestId$$), $message$$.error ? $pendingRequest$$.reject(Error("Request " + $message$$.name + " failed: " + $message$$.error)) : $pendingRequest$$.resolve($message$$.data));
  };
  module$exports$boq$paisamicroappsjs$messaging$Messaging.Messaging.prototype.sendResponse_ = function($requestId$$, $messageName$$, $messageData$$) {
    this.sendMessage_({id:$requestId$$, type:module$contents$boq$paisamicroappsjs$messaging$Messaging_MessageType.RESPONSE, name:$messageName$$, data:$messageData$$});
  };
  module$exports$boq$paisamicroappsjs$messaging$Messaging.Messaging.prototype.sendResponseError_ = function($requestId$$, $messageName$$, $errString_error$$) {
    $errString_error$$ = $errString_error$$ ? $errString_error$$ instanceof Error ? String($errString_error$$) : $errString_error$$ : "Unknown error";
    (0,goog.log.info)(this.logger_, "sendResponseError_, Message name: " + $messageName$$ + ", " + $errString_error$$);
    this.sendMessage_({id:$requestId$$, type:module$contents$boq$paisamicroappsjs$messaging$Messaging_MessageType.RESPONSE, name:$messageName$$, error:$errString_error$$});
  };
  module$exports$boq$paisamicroappsjs$messaging$Messaging.Messaging.prototype.sendMessage_ = function($message$$) {
    this.port_.postMessage($message$$);
  };
  module$exports$boq$paisamicroappsjs$messaging$Messaging.Messaging.prototype.sendRequest = function($messageName$$, $messageData$$, $requiresResponse$$) {
    var $$jscomp$this$$ = this;
    $requiresResponse$$ = void 0 === $requiresResponse$$ ? !0 : $requiresResponse$$;
    var $requestId$$ = ++this.requestIdCounter_, $promise$$ = Promise.resolve();
    $requiresResponse$$ && ($promise$$ = new Promise(function($resolve$$, $reject$$) {
      $$jscomp$this$$.sentMessages_.set($requestId$$, {resolve:$resolve$$, reject:$reject$$});
    }));
    var $messageToSent$$ = {id:$requestId$$, type:module$contents$boq$paisamicroappsjs$messaging$Messaging_MessageType.REQUEST, name:$messageName$$, data:$messageData$$, isResponseRequired:$requiresResponse$$};
    this.waitForConnectEnabled_ ? this.waitForConnectPromise_.then(function() {
      $$jscomp$this$$.sendMessage_($messageToSent$$);
    }) : this.sendMessage_($messageToSent$$);
    return $promise$$;
  };
  //[javascript/closure/uri/utils.js]
  goog.uri = {};
  goog.uri.utils = {};
  goog.uri.utils.CharCode_ = {AMPERSAND:38, EQUAL:61, HASH:35, QUESTION:63};
  goog.uri.utils.buildFromEncodedParts = function($opt_scheme$$, $opt_userInfo$$, $opt_domain$$, $opt_port$$, $opt_path$$, $opt_queryData$$, $opt_fragment$$) {
    var $out$$ = "";
    $opt_scheme$$ && ($out$$ += $opt_scheme$$ + ":");
    $opt_domain$$ && ($out$$ += "//", $opt_userInfo$$ && ($out$$ += $opt_userInfo$$ + "@"), $out$$ += $opt_domain$$, $opt_port$$ && ($out$$ += ":" + $opt_port$$));
    $opt_path$$ && ($out$$ += $opt_path$$);
    $opt_queryData$$ && ($out$$ += "?" + $opt_queryData$$);
    $opt_fragment$$ && ($out$$ += "#" + $opt_fragment$$);
    return $out$$;
  };
  goog.uri.utils.splitRe_ = /^(?:([^:/?#.]+):)?(?:\/\/(?:([^/?#]*)@)?([^/#?]*?)(?::([0-9]+))?(?=[/#?]|$))?([^?#]+)?(?:\?([^#]*))?(?:#([\s\S]*))?$/;
  goog.uri.utils.ComponentIndex = {SCHEME:1, USER_INFO:2, DOMAIN:3, PORT:4, PATH:5, QUERY_DATA:6, FRAGMENT:7};
  goog.uri.utils.split = function($uri$$) {
    return $uri$$.match(goog.uri.utils.splitRe_);
  };
  goog.uri.utils.decodeIfPossible_ = function($uri$$, $opt_preserveReserved$$) {
    return $uri$$ ? $opt_preserveReserved$$ ? decodeURI($uri$$) : decodeURIComponent($uri$$) : $uri$$;
  };
  goog.uri.utils.getComponentByIndex_ = function($componentIndex$$, $uri$$) {
    return goog.uri.utils.split($uri$$)[$componentIndex$$] || null;
  };
  goog.uri.utils.getScheme = function($uri$$) {
    return goog.uri.utils.getComponentByIndex_(goog.uri.utils.ComponentIndex.SCHEME, $uri$$);
  };
  goog.uri.utils.getEffectiveScheme = function($protocol$jscomp$1_scheme$jscomp$3_uri$$) {
    $protocol$jscomp$1_scheme$jscomp$3_uri$$ = goog.uri.utils.getScheme($protocol$jscomp$1_scheme$jscomp$3_uri$$);
    !$protocol$jscomp$1_scheme$jscomp$3_uri$$ && goog.global.self && goog.global.self.location && ($protocol$jscomp$1_scheme$jscomp$3_uri$$ = goog.global.self.location.protocol, $protocol$jscomp$1_scheme$jscomp$3_uri$$ = $protocol$jscomp$1_scheme$jscomp$3_uri$$.substr(0, $protocol$jscomp$1_scheme$jscomp$3_uri$$.length - 1));
    return $protocol$jscomp$1_scheme$jscomp$3_uri$$ ? $protocol$jscomp$1_scheme$jscomp$3_uri$$.toLowerCase() : "";
  };
  goog.uri.utils.getUserInfoEncoded = function($uri$$) {
    return goog.uri.utils.getComponentByIndex_(goog.uri.utils.ComponentIndex.USER_INFO, $uri$$);
  };
  goog.uri.utils.getUserInfo = function($uri$$) {
    return goog.uri.utils.decodeIfPossible_(goog.uri.utils.getUserInfoEncoded($uri$$));
  };
  goog.uri.utils.getDomainEncoded = function($uri$$) {
    return goog.uri.utils.getComponentByIndex_(goog.uri.utils.ComponentIndex.DOMAIN, $uri$$);
  };
  goog.uri.utils.getDomain = function($uri$$) {
    return goog.uri.utils.decodeIfPossible_(goog.uri.utils.getDomainEncoded($uri$$), !0);
  };
  goog.uri.utils.getPort = function($uri$$) {
    return Number(goog.uri.utils.getComponentByIndex_(goog.uri.utils.ComponentIndex.PORT, $uri$$)) || null;
  };
  goog.uri.utils.getPathEncoded = function($uri$$) {
    return goog.uri.utils.getComponentByIndex_(goog.uri.utils.ComponentIndex.PATH, $uri$$);
  };
  goog.uri.utils.getPath = function($uri$$) {
    return goog.uri.utils.decodeIfPossible_(goog.uri.utils.getPathEncoded($uri$$), !0);
  };
  goog.uri.utils.getQueryData = function($uri$$) {
    return goog.uri.utils.getComponentByIndex_(goog.uri.utils.ComponentIndex.QUERY_DATA, $uri$$);
  };
  goog.uri.utils.getFragmentEncoded = function($uri$$) {
    var $hashIndex$$ = $uri$$.indexOf("#");
    return 0 > $hashIndex$$ ? null : $uri$$.substr($hashIndex$$ + 1);
  };
  goog.uri.utils.setFragmentEncoded = function($uri$$, $fragment$$) {
    return goog.uri.utils.removeFragment($uri$$) + ($fragment$$ ? "#" + $fragment$$ : "");
  };
  goog.uri.utils.getFragment = function($uri$$) {
    return goog.uri.utils.decodeIfPossible_(goog.uri.utils.getFragmentEncoded($uri$$));
  };
  goog.uri.utils.getHost = function($pieces_uri$$) {
    $pieces_uri$$ = goog.uri.utils.split($pieces_uri$$);
    return goog.uri.utils.buildFromEncodedParts($pieces_uri$$[goog.uri.utils.ComponentIndex.SCHEME], $pieces_uri$$[goog.uri.utils.ComponentIndex.USER_INFO], $pieces_uri$$[goog.uri.utils.ComponentIndex.DOMAIN], $pieces_uri$$[goog.uri.utils.ComponentIndex.PORT]);
  };
  goog.uri.utils.getOrigin = function($pieces$jscomp$1_uri$$) {
    $pieces$jscomp$1_uri$$ = goog.uri.utils.split($pieces$jscomp$1_uri$$);
    return goog.uri.utils.buildFromEncodedParts($pieces$jscomp$1_uri$$[goog.uri.utils.ComponentIndex.SCHEME], null, $pieces$jscomp$1_uri$$[goog.uri.utils.ComponentIndex.DOMAIN], $pieces$jscomp$1_uri$$[goog.uri.utils.ComponentIndex.PORT]);
  };
  goog.uri.utils.getPathAndAfter = function($pieces$jscomp$2_uri$$) {
    $pieces$jscomp$2_uri$$ = goog.uri.utils.split($pieces$jscomp$2_uri$$);
    return goog.uri.utils.buildFromEncodedParts(null, null, null, null, $pieces$jscomp$2_uri$$[goog.uri.utils.ComponentIndex.PATH], $pieces$jscomp$2_uri$$[goog.uri.utils.ComponentIndex.QUERY_DATA], $pieces$jscomp$2_uri$$[goog.uri.utils.ComponentIndex.FRAGMENT]);
  };
  goog.uri.utils.removeFragment = function($uri$$) {
    var $hashIndex$$ = $uri$$.indexOf("#");
    return 0 > $hashIndex$$ ? $uri$$ : $uri$$.substr(0, $hashIndex$$);
  };
  goog.uri.utils.haveSameDomain = function($pieces1_uri1$$, $pieces2_uri2$$) {
    $pieces1_uri1$$ = goog.uri.utils.split($pieces1_uri1$$);
    $pieces2_uri2$$ = goog.uri.utils.split($pieces2_uri2$$);
    return $pieces1_uri1$$[goog.uri.utils.ComponentIndex.DOMAIN] == $pieces2_uri2$$[goog.uri.utils.ComponentIndex.DOMAIN] && $pieces1_uri1$$[goog.uri.utils.ComponentIndex.SCHEME] == $pieces2_uri2$$[goog.uri.utils.ComponentIndex.SCHEME] && $pieces1_uri1$$[goog.uri.utils.ComponentIndex.PORT] == $pieces2_uri2$$[goog.uri.utils.ComponentIndex.PORT];
  };
  goog.uri.utils.assertNoFragmentsOrQueries_ = function($uri$$) {
    goog.asserts.assert(0 > $uri$$.indexOf("#") && 0 > $uri$$.indexOf("?"), "goog.uri.utils: Fragment or query identifiers are not supported: [%s]", $uri$$);
  };
  goog.uri.utils.parseQueryData = function($encodedQuery_pairs$$, $callback$$) {
    if ($encodedQuery_pairs$$) {
      $encodedQuery_pairs$$ = $encodedQuery_pairs$$.split("&");
      for (var $i$$ = 0; $i$$ < $encodedQuery_pairs$$.length; $i$$++) {
        var $indexOfEquals$$ = $encodedQuery_pairs$$[$i$$].indexOf("="), $value$$ = null;
        if (0 <= $indexOfEquals$$) {
          var $name$$ = $encodedQuery_pairs$$[$i$$].substring(0, $indexOfEquals$$);
          $value$$ = $encodedQuery_pairs$$[$i$$].substring($indexOfEquals$$ + 1);
        } else {
          $name$$ = $encodedQuery_pairs$$[$i$$];
        }
        $callback$$($name$$, $value$$ ? goog.string.urlDecode($value$$) : "");
      }
    }
  };
  goog.uri.utils.splitQueryData_ = function($uri$$) {
    var $hashIndex$$ = $uri$$.indexOf("#");
    0 > $hashIndex$$ && ($hashIndex$$ = $uri$$.length);
    var $questionIndex$$ = $uri$$.indexOf("?");
    if (0 > $questionIndex$$ || $questionIndex$$ > $hashIndex$$) {
      $questionIndex$$ = $hashIndex$$;
      var $queryData$$ = "";
    } else {
      $queryData$$ = $uri$$.substring($questionIndex$$ + 1, $hashIndex$$);
    }
    return [$uri$$.substr(0, $questionIndex$$), $queryData$$, $uri$$.substr($hashIndex$$)];
  };
  goog.uri.utils.joinQueryData_ = function($parts$$) {
    return $parts$$[0] + ($parts$$[1] ? "?" + $parts$$[1] : "") + $parts$$[2];
  };
  goog.uri.utils.appendQueryData_ = function($queryData$$, $newData$$) {
    return $newData$$ ? $queryData$$ ? $queryData$$ + "&" + $newData$$ : $newData$$ : $queryData$$;
  };
  goog.uri.utils.appendQueryDataToUri_ = function($parts$jscomp$8_uri$$, $queryData$$) {
    if (!$queryData$$) {
      return $parts$jscomp$8_uri$$;
    }
    $parts$jscomp$8_uri$$ = goog.uri.utils.splitQueryData_($parts$jscomp$8_uri$$);
    $parts$jscomp$8_uri$$[1] = goog.uri.utils.appendQueryData_($parts$jscomp$8_uri$$[1], $queryData$$);
    return goog.uri.utils.joinQueryData_($parts$jscomp$8_uri$$);
  };
  goog.uri.utils.appendKeyValuePairs_ = function($key$$, $value$$, $pairs$$) {
    goog.asserts.assertString($key$$);
    if (goog.isArray($value$$)) {
      goog.asserts.assertArray($value$$);
      for (var $j$$ = 0; $j$$ < $value$$.length; $j$$++) {
        goog.uri.utils.appendKeyValuePairs_($key$$, String($value$$[$j$$]), $pairs$$);
      }
    } else {
      null != $value$$ && $pairs$$.push($key$$ + ("" === $value$$ ? "" : "=" + goog.string.urlEncode($value$$)));
    }
  };
  goog.uri.utils.buildQueryData = function($keysAndValues$$, $i$$) {
    goog.asserts.assert(0 == Math.max($keysAndValues$$.length - ($i$$ || 0), 0) % 2, "goog.uri.utils: Key/value lists must be even in length.");
    var $params$$ = [];
    for ($i$$ = $i$$ || 0; $i$$ < $keysAndValues$$.length; $i$$ += 2) {
      var $key$$ = $keysAndValues$$[$i$$];
      goog.uri.utils.appendKeyValuePairs_($key$$, $keysAndValues$$[$i$$ + 1], $params$$);
    }
    return $params$$.join("&");
  };
  goog.uri.utils.buildQueryDataFromMap = function($map$$) {
    var $params$$ = [], $key$$;
    for ($key$$ in $map$$) {
      goog.uri.utils.appendKeyValuePairs_($key$$, $map$$[$key$$], $params$$);
    }
    return $params$$.join("&");
  };
  goog.uri.utils.appendParams = function($uri$$, $var_args$$) {
    var $queryData$$ = 2 == arguments.length ? goog.uri.utils.buildQueryData(arguments[1], 0) : goog.uri.utils.buildQueryData(arguments, 1);
    return goog.uri.utils.appendQueryDataToUri_($uri$$, $queryData$$);
  };
  goog.uri.utils.appendParamsFromMap = function($uri$$, $map$jscomp$6_queryData$$) {
    $map$jscomp$6_queryData$$ = goog.uri.utils.buildQueryDataFromMap($map$jscomp$6_queryData$$);
    return goog.uri.utils.appendQueryDataToUri_($uri$$, $map$jscomp$6_queryData$$);
  };
  goog.uri.utils.appendParam = function($uri$$, $key$$, $opt_value$jscomp$11_value$$) {
    $opt_value$jscomp$11_value$$ = goog.isDefAndNotNull($opt_value$jscomp$11_value$$) ? "=" + goog.string.urlEncode($opt_value$jscomp$11_value$$) : "";
    return goog.uri.utils.appendQueryDataToUri_($uri$$, $key$$ + $opt_value$jscomp$11_value$$);
  };
  goog.uri.utils.findParam_ = function($uri$$, $index$$, $keyEncoded$$, $hashOrEndIndex$$) {
    for (var $keyLength$$ = $keyEncoded$$.length; 0 <= ($index$$ = $uri$$.indexOf($keyEncoded$$, $index$$)) && $index$$ < $hashOrEndIndex$$;) {
      var $followingChar_precedingChar$$ = $uri$$.charCodeAt($index$$ - 1);
      if ($followingChar_precedingChar$$ == goog.uri.utils.CharCode_.AMPERSAND || $followingChar_precedingChar$$ == goog.uri.utils.CharCode_.QUESTION) {
        if ($followingChar_precedingChar$$ = $uri$$.charCodeAt($index$$ + $keyLength$$), !$followingChar_precedingChar$$ || $followingChar_precedingChar$$ == goog.uri.utils.CharCode_.EQUAL || $followingChar_precedingChar$$ == goog.uri.utils.CharCode_.AMPERSAND || $followingChar_precedingChar$$ == goog.uri.utils.CharCode_.HASH) {
          return $index$$;
        }
      }
      $index$$ += $keyLength$$ + 1;
    }
    return -1;
  };
  goog.uri.utils.hashOrEndRe_ = /#|$/;
  goog.uri.utils.hasParam = function($uri$$, $keyEncoded$$) {
    return 0 <= goog.uri.utils.findParam_($uri$$, 0, $keyEncoded$$, $uri$$.search(goog.uri.utils.hashOrEndRe_));
  };
  goog.uri.utils.getParamValue = function($uri$$, $keyEncoded$$) {
    var $hashOrEndIndex$$ = $uri$$.search(goog.uri.utils.hashOrEndRe_), $foundIndex$$ = goog.uri.utils.findParam_($uri$$, 0, $keyEncoded$$, $hashOrEndIndex$$);
    if (0 > $foundIndex$$) {
      return null;
    }
    var $endPosition$$ = $uri$$.indexOf("&", $foundIndex$$);
    if (0 > $endPosition$$ || $endPosition$$ > $hashOrEndIndex$$) {
      $endPosition$$ = $hashOrEndIndex$$;
    }
    $foundIndex$$ += $keyEncoded$$.length + 1;
    return goog.string.urlDecode($uri$$.substr($foundIndex$$, $endPosition$$ - $foundIndex$$));
  };
  goog.uri.utils.getParamValues = function($uri$$, $keyEncoded$$) {
    for (var $hashOrEndIndex$$ = $uri$$.search(goog.uri.utils.hashOrEndRe_), $position$$ = 0, $foundIndex$$, $result$$ = []; 0 <= ($foundIndex$$ = goog.uri.utils.findParam_($uri$$, $position$$, $keyEncoded$$, $hashOrEndIndex$$));) {
      $position$$ = $uri$$.indexOf("&", $foundIndex$$);
      if (0 > $position$$ || $position$$ > $hashOrEndIndex$$) {
        $position$$ = $hashOrEndIndex$$;
      }
      $foundIndex$$ += $keyEncoded$$.length + 1;
      $result$$.push(goog.string.urlDecode($uri$$.substr($foundIndex$$, $position$$ - $foundIndex$$)));
    }
    return $result$$;
  };
  goog.uri.utils.trailingQueryPunctuationRe_ = /[?&]($|#)/;
  goog.uri.utils.removeParam = function($uri$$, $keyEncoded$$) {
    for (var $hashOrEndIndex$$ = $uri$$.search(goog.uri.utils.hashOrEndRe_), $position$$ = 0, $foundIndex$$, $buffer$$ = []; 0 <= ($foundIndex$$ = goog.uri.utils.findParam_($uri$$, $position$$, $keyEncoded$$, $hashOrEndIndex$$));) {
      $buffer$$.push($uri$$.substring($position$$, $foundIndex$$)), $position$$ = Math.min($uri$$.indexOf("&", $foundIndex$$) + 1 || $hashOrEndIndex$$, $hashOrEndIndex$$);
    }
    $buffer$$.push($uri$$.substr($position$$));
    return $buffer$$.join("").replace(goog.uri.utils.trailingQueryPunctuationRe_, "$1");
  };
  goog.uri.utils.setParam = function($uri$$, $keyEncoded$$, $value$$) {
    return goog.uri.utils.appendParam(goog.uri.utils.removeParam($uri$$, $keyEncoded$$), $keyEncoded$$, $value$$);
  };
  goog.uri.utils.setParamsFromMap = function($parts$jscomp$9_uri$$, $params$$) {
    $parts$jscomp$9_uri$$ = goog.uri.utils.splitQueryData_($parts$jscomp$9_uri$$);
    var $queryData$$ = $parts$jscomp$9_uri$$[1], $buffer$$ = [];
    $queryData$$ && goog.array.forEach($queryData$$.split("&"), function($pair$$) {
      var $indexOfEquals$jscomp$1_name$$ = $pair$$.indexOf("=");
      $indexOfEquals$jscomp$1_name$$ = 0 <= $indexOfEquals$jscomp$1_name$$ ? $pair$$.substr(0, $indexOfEquals$jscomp$1_name$$) : $pair$$;
      $params$$.hasOwnProperty($indexOfEquals$jscomp$1_name$$) || $buffer$$.push($pair$$);
    });
    $parts$jscomp$9_uri$$[1] = goog.uri.utils.appendQueryData_($buffer$$.join("&"), goog.uri.utils.buildQueryDataFromMap($params$$));
    return goog.uri.utils.joinQueryData_($parts$jscomp$9_uri$$);
  };
  goog.uri.utils.appendPath = function($baseUri$$, $path$$) {
    goog.uri.utils.assertNoFragmentsOrQueries_($baseUri$$);
    goog.string.endsWith($baseUri$$, "/") && ($baseUri$$ = $baseUri$$.substr(0, $baseUri$$.length - 1));
    goog.string.startsWith($path$$, "/") && ($path$$ = $path$$.substr(1));
    return goog.string.buildString($baseUri$$, "/", $path$$);
  };
  goog.uri.utils.setPath = function($parts$jscomp$10_uri$$, $path$$) {
    goog.string.startsWith($path$$, "/") || ($path$$ = "/" + $path$$);
    $parts$jscomp$10_uri$$ = goog.uri.utils.split($parts$jscomp$10_uri$$);
    return goog.uri.utils.buildFromEncodedParts($parts$jscomp$10_uri$$[goog.uri.utils.ComponentIndex.SCHEME], $parts$jscomp$10_uri$$[goog.uri.utils.ComponentIndex.USER_INFO], $parts$jscomp$10_uri$$[goog.uri.utils.ComponentIndex.DOMAIN], $parts$jscomp$10_uri$$[goog.uri.utils.ComponentIndex.PORT], $path$$, $parts$jscomp$10_uri$$[goog.uri.utils.ComponentIndex.QUERY_DATA], $parts$jscomp$10_uri$$[goog.uri.utils.ComponentIndex.FRAGMENT]);
  };
  goog.uri.utils.StandardQueryParam = {RANDOM:"zx"};
  goog.uri.utils.makeUnique = function($uri$$) {
    return goog.uri.utils.setParam($uri$$, goog.uri.utils.StandardQueryParam.RANDOM, goog.string.getRandomString());
  };
  //[java/com/google/nbu/paisa/microapps/js/api/v1/microapps_api_impl.js]
  var module$contents$boq$paisamicroappsjs$api$v1$MicroappsApiImpl_instance = null;
  function module$contents$boq$paisamicroappsjs$api$v1$MicroappsApiImpl_getParameter($pageLocationHash_urlParameters_win$$, $data$jscomp$59_param$$, $defaultValue$$) {
    $pageLocationHash_urlParameters_win$$ = $pageLocationHash_urlParameters_win$$.location.hash;
    $pageLocationHash_urlParameters_win$$ = new URLSearchParams($pageLocationHash_urlParameters_win$$ ? $pageLocationHash_urlParameters_win$$.slice(1) : "");
    $data$jscomp$59_param$$ = $pageLocationHash_urlParameters_win$$.get($data$jscomp$59_param$$);
    return null == $data$jscomp$59_param$$ ? $defaultValue$$ : $data$jscomp$59_param$$;
  }
  var module$exports$boq$paisamicroappsjs$api$v1$MicroappsApiImpl = function($win$$, $messaging$$) {
    this.messaging_ = $messaging$$;
    this.win_ = $win$$;
    this.logger_ = (0,goog.log.getLogger)("boq.paisamicroappsjs.v1.apiimpl");
  };
  module$exports$boq$paisamicroappsjs$api$v1$MicroappsApiImpl.getInstance = function() {
    if (!module$contents$boq$paisamicroappsjs$api$v1$MicroappsApiImpl_instance) {
      var $environment$$ = module$contents$boq$paisamicroappsjs$api$v1$MicroappsApiImpl_getParameter(window, "microappsViewerEnv", module$exports$boq$paisamicroappsjs$messaging$constants.Environment.PREPROD);
      module$contents$boq$paisamicroappsjs$api$v1$MicroappsApiImpl_instance = new module$exports$boq$paisamicroappsjs$api$v1$MicroappsApiImpl(window, module$exports$boq$paisamicroappsjs$messaging$Messaging.MessagingManager.connectWithViewer(window, new MessageChannel, {env:$environment$$}));
    }
    return module$contents$boq$paisamicroappsjs$api$v1$MicroappsApiImpl_instance;
  };
  module$exports$boq$paisamicroappsjs$api$v1$MicroappsApiImpl.prototype.initialize = function() {
    this.isLoadedInTrustedOrigins_() || goog.dom.safe.setLocationHref(goog.asserts.assert(this.win_.location), goog.html.SafeUrl.ABOUT_BLANK);
  };
  module$exports$boq$paisamicroappsjs$api$v1$MicroappsApiImpl.prototype.requestPayment = function($paymentRequest$$) {
    return null != $paymentRequest$$ && goog.isObject($paymentRequest$$) ? this.messaging_.sendRequest("requestPayment", $paymentRequest$$) : Promise.reject("valid paymentRequest object is required for this API.");
  };
  module$exports$boq$paisamicroappsjs$api$v1$MicroappsApiImpl.prototype.requestScan = function($request$jscomp$5_scanRequest$$) {
    $request$jscomp$5_scanRequest$$ = $request$jscomp$5_scanRequest$$ || {};
    return this.messaging_.sendRequest("requestScan", $request$jscomp$5_scanRequest$$);
  };
  module$exports$boq$paisamicroappsjs$api$v1$MicroappsApiImpl.prototype.getParameter = function($param$$, $defaultValue$$) {
    return module$contents$boq$paisamicroappsjs$api$v1$MicroappsApiImpl_getParameter(this.win_, $param$$, $defaultValue$$);
  };
  module$exports$boq$paisamicroappsjs$api$v1$MicroappsApiImpl.prototype.getCurrentLocation = function() {
    return this.messaging_.sendRequest("getCurrentLocation", {});
  };
  module$exports$boq$paisamicroappsjs$api$v1$MicroappsApiImpl.prototype.isLoadedInTrustedOrigins_ = function() {
    if (this.win_.self !== this.win_.top) {
      if (this.win_.location.origin && this.win_.location.ancestorOrigins) {
        var $appOrigin$$ = this.win_.location.origin, $parentOrigins$$ = this.win_.location.ancestorOrigins;
        return goog.array.every($parentOrigins$$, function($origin$$) {
          var $domain$$ = goog.uri.utils.getDomain($origin$$) || "";
          return goog.string.caseInsensitiveEndsWith($domain$$, ".google.com") || $origin$$ === $appOrigin$$;
        });
      }
      return !1;
    }
    return !0;
  };
  //[java/com/google/nbu/paisa/microapps/js/api/api_exports.js]
  var module$contents$boq$paisamicroappsjs$api$exports_api = module$exports$boq$paisamicroappsjs$api$MicroappsApi.create(function() {
    return module$exports$boq$paisamicroappsjs$api$v1$MicroappsApiImpl.getInstance();
  });
  goog.exportSymbol("microapps.requestPayment", module$contents$boq$paisamicroappsjs$api$exports_api.requestPayment);
  goog.exportSymbol("microapps.requestScan", module$contents$boq$paisamicroappsjs$api$exports_api.requestScan);
  goog.exportSymbol("microapps.getParameter", module$contents$boq$paisamicroappsjs$api$exports_api.getParameter);
  goog.exportSymbol("microapps.initialize", module$contents$boq$paisamicroappsjs$api$exports_api.initialize);
  goog.exportSymbol("microapps.getCurrentLocation", module$contents$boq$paisamicroappsjs$api$exports_api.getCurrentLocation);
  }).call(this);
