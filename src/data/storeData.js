const ITEMS = [{
  id: 0,
  name: 'Apple',
  thumbnail: '🍎',
  photoUrl: 'https://media.istockphoto.com/photos/red-apple-picture-id495878092?k=6&m=495878092&s=612x612&w=0&h=q9k5jN-1giBGZgTM6QhyKkPqtGf6vRpkgDzAwEz9DkY=',
  price: 10,
  description: 'An apple',
},
{
  id: 1,
  name: 'Ball',
  thumbnail: '🌎',
  photoUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Earth-DSCOVR-20150706-IFV.jpg/220px-Earth-DSCOVR-20150706-IFV.jpg',
  price: 2,
  description: 'A ball',
}, {
  id: 2,
  name: 'Cat',
  thumbnail: '🐈',
  photoUrl: 'https://images.fineartamerica.com/images-medium-large/cat-on-white-background-american-images-inc.jpg',
  price: 50,
  description: 'A cute cat',
}, {
  id: 3,
  name: 'Dog',
  thumbnail: '🐶',
  photoUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQAWrNu1pCVH1sMxGEUwyXE97qRdpyNgojXj37uvN42909V87b7',
  price: 20,
  description: 'A dog',
}, {
  id: 4,
  name: 'Elephant',
  thumbnail: '🐘',
  photoUrl: 'https://images-na.ssl-images-amazon.com/images/I/51O9QgAdkGL.jpg',
  price: 1000,
  description: 'An Elephant',
},
]

export { ITEMS }
