import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'vue-googlemaps/dist/vue-googlemaps.css'
import VueGoogleMaps from 'vue-googlemaps'

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    // Google API key
    apiKey: 'AIzaSyBlE2-T4wsb5gZFYV0ghcfzXOBAi0o0tOQ',
    // Enable more Google Maps libraries here
    libraries: ['places'],
    // Use new renderer
    useBetaRenderer: false,
  },
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
